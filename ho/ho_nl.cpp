
#include <Common/GetPot>

#include <boost/integer/static_min_max.hpp>
#include <boost/math/constants/constants.hpp>

#include "ContinuousData/Data.h"
#include "Scheme/Schemes.h"

// #include "Common/chrono.hpp"

// #include "Data.h"
// #include "Postproc/postProcessing.h"
// #include "Scheme/NewtonMethod.h"

// #include "Eigen/SparseCore"
// #include "Eigen/SparseLU"

// #include <unsupported/Eigen/SparseExtra>

#ifndef DEGREE
#define DEGREE 2
#endif

#ifndef epsilon_stab
#define epsilon_stab 1.
#endif

#ifndef r_expfitt
#define r_expfitt 0.
#endif

#ifndef nb_pts_in_graph
#define nb_pts_in_graph 12
#endif

#ifndef nb_pts_in_visu
#define nb_pts_in_visu 100
#endif

using boost::math::constants::pi;

using namespace ho;


//------------------------------------------------------------------------------

typedef Eigen::Matrix<std::size_t, Eigen::Dynamic, 1> IndexVectorType;
typedef Eigen::SparseMatrix<Real> SparseMatrixType;
typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> SolutionVectorType;
typedef Eigen::Triplet<Real> TripletType;

typedef ho::diffusion::LocalContrib<DEGREE> LC;
typedef ho::diffusion::GlobalContrib<DEGREE> GC;
typedef ho::GlobalSys<DEGREE> GS;
typedef ho::DiscUnkw<DEGREE> DU;
typedef ho::NL::NewtonMethod<DEGREE> NMethod; 
typedef ho::NL::Scheme<DEGREE> SCH; 


typedef ho::HierarchicalScalarBasis1d<DEGREE> FaceBasis;


//------------------------------------------------------------------------------
// Functions, declarations


// Create the Mesh 
std::shared_ptr<Mesh> Mesh_read(const string & Mesh_file);

// "specific" mains 
int main_nonpoly(const bool & debug); 
int main_positivity(const bool & debug);
int main_convergence(const bool & debug);
int main_exact(const bool & debug); 

//vector<Real>  computations_test_loc(const Mesh *Th, const int & iT);

//------------------------------------------------------------------------------
// Functions, implementation 

int main()
{ 
    std::cout << "\n\nScheme considered : nonlinear scheme" << std::endl; 
    std::cout << "\nMixed HHO("<< DEGREE <<","<< DEGREE+1 <<") version, with Lehrenfeld-Schöberl stabilisation and full gradient \n"  << std::endl;
    std::cout << "Please double check that you use the correct degree unknow."  << std::endl;
    std::cout << "Reminder: the expected accuracy order in L^2 norm is " <<  DEGREE+2 << std::endl;
    std::cout << "\n\n ~~~~~~  ~~~~~~  ~~~~~~  ~~~~~~  ~~~~~~  ~~~~~~  ~~~~~~  ~~~~~~  ~~~~~~   \n\n"  << std::endl;    
    
    // boolean debug, for specific test case 
    bool debug = false; 

    std::string choice; 
    std::cout << "Select your test case by giving a keyword: "  << std::endl;
    std::cout << " - convergence study \t\t->\tconvergence "  << std::endl;
    std::cout << " - explicit solution \t\t->\texpli "  << std::endl;    
    std::cout << " - challenging positivity \t->\tposi "  << std::endl;   
    std::cout << " - non-polynomial potential \t->\tnonpoly "  << std::endl;
    cin >> choice ; 
    std::cout << " \n"  << std::endl;

    if(choice == "convergence"){
        main_convergence(debug);
    } //if convergence
    
    else if(choice == "expli"){
        main_exact(debug);
    } // if expli
    
    else if(choice == "posi"){
        main_positivity(debug);
    } // if positivity
    
    else if(choice == "nonpoly"){
        main_nonpoly(debug);
    } // if nonpoly

    else{
        std::cout << "You used an incorrect keyword "  << std::endl;        
        std::cout << "Please relaunch the simulation and select a correct keyword"  << std::endl;
        std::cout << "END OF THE SIMULATION"  << std::endl;
    } //else wrong key word 


    return 0;
} // main 

int main_exact(const bool & debug){ // explicit solution | longtime behaviour 
    
    
    cout << "Simulation with an explicit solution" << endl;

 
    
    string  geo_mesh ; //= "pi6_tiltedhexagonal_2"; // = "hexagonal";

    Real T ;
    Real Dt ;
    
    if(debug){
        cout << "Simulation corresponding to the long-time results of the paper." << endl;
        //geo_mesh = "mesh2_2";
        //geo_mesh = "mesh_quad_2";
        geo_mesh = "mesh4_1_4";
        //geo_mesh = "hexagonal_1";
        //geo_mesh = "mesh7";        
        //geo_mesh = "pi6_tiltedhexagonal_2"; 
        T = 350.;
        Dt = 0.1;
    }
    else{
        cout << "Custom simulation. Select mesh, time step and final time." << endl;
        cout << "Mesh ? (name with number, ex : hexagonal_3 or mesh3_5)" << endl;
        cin >> geo_mesh ;

        cout << "Final time ? " << endl;
        cin >> T;

        cout << "Time step? " << endl;
        cin >> Dt;
    }
    

    const ho::NL::AdvDiff_exact & data = ho::NL::AdvDiff_exact();
    const Mean_Fct mean= Mean_Fct("ari"); 

    std::string Mesh_base = "../meshes/";
    std::string Mesh_file = Mesh_base + geo_mesh +".typ1";
    const std::shared_ptr<Mesh> Th = Mesh_read(Mesh_file);

    SCH scheme = SCH(data, mean, epsilon_stab, r_expfitt, 4, 1., 0.2, 0.5, 1.*std::pow(10.,-30));
    scheme.compute_solution(Th.get(),T,Dt);    
    
    
    
    cout << "\n\nWould you like to visualise the computed solution ? \n(1 for yes, 0 for no)" << endl;
    bool visu;
    cin >> visu;
    if(visu){
        scheme.save_visu_files(Th.get(),  nb_pts_in_visu  ); 
    }    
    
    cout << "\n\nWould you like to compute information about the positivity of the solution ? \n(1 for yes, 0 for no) \nWARNING: this can take a lot of time" << endl;
    bool pos;
    cin >> pos;    
    if(pos){
        scheme.give_positivity_information(Th.get()); 
    }    

    cout << "\n\nWould you like to compute information about the long-time behviour of the solution ? \n(1 for yes, 0 for no)" << endl;
    bool longtime;
    cin >> longtime;    
    if(longtime){
        scheme.save_file_longtime(Th.get(), nb_pts_in_graph); 
    }           
    
    cout << "\n\nWould you like to compute information about the accuracy of the solution ? \n(1 for yes, 0 for no)" << endl;
    bool accuracy;
    cin >> accuracy;    
    if(accuracy){   
        int p_space;
        int q_time;

        cout << "Norm in space for the error computation ? (p in the L^q_t(L^p_x) ) ? " << endl;
        cin >> p_space;

        cout << "Norm in time for the error computation ? (q in the L^q_t(L^p_x) ) ? " << endl;
        cin >> q_time;
        
        Real norm_exact_sol=0.;
        Real rel_error = scheme.Error_rel_Lq_Lp(Th.get() , q_time , p_space, norm_exact_sol); 
        cout << "\n\nWould you like to compute information about the accuracy of the gradient of the solution (pseudo H^1 norm in space) ? \n(1 for yes, 0 for no)\n WARNING: can take a lot of time" << endl;
        bool grad;
        cin >> grad;    
        if(grad){
            Real norm_grad_exact_sol = 0.; 
            Real rel_grad_error = scheme.Error_rel_grad_Lq_L2( Th.get(), q_time , norm_grad_exact_sol ); 
            
            std::cout << "\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n " << std::endl; 
            
            std::cout << "Relative error (sol): \t " << rel_error << std::endl;         
            std::cout << "Relative error (grad): \t " << rel_grad_error << std::endl; 
            std::cout << "\nFYI: norms of exact solution." << std::endl; 
            std::cout << "Norm (sol): \t " << norm_exact_sol << std::endl;         
            std::cout << "Norm (grad): \t " << norm_grad_exact_sol  << std::endl; 
        }//if grad   
        else{
            
            std::cout << "\n ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n " << std::endl; 
            
            std::cout << "Relative error (sol): \t " << rel_error << std::endl;         
            std::cout << "\nFYI: norms of exact solution." << std::endl; 
            std::cout << "Norm (sol): \t " << norm_exact_sol << std::endl;         
        } // if not grad 


    }  // if accuracy   

    std::cout << "Done" << std::endl;
    return 0;
} // main_exact 

int main_convergence(const bool & debug){      // CV test (time-space)
    
    cout << "Covergence test for a evolutive problem" << endl;

    Real T ;
    Real Dt_ini ;

 
    string  geo_mesh ; // = "hexagonal";
    int  i_max ; //= 5;    
    int p_space; 
    int q_time; 

    if(debug){
        //geo_mesh = "mesh2";
        geo_mesh = "mesh1";
        //geo_mesh = "mesh4_1";
        //geo_mesh = "pi6_tiltedhexagonal";
        i_max = 3;
        T = 0.1;
        Dt_ini = 0.05;
        
        p_space = 2; 
        q_time = 2;

    }
    else{
        cout << "Custom simulation. Select mesh family, initial time step, final time and norms." << endl;
        cout << "Mesh geometry ? (name without _, ex : hexagonal or mesh3)" << endl;
        cin >> geo_mesh ;

        cout << "Number of meshes ? " << endl;
        cin >> i_max; 

        cout << "Final time ? " << endl;
        cin >> T;

        cout << "Time step(initial) ? " << endl;
        cin >> Dt_ini;

        cout << "Norm in space for the error computation ? (p in the L^q_t(L^p_x) ) ? " << endl;
        cin >> p_space;

        cout << "Norm in time for the error computation ? (q in the L^q_t(L^p_x) ) ? " << endl;
        cin >> q_time;

    }



    std::string Mesh_base = "../meshes/";
    std::vector<Real> Errors_sol;
    std::vector<Real> Errors_grad; 
    std::vector<Real> Meshsizes;     
    std::vector<Real> Times_comp; 
    Errors_sol.resize(i_max);
    Errors_grad.resize(i_max);
    Meshsizes.resize(i_max);
    Times_comp.resize(i_max);

    const ho::NL::AdvDiff_exact & data = ho::NL::AdvDiff_exact();        
    //const ho::NL::AdvDiff_exact_mix & data = ho::NL::AdvDiff_exact_mix();    

    const Mean_Fct mean= Mean_Fct("ari"); 

    Real dt_i = Dt_ini / std::pow(2.,DEGREE + 2); 
    
    //optimal scaling of time step ??
    //Real dt_i = Dt_ini / std::pow(2. + DEGREE,DEGREE + 2); 

    for(int i=1; i< i_max+1 ; i++){
        std::string Mesh_file = Mesh_base + geo_mesh +"_" + std::to_string(i) +".typ1";
        const std::shared_ptr<Mesh> Th_i = Mesh_read(Mesh_file);
            
        
        SCH scheme = SCH(data,mean, epsilon_stab, r_expfitt, 4, 1., 0.5, 0.5, 1.*std::pow(10.,-30));

        scheme.compute_solution(Th_i.get(),T,dt_i);
        Real norm_exact_sol=0.;
        Real norm_grad_exact_sol=0.;  
        Real rel_error = scheme.Error_rel_Lq_Lp(Th_i.get() , q_time , p_space, norm_exact_sol); 
        Real rel_grad_error = scheme.Error_rel_grad_Lq_L2( Th_i.get(), q_time , norm_grad_exact_sol ); 

        std::cout << "Relative error (sol): \t " << rel_error << std::endl;         
        std::cout << "Relative error (grad): \t " << rel_grad_error << std::endl; 
        std::cout << " \n\n\n" << std::endl; 
        Errors_sol[i-1] = rel_error;
        Errors_grad[i-1] = rel_grad_error; 
        Meshsizes[i-1] = (Th_i.get())->meshsize();
        Times_comp[i-1] = scheme.get_time_cost();
        dt_i *= 1. / std::pow(2.,DEGREE + 2); 
    } //for i 

    std::cout << "\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~ \n ~~ ~~  Computation of errors : OK ~~ ~~ \n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n "; 
    std::cout << "\nMixed HHO("<< DEGREE <<","<< DEGREE +1 <<") version, with Lehrenfeld-Schöberl stabilisation "  << std::endl;
    std::cout << " ~~~~~~~~~~~~~~~~~~~~~ \n" << std::endl;
    std::cout << " Relatives errors:" << std::endl;
    for(int j = 0; j< i_max; j++){
        std::cout << "h = " << Meshsizes[j] << " , error :\t " << Errors_sol[j] << "\t| error grad:\t " << Errors_grad[j] <<std::endl; 
    } // for j 
    std::cout << " ~~~~~~~~~~~~~~~~~~~~~ \n\n" << std::endl;
    std::cout << "Order u (L" + to_string(p_space) + ") : \t" ;
    for(int j = 1; j< i_max; j++){
        std::cout << log(Errors_sol[j]/Errors_sol[j-1]) / log(Meshsizes[j]/Meshsizes[j-1]) << "\t";
    } // for j 
    std::cout << "\nExpected order : \t " << DEGREE +2 << std::endl; 
    std::cout << " ~~~~~~~~~~~~~~~~~~~~~ \n\n" << std::endl;    
    std::cout << "Order nabla u  (L2) : \t" ;
    for(int j = 1; j< i_max; j++){
        std::cout << log(Errors_grad[j]/Errors_grad[j-1]) / log(Meshsizes[j]/Meshsizes[j-1]) << "\t";
    } // for j 
    std::cout << "\nExpected order : \t " << DEGREE +1 << std::endl; 
    std::cout << " ~~~~~~~~~~~~~~~~~~~~~ \n\n" << std::endl;


    // saving an errors graphs file  
    
    std::ofstream myfile;
    myfile.open ("errors_hho_"+to_string(DEGREE)+".csv");
    myfile << "Description\n";
    myfile <<"Convergence test"<<"," <<"advection-diffusion"<< endl;
    myfile <<"Scheme : nonlinear Mixed HHO("<< DEGREE <<";"<< DEGREE+1 <<") version with Lehrenfeld-Schöberl stabilisation and full gradient"<< endl; 
    myfile <<"Mesh family "<<"," << geo_mesh  << endl;
    myfile <<"" << endl;
    myfile <<"Errors type for solution , "<< "L^" +to_string(q_time) + "_t(L^" + to_string(p_space) + "_x)" << endl;    
    myfile <<"Errors type for gradient , "<< "L^" +to_string(q_time) + "_t(L^2_x)" << endl;
    myfile <<"" << endl;
    myfile << "Meshsize, error_sol, error_grad, time_cost\n"; 
    for(int j = 0; j< i_max; j++){
        myfile << Meshsizes[j] <<","<< Errors_sol[j] <<","<< Errors_grad[j] <<","<< Times_comp[j] <<endl;
    }//for j
    myfile.close();

    std::ofstream file_graph;
    file_graph.open ("errors_hho_"+to_string(DEGREE));
    file_graph << "Meshsize error_sol error_grad time_cost"<<std::endl;     
    for(int j = 0; j< i_max; j++){
       file_graph << Meshsizes[j] <<" "<< Errors_sol[j] <<" "<< Errors_grad[j] <<" "<< Times_comp[j] <<endl;
    }//for j
    file_graph.close();


    std::cout << "Done" << std::endl;
    return 0;
} // main_convergence

int main_positivity(const bool & debug){ //test for positivity
    
    cout << "Investigation of the 'positivity' of the numerical solution with the 'positivity breaker' test of the paper" << endl;
      
    
    string  geo_mesh ; 

    Real T ;
    Real Dt ;
    
    if(debug){
        cout << "Test case corresponding to the 'structure preservation' article" << endl;

        geo_mesh = "pi6_tiltedhexagonal_5"; 
        //geo_mesh = "mesh_quad_2"; 
        //geo_mesh = "mesh1_6"; 

        T =  5.*std::pow(10.,-4);
        //T =  std::pow(10.,-4);
        Dt = std::pow(10,-5); 
    }
    else{
        cout << "Custom simulation. Select mesh, time step and final time." << endl;
        cout << "Mesh ? (name with number, ex : hexagonal_3 or mesh3_5)" << endl;
        cin >> geo_mesh ;

        cout << "Final time ? " << endl;
        cin >> T;

        cout << "Time step? " << endl;
        cin >> Dt;
    }
    

    const ho::NL::AdvDiff_pos & data = ho::NL::AdvDiff_pos();
    const Mean_Fct mean= Mean_Fct("ari"); 

    std::string Mesh_base = "../meshes/";
    std::string Mesh_file = Mesh_base + geo_mesh +".typ1";
    const std::shared_ptr<Mesh> Th = Mesh_read(Mesh_file);

    SCH scheme = SCH(data, mean, epsilon_stab, r_expfitt, 4, 1., 0.5, 0.5, 1.*std::pow(10.,-30));
    scheme.compute_solution(Th.get(),T,Dt);
    scheme.give_positivity_information( Th.get(), true); 
    //scheme.save_visu_files(Th.get(),  nb_pts_in_visu  ); 
    
        
    
    cout << "\n\nWould you like to visualise the computed solution ? \n(1 for yes, 0 for no)" << endl;
    bool visu;
    cin >> visu;
    if(visu){
        scheme.save_visu_files(Th.get(),  nb_pts_in_visu  ); 
    }    
    
    cout << "\n\nWould you like to compute information about the long-time behviour of the solution ? \n(1 for yes, 0 for no)" << endl;
    bool longtime;
    cin >> longtime;    
    if(longtime){
        scheme.save_file_longtime(Th.get(), nb_pts_in_graph); 
    }   
    // Real norm_exact_sol = 0.; 
    // Real rel_error = scheme.Error_rel_Lq_Lp(Th.get() , -1,2,data.u, norm_exact_sol); 
    
    // std::cout << "Norm of the exact solution : \t " << norm_exact_sol << std::endl; 
    // std::cout << "Relative error : \t " << rel_error << std::endl; 
    //scheme.save_file_longtime(Th.get(), nb_pts_in_graph); 

    //scheme.debug_exact_sol(Th.get(), T, nb_pts_in_visu/10);

    //scheme.debug_Gaudeul_test(Th.get(),Dt,2,3); 

    //scheme.debug_loc_Gaudeul_test_condensed(Th.get(), Dt, 3, 3);
    
    //scheme.debug_loc_Gaudeul_test(Th.get(), Dt, 3, 3);

    // scheme.debug_loc_Gaudeul_test_by_block(Th.get(),Dt, 1 , 2,2);
    // scheme.debug_loc_Gaudeul_test_by_block(Th.get(),Dt, 2 , 2,2);
    // scheme.debug_loc_Gaudeul_test_by_block(Th.get(),Dt, 3 , 2,2);
    // scheme.debug_loc_Gaudeul_test_by_block(Th.get(),Dt, 4 , 2,2);

    std::cout << "Done" << std::endl;
    return 0;
} // main_positivity


int main_nonpoly(const bool & debug){ // nonpoly | longtime behaviour 
 
    cout << "Investigation of long-time behaviour of the numerical solution with non-polynomial advection potential" << endl;
    
    string  geo_mesh ; //= "pi6_tiltedhexagonal_2"; // = "hexagonal";
    //int  i_max ; //= 5;

    Real T ;
    Real Dt ;
    
    if(debug){
        cout << "Test case corresponding to the 'structure preservation' article" << endl;
        //geo_mesh = "mesh2_2";
        //geo_mesh = "mesh_quad_2";
        geo_mesh = "mesh_quad_2";
        //geo_mesh = "hexagonal_1";
        //geo_mesh = "mesh7";        
        //geo_mesh = "pi6_tiltedhexagonal_2"; 
        T = 5.;
        Dt = 0.2;
    }
    else{
        cout << "Custom simulation. Select mesh, time step and final time." << endl;
        cout << "Mesh ? (name with number, ex : hexagonal_3 or mesh3_5)" << endl;
        cin >> geo_mesh ;

        cout << "Final time ? " << endl;
        cin >> T;

        cout << "Time step? " << endl;
        cin >> Dt;
    }
    

    const ho::NL::AdvDiff_nonpoly & data = ho::NL::AdvDiff_nonpoly();
    const Mean_Fct mean= Mean_Fct("ari"); 

    std::string Mesh_base = "../meshes/";
    std::string Mesh_file = Mesh_base + geo_mesh +".typ1";
    const std::shared_ptr<Mesh> Th = Mesh_read(Mesh_file);

    SCH scheme = SCH(data, mean, epsilon_stab, r_expfitt, 4, 1., 0.2, 0.5, 1.*std::pow(10.,-30));
    scheme.compute_solution(Th.get(),T,Dt);
   
    scheme.save_file_longtime(Th.get(), nb_pts_in_graph);        
     
    cout << "\n\nWould you like to visualise the computed solution ? \n(1 for yes, 0 for no)" << endl;
    bool visu;
    cin >> visu;
    if(visu){
        scheme.save_visu_files(Th.get(),  nb_pts_in_visu  ); 
    }    
    
    cout << "\n\nWould you like to compute information about the positivity of the solution ? \n(1 for yes, 0 for no) \nWARNING: this can take a lot of time" << endl;
    bool pos;
    cin >> pos;    
    if(pos){
        scheme.give_positivity_information(Th.get()); 
    }    



    std::cout << "Done" << std::endl;
    return 0;
} // main_nonpoly



//
// mesh reader, returns a pointer towards the Mesh 
std::shared_ptr<Mesh> Mesh_read(const string & Mesh_file){

    std::string sep = "\n----------------------------------------\n";
    Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
    std::cout << "Reading the mesh "  << std::endl;

    // Read options and data
    //GetPot options(argc, argv);
    std::string mesh_file = Mesh_file;

    // Pretty-print floating point numbers
    std::cout.precision(2);
    std::cout.flags(std::ios_base::scientific);

    std::cout << "Mesh file name : " << Mesh_file << std::endl;
    // Create mesh
    std::shared_ptr<Mesh> Th(new Mesh());
    if(mesh_file.find("typ1")!=std::string::npos)
        Th->readFVCA5File(mesh_file.c_str());
    else if(mesh_file.find("dgm")!=std::string::npos)
        Th->readDGMFile(mesh_file.c_str());
    else {
        std::cerr << "Unknown mesh format" << std::endl;
        exit(1);
    }

    std::cout << "Mesh created"  << std::endl;

    Th->buildFaceGroups();
    std::cout << "MESH: " << mesh_file << std::endl;
    std::cout << FORMAT(50) << "num_cells" << Th->numberOfCells() << std::endl;
    std::cout << FORMAT(50) << "num_faces" << Th->numberOfFaces() << std::endl;
    std::cout << FORMAT(50) << "num_unknws_cells" << Th->numberOfCells() * LC::nb_cell_dofs << std::endl;
    std::cout << FORMAT(50) << "num_unknws_faces" << Th->numberOfInternalFaces() * LC::nb_local_face_dofs + Th->numberOfBoundaryFaces() * LC::nb_local_face_dofs << std::endl;
    
    //------------------------------------------------------------------------------
    // Estimate mesh size
    Real h = Th->meshsize();
    std::cout << FORMAT(50) << "meshsize" << h << std::endl;

    return Th;
}






///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
