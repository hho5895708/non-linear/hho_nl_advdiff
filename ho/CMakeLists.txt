ADD_EXECUTABLE(hho_nl_advdiff ./ho_nl.cpp)

TARGET_LINK_LIBRARIES(hho_nl_advdiff Basis Mesh QuadratureRules Postproc ContinuousData Scheme)

