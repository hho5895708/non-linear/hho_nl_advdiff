# hho_nl_advdiff

## README

### Description

The code hho_nl_advfiff is an implementation of a nonlinear mixed HHO scheme for a linear advection-diffusion equation.

This code was developped in order to investigate the interest of using nonlinear high-order schemes to discretise dissipative problems.
It computes a numerical approximation of a transcient anisotropic advection-diffusion equation.
The nonlinear discretisation used ensure that the Boltzmann entropy structure of the continuous problem is conserved at the discrete level, and therefore ensure (among others features) that the computed solution is positive.

This code was used to get some numerical results presented in a [proceeding](https://hal.science/hal-04036599) as well as an [article](https://hal.science/hal-04250412) about structure-preservation with high-order schemes. 

While implemented on a linear problem, this scheme is intended to be used as a basis for methods on more complicated problems, including semiconductor models and cross-diffusion systems. 

#### Installation
 
The following softwares/libraries are required for compilation:

* C++
* CMake
* Eigen
* Boost 

To compile the code, you should create a build directory.


mkdir build                 <br>
cd build                    <br>
cmake ..                    <br>
make                        

The compilation will create a bin directory, in which you can find and execute "hho_nl_advdiff".
In order to get some data about visualisation (solution profiles), you should create a "profiles" directory in the build directory.

mkdir profiles               <br>
./bin/hho_nl_advdiff      <br>


### Getting started

The degree(s) of polynomial unknowns has ti be specified in "hho_nl.cpp", in the constant DEGREE, and is thus fixed at the compilation.

By executing hho_nl_advdiff, you will be able to launch various simulations.
You just have to follow the different instructions given on the console. <br>
You can currently use 4 differents "modes", by enterring the associated keyword:
* "convergence": a convergence testthanks to an explicit solution, will generated errors graphs data and print the experimental order of convergence; 
* "expli": a test case with explicit solution, for which you can get the accuracy of the computed solution with respect to the exact one;
* "posi": a test case design to challenge the positivity of linear scheme, the expression of the solution is not explicit; 
* "nonpoly": a test case with non-polynomial advection potential, the expression of the solution is not explicit.   <br> 

For the three last tests, you can also chose to visualise the solution, compute the minimal values reached and compute (and save associated files) information about the long-time behaviour of the solution.

The data of the problem (initial condition, source term, boundary values, physical data) can be specified in the ExactSolution constructor (file Data.h).
Some specific test case already available in this file.<br>

The visualisation files (.vtu format) can be read directcly using Paraview. 
The othfiles time_hhokare data files about the long-time behaviour, while errors_hho_k are related to errors with respect to exact solution and convergence graphs.


### Authors 

Author: Julien Moatti (julien.moatti@tuwien.ac.at)

### License

GNU General Public License v3.0

### Project status

This code is not a final version, and is being updated from time to time. 
French-language comments are related to features that will be implemented (hopefully) soon. 

!!! The version of this code is designed to use with Neumann boundary conditions only !!! <br> 
The scheme should (not yet) compute correct solutions with Dirichlet boundary conditions. 
