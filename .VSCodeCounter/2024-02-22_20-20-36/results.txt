Date : 2024-02-22 20:20:36
Directory : /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme
Total : 16 files,  3200 codes, 1733 comments, 1074 blanks, all 6007 lines

Languages
+----------+------------+------------+------------+------------+------------+
| language | files      | code       | comment    | blank      | total      |
+----------+------------+------------+------------+------------+------------+
| C++      |         16 |      3,200 |      1,733 |      1,074 |      6,007 |
+----------+------------+------------+------------+------------+------------+

Directories
+--------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| path                                                                           | files      | code       | comment    | blank      | total      |
+--------------------------------------------------------------------------------+------------+------------+------------+------------+------------+
| .                                                                              |         16 |      3,200 |      1,733 |      1,074 |      6,007 |
+--------------------------------------------------------------------------------+------------+------------+------------+------------+------------+

Files
+--------------------------------------------------------------------------------+----------+------------+------------+------------+------------+
| filename                                                                       | language | code       | comment    | blank      | total      |
+--------------------------------------------------------------------------------+----------+------------+------------+------------+------------+
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/DiscreteData.cpp        | C++      |          1 |        721 |          1 |        723 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/DiscreteData.h          | C++      |        792 |        160 |        204 |      1,156 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/GlobalContributions.cpp | C++      |          1 |          0 |          2 |          3 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/GlobalContributions.h   | C++      |        154 |         13 |         52 |        219 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/GlobalSystem.cpp        | C++      |          1 |          0 |          0 |          1 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/GlobalSystem.h          | C++      |        141 |         49 |         56 |        246 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/LocalContributions.cpp  | C++      |          1 |        360 |          0 |        361 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/LocalContributions.h    | C++      |        313 |         86 |        125 |        524 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/Mean.cpp                | C++      |        135 |          1 |         14 |        150 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/Mean.h                  | C++      |         19 |          0 |         10 |         29 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/NLcontributions.cpp     | C++      |          1 |          0 |          2 |          3 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/NLcontributions.h       | C++      |        418 |        136 |        162 |        716 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/NewtonMethod.cpp        | C++      |          1 |          0 |          3 |          4 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/NewtonMethod.h          | C++      |        538 |         81 |        211 |        830 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/Schemes.cpp             | C++      |          1 |          0 |          2 |          3 |
| /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme/Schemes.h               | C++      |        683 |        126 |        230 |      1,039 |
| Total                                                                          |          |      3,200 |      1,733 |      1,074 |      6,007 |
+--------------------------------------------------------------------------------+----------+------------+------------+------------+------------+