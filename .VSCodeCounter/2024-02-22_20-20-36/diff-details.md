# Diff Details

Date : 2024-02-22 20:20:36

Directory /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme

Total : 17 files,  2831 codes, 1660 comments, 916 blanks, all 5407 lines

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [ho/ho_nl.cpp](/ho/ho_nl.cpp) | C++ | -369 | -73 | -158 | -600 |
| [src/Scheme/DiscreteData.cpp](/src/Scheme/DiscreteData.cpp) | C++ | 1 | 721 | 1 | 723 |
| [src/Scheme/DiscreteData.h](/src/Scheme/DiscreteData.h) | C++ | 792 | 160 | 204 | 1,156 |
| [src/Scheme/GlobalContributions.cpp](/src/Scheme/GlobalContributions.cpp) | C++ | 1 | 0 | 2 | 3 |
| [src/Scheme/GlobalContributions.h](/src/Scheme/GlobalContributions.h) | C++ | 154 | 13 | 52 | 219 |
| [src/Scheme/GlobalSystem.cpp](/src/Scheme/GlobalSystem.cpp) | C++ | 1 | 0 | 0 | 1 |
| [src/Scheme/GlobalSystem.h](/src/Scheme/GlobalSystem.h) | C++ | 141 | 49 | 56 | 246 |
| [src/Scheme/LocalContributions.cpp](/src/Scheme/LocalContributions.cpp) | C++ | 1 | 360 | 0 | 361 |
| [src/Scheme/LocalContributions.h](/src/Scheme/LocalContributions.h) | C++ | 313 | 86 | 125 | 524 |
| [src/Scheme/Mean.cpp](/src/Scheme/Mean.cpp) | C++ | 135 | 1 | 14 | 150 |
| [src/Scheme/Mean.h](/src/Scheme/Mean.h) | C++ | 19 | 0 | 10 | 29 |
| [src/Scheme/NLcontributions.cpp](/src/Scheme/NLcontributions.cpp) | C++ | 1 | 0 | 2 | 3 |
| [src/Scheme/NLcontributions.h](/src/Scheme/NLcontributions.h) | C++ | 418 | 136 | 162 | 716 |
| [src/Scheme/NewtonMethod.cpp](/src/Scheme/NewtonMethod.cpp) | C++ | 1 | 0 | 3 | 4 |
| [src/Scheme/NewtonMethod.h](/src/Scheme/NewtonMethod.h) | C++ | 538 | 81 | 211 | 830 |
| [src/Scheme/Schemes.cpp](/src/Scheme/Schemes.cpp) | C++ | 1 | 0 | 2 | 3 |
| [src/Scheme/Schemes.h](/src/Scheme/Schemes.h) | C++ | 683 | 126 | 230 | 1,039 |

[Summary](results.md) / [Details](details.md) / [Diff Summary](diff.md) / Diff Details