# Summary

Date : 2024-02-22 20:20:36

Directory /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme

Total : 16 files,  3200 codes, 1733 comments, 1074 blanks, all 6007 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C++ | 16 | 3,200 | 1,733 | 1,074 | 6,007 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 16 | 3,200 | 1,733 | 1,074 | 6,007 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)