# Diff Summary

Date : 2024-02-22 20:20:36

Directory /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/Scheme

Total : 17 files,  2831 codes, 1660 comments, 916 blanks, all 5407 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C++ | 17 | 2,831 | 1,660 | 916 | 5,407 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 17 | 2,831 | 1,660 | 916 | 5,407 |
| . (Files) | 16 | 3,200 | 1,733 | 1,074 | 6,007 |
| .. | 1 | -369 | -73 | -158 | -600 |
| ../.. | 1 | -369 | -73 | -158 | -600 |
| ../../ho | 1 | -369 | -73 | -158 | -600 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)