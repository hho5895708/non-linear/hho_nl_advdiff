# Diff Summary

Date : 2024-02-22 20:20:57

Directory /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/ContinuousData

Total : 20 files,  -2709 codes, -1673 comments, -921 blanks, all -5303 lines

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C++ | 20 | -2,709 | -1,673 | -921 | -5,303 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 20 | -2,709 | -1,673 | -921 | -5,303 |
| . (Files) | 4 | 491 | 60 | 153 | 704 |
| .. | 16 | -3,200 | -1,733 | -1,074 | -6,007 |
| ../Scheme | 16 | -3,200 | -1,733 | -1,074 | -6,007 |

[Summary](results.md) / [Details](details.md) / Diff Summary / [Diff Details](diff-details.md)