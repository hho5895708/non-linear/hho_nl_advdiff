# Summary

Date : 2024-02-22 20:20:57

Directory /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/ContinuousData

Total : 4 files,  491 codes, 60 comments, 153 blanks, all 704 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C++ | 4 | 491 | 60 | 153 | 704 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 4 | 491 | 60 | 153 | 704 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)