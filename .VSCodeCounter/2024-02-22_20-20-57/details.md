# Details

Date : 2024-02-22 20:20:57

Directory /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/src/ContinuousData

Total : 4 files,  491 codes, 60 comments, 153 blanks, all 704 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [src/ContinuousData/Data.cpp](/src/ContinuousData/Data.cpp) | C++ | 1 | 0 | 1 | 2 |
| [src/ContinuousData/Data.h](/src/ContinuousData/Data.h) | C++ | 329 | 58 | 104 | 491 |
| [src/ContinuousData/Stat.cpp](/src/ContinuousData/Stat.cpp) | C++ | 123 | 2 | 38 | 163 |
| [src/ContinuousData/Stat.h](/src/ContinuousData/Stat.h) | C++ | 38 | 0 | 10 | 48 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)