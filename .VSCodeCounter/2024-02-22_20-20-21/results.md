# Summary

Date : 2024-02-22 20:20:21

Directory /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/ho

Total : 1 files,  369 codes, 73 comments, 158 blanks, all 600 lines

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Languages
| language | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| C++ | 1 | 369 | 73 | 158 | 600 |

## Directories
| path | files | code | comment | blank | total |
| :--- | ---: | ---: | ---: | ---: | ---: |
| . | 1 | 369 | 73 | 158 | 600 |

Summary / [Details](details.md) / [Diff Summary](diff.md) / [Diff Details](diff-details.md)