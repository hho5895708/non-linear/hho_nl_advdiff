# Details

Date : 2024-02-22 20:20:21

Directory /home/julien/Code/[7] HHO_nl/hho_nl_advdiff/ho

Total : 1 files,  369 codes, 73 comments, 158 blanks, all 600 lines

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)

## Files
| filename | language | code | comment | blank | total |
| :--- | :--- | ---: | ---: | ---: | ---: |
| [ho/ho_nl.cpp](/ho/ho_nl.cpp) | C++ | 369 | 73 | 158 | 600 |

[Summary](results.md) / Details / [Diff Summary](diff.md) / [Diff Details](diff-details.md)