#ifndef QR_HPP
#define QR_HPP 1

#include "Common/point.hpp"

namespace fe {
  using namespace common;
  /*!
    \class QuadratureRule
    \author Daniele A. Di Pietro
    \date 2006-11-20
    \brief Quadrature rules
  */
  template<typename T>
  class QuadratureRule {
  public:
    //! Real type
    typedef T real_type;
    //! Node type
    typedef common::Point<T> node_type;
    //! Node vector
    typedef std::vector<node_type> node_vector;
    //! Weight vector
    typedef std::vector<T> weight_vector;

    //! Construct the quadrature using
    /*!
      \param N the number of points
      \param doe the degree of exactness
    */
    QuadratureRule(int N, int doe = 0) : M_N(N), M_doe(doe), M_P(N), M_w(N) {}
    //! Construct the quadrature using
    /*! 
      \param N the number of points
      \param doe the degree of exactness
      \param P a C-style vector of nodes
      \param w a C-style vector of weights
    */
    QuadratureRule(int N, int doe, node_type* P, real_type* w);

    //! Return the number of nodes
    inline int& numberOfNodes(){ return M_N; }
    inline const int& numberOfNodes() const { return M_N; }
    //! Return the degree of exactness
    inline int& doe(){ return M_doe; }
    inline const int& doe() const { return M_doe; }
    //! Return the node vector
    inline node_vector& nodes(){ return M_P; }
    inline const node_vector& nodes() const { return M_P; }
    //! Return the i-th node
    inline node_type& node(int i) { return M_P[i]; }
    inline const node_type& node(int i) const { return M_P[i]; }
    //!Return the weight vector
    inline weight_vector& weight(){ return M_w; }
    inline const weight_vector& weight() const { return M_w; }
    //! Return the i-th weight
    inline real_type& weight(int i) { return M_w[i]; }
    inline const real_type& weight(int i) const { return M_w[i]; }

  private:
    //! Number of nodes
    int M_N;
    //! Degree of exactness
    int M_doe;
    //! Node vector
    node_vector M_P;
    //! Weight vector
    weight_vector M_w;

  protected:
    void _setPW(node_type* P, real_type* w);
  };

  ////////////////////////////////////////////////////////////
  // Implementation

  template<typename T>
  QuadratureRule<T>::QuadratureRule(int N, int doe, node_type* P, real_type* w) : 
    M_N(N), 
    M_doe(doe),
    M_P(N), 
    M_w(N) {
    node_type* p1 = P;
    real_type* p2 = w;
    for(int i = 0; i < M_N; i++, p1++, p2++) {
      M_P[i] = *(p1);
      M_w[i] = *(w);
    }
  }

  template<typename T>
  void QuadratureRule<T>::_setPW(node_type* P, real_type* w) {
    for(int i = 0; i < this->numberOfNodes(); i++) {
      M_P[i] = P[i];
      M_w[i] = w[i];
    }
  }

  template<typename T>
  std::ostream& operator<<(std::ostream& ostr, const fe::QuadratureRule<T>& qr) {
    // Print nodes
    ostr << "Nodes" << std::endl;
    for (int i = 0; i < qr.numberOfNodes(); i++)
      ostr << "[" << i << "] " << qr.node(i) << std::endl;
    // Print weights
    ostr << "Weights" << std::endl;
    for (int i = 0; i < qr.numberOfNodes(); i++) {
      ostr << "[" << i << "] " << qr.weight(i) << std::flush;
      if (i < qr.numberOfNodes() - 1) ostr << std::endl;
    }
    return ostr;
  }
}

#endif
