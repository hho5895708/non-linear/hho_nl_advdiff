#include "DunavantQuadratureRule.h"

#include "triangle_dunavant_rule.hpp"

DunavantQuadratureRule::DunavantQuadratureRule(const int & doe)
{
  int rule_num = dunavant_rule_num();
  int rule, order_num, degree;
  for(rule = 1; rule <= rule_num; rule++) {
    degree = dunavant_degree(rule);
    if(degree >= doe) break;
  } // for rule
  assert(rule != rule_num or degree >= doe);

  order_num = dunavant_order_num(rule);
  std::vector<Real> xytab(2*order_num), wtab(order_num);
  m_nb_nodes = order_num;
  dunavant_rule(rule, order_num, &xytab[0], &wtab[0]);

  m_points.resize(m_nb_nodes);
  m_weights.resize(m_nb_nodes);
  for(int iQN = 0; iQN < m_nb_nodes; iQN++) {
    Point xQN; xQN(0) = xytab[0+iQN*2]; xQN(1) = xytab[1+iQN*2];
    m_points[iQN] = xQN;
    m_weights[iQN] = wtab[iQN];
  } // for iQN
}
