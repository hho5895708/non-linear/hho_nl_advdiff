// -*- C++ -*-
#ifndef DUNAVANTQUADRATURERULE_H
#define DUNAVANTQUADRATURERULE_H

#include <Eigen/Dense>

#include "Mesh/Mesh.h"

class DunavantQuadratureRule
{
public:
  DunavantQuadratureRule(const int & doe);

  inline const std::vector<Point> & points() const { return m_points; }
  inline const std::vector<double> & weights() const { return m_weights; }
  inline const Point & point(const std::size_t & iQN) const { return m_points[iQN]; }
  inline const double & weight(const std::size_t & iQN) const { return m_weights[iQN]; }
  inline int numberOfNodes() const { return m_nb_nodes; }

private:
  std::vector<Point> m_points;
  std::vector<Real> m_weights;
  int m_nb_nodes;
};

#endif
