// -*- C++ -*-
#ifndef BASIS_H
#define BASIS_H

#include <array>
#include <functional>

#include "Common/defs.h"

namespace ho
{
  namespace detail
  {
    template<unsigned int n>
    struct Factorial {
      enum { value = n * Factorial<n-1>::value };
    };

    template<>
    struct Factorial<0> {
      enum { value = 1 };
    };

    template<unsigned int n, unsigned int k>
    struct Binomial {
      enum { value = Factorial<n>::value / (Factorial<n-k>::value * Factorial<k>::value) };
    };
  } // namespace detail

  //------------------------------------------------------------------------------


  template<int K>
  class HierarchicalBasis2d
  {
  public:
    typedef boost::numeric::ublas::bounded_vector<Real, 2> GradientType;

    typedef  std::function<Real(const Point &)> BasisFunctionType;
    typedef  std::function<GradientType(const Point &)> BasisGradientType;

    struct BasisFunction 
    {
      BasisFunctionType phi;
      BasisGradientType dphi;
    };

    class Evaluation {
    public:
      Evaluation(const HierarchicalBasis2d<K> * B, 
                 const std::vector<Point> & points, 
                 int first = 0, 
                 int last = HierarchicalBasis2d<K>::size);

      const std::vector<RealVector> & Phi() const { return m_eval_phi; }
      const RealVector & phi(int i) const { return m_eval_phi[i]; }
      const std::vector<RealMatrix> & dPhi() const { return m_eval_dphi; }
      const RealMatrix & dphi(int i) const { return m_eval_dphi[i]; }

    private:
      std::vector<RealVector> m_eval_phi;
      std::vector<RealMatrix> m_eval_dphi;
    };

    enum { size = detail::Binomial<K+2,2>::value };

    typedef std::array<BasisFunction, HierarchicalBasis2d<K>::size> BasisFunctionArray;

    HierarchicalBasis2d(const Point & xT, const Real & hT);

    inline const BasisFunctionArray & Phi() const { return m_Phi; }
    inline const BasisFunction & phi(int i) const { return m_Phi[i]; }
    
  private:
    Point m_xT;
    Real m_hT;
    BasisFunctionArray m_Phi;
  };

  //------------------------------------------------------------------------------
  // Implementation

  template<int K>
  HierarchicalBasis2d<K>::HierarchicalBasis2d(const Point & xT, const Real & hT)
    : m_xT(xT)
    , m_hT(hT)
  {
    int i_phi = 0;
    for(int k = 0; k <= K; k++) {
      for(int i = 0; i <= k; i++, i_phi++) {
        BasisFunctionType phi = [i,k,this](const Point & x) -> Real { 
          Real _x = (x(0)-this->m_xT(0))/this->m_hT;
          Real _y = (x(1)-this->m_xT(1))/this->m_hT;
          return std::pow(_x,i) * std::pow(_y,k-i);
        };
        BasisGradientType dphi = [i,k,this](const Point & x) -> GradientType {
          GradientType g;
          Real _x = (x(0)-this->m_xT(0))/this->m_hT;
          Real _y = (x(1)-this->m_xT(1))/this->m_hT;
          g(0) = (i == 0) ? 0. : i*std::pow(_x,i-1)/m_hT * std::pow(_y,k-i);
          g(1) = (i == k) ? 0. : std::pow(_x,i) * (k-i) * std::pow(_y,k-i-1)/m_hT;
          return g;
        };
        m_Phi[i_phi].phi = phi;
        m_Phi[i_phi].dphi = dphi;
      }
    }
  }

  template<int K>
  HierarchicalBasis2d<K>::Evaluation::Evaluation(const HierarchicalBasis2d<K> * B, 
                                                 const std::vector<Point> & points, 
                                                 int first, 
                                                 int last)
    : m_eval_phi(points.size())
    , m_eval_dphi(points.size())
  {
    assert(last > first && first >= 0 && last <= HierarchicalBasis2d<K>::size);

    int offset = last - first;
    int nb_points = points.size();

    for(int l = 0; l < nb_points; l++) {
      const Point & xl = points[l];
      RealVector & eval_phi_l = m_eval_phi[l];
      RealMatrix & eval_dphi_l = m_eval_dphi[l];

      eval_phi_l.resize(offset);
      eval_dphi_l.resize(offset, 2);

      int i_phi = 0;
      for(int i = first; i < last; i++, i_phi++) {
        eval_phi_l(i_phi) = B->phi(i).phi(xl);
        RealMatrixRow eval_dphi_li(eval_dphi_l, i_phi);
        eval_dphi_li = B->phi(i).dphi(xl);
      }
    }
  }

  //------------------------------------------------------------------------------

  template<int K>
  class HierarchicalBasis1d
  {
  public:
    typedef std::function<Real(const Point &)> BasisFunctionType;

    enum { size = K+1 };

    typedef std::array<BasisFunctionType, HierarchicalBasis1d<K>::size> BasisFunctionArray;

    class Evaluation {
    public:
      Evaluation(const HierarchicalBasis1d<K> * B, 
                 const std::vector<Point> & points);

      const std::vector<RealVector> & Phi() const { return m_eval_phi; }
      const RealVector & phi(int i) const { return m_eval_phi[i]; }

    private:
      std::vector<RealVector> m_eval_phi;
    };

    HierarchicalBasis1d(const Point & x0, const Point & xF, const Real & hF);

    inline const BasisFunctionArray & Phi() const { return m_Phi; }
    inline const BasisFunctionType & phi(int i) const { return m_Phi[i]; }

  private:
    Point m_x0;
    Point m_xF;
    Real m_hF;
    BasisFunctionArray m_Phi;
  };

  //------------------------------------------------------------------------------
  // Implementation

  template<int K>
  HierarchicalBasis1d<K>::HierarchicalBasis1d(const Point & x0, const Point & xF, const Real & hF)
    : m_x0(x0)
    , m_xF(xF)
    , m_hF(hF)
  {
    for(int k = 0; k <= K; k++) {
      BasisFunctionType phi = [k,this](const Point & x) -> Real {
        Real d = inner_prod(x-this->m_xF, this->m_x0-this->m_xF)/(std::pow(this->m_hF, 2));
        return std::pow(d, k);
      };
      m_Phi[k] = phi;
    }
  }

  template<int K>
  HierarchicalBasis1d<K>::Evaluation::Evaluation(const HierarchicalBasis1d<K> * B,
                                                 const std::vector<Point> & points)
    : m_eval_phi(points.size())
  {
    int nb_points = points.size();

    for(int l = 0; l < nb_points; l++) {
      const Point & xl = points[l];

      RealVector & eval_phi_l = m_eval_phi[l];
      
      eval_phi_l.resize(HierarchicalBasis1d<K>::size);

      for(int i = 0; i < HierarchicalBasis1d<K>::size; i++) {
        eval_phi_l[i] = B->phi(i)(xl);
      }
    }
  }

} // namespace ho
#endif
