#ifndef SCHEMES_H
#define SCHEMES_H

#include "NewtonMethod.h"


namespace ho{
namespace NL{



template<std::size_t K>
class Scheme
{
    typedef ho::diffusion::LocalContrib<K> LC;
    typedef ho::diffusion::GlobalContrib<K> GC;
    typedef ho::GlobalSys<K> GS;
    typedef ho::DiscUnkw<K> DU;
    typedef ho::NL::NewtonMethod<K> NMethod;     
    
    
    public:
    //
    //Constructor for the scheme
    Scheme( const DataEvol & data,  
            const Mean_Fct & mean,
            const Real & eps_stab = 0., 
            const Real & r = 1.,  
            const int & add_integrator_val = 3,
            const Real & ratio_dt_max = 1., 
            const Real & increase_time_step =  0.5,
            const Real & reduce_time_step = 0.5,
            const Real & dt_critical = 1.*std::pow(10., -30) );
    //
    // use the sheme to compute the solution
    void compute_solution(const Mesh * Th, const Real & T_end ,   const Real & time_step );    
    //
    // get the computational time of the scheme 
    Real get_time_cost() const;
    //
    // generate visualisation files (profiles) from the computed solution 
    void save_visu_files(const Mesh * Th, const int & nb_points , const std::string & name ="") const;    
    // compute and return the relative L^q_t(L^p) error ( for q=-1, uniform norm in time ) | Rmk: do not take into account the initial time t=0
    Real Error_rel_Lq_Lp(   const Mesh * Th,
                            const int & q_time, 
                            const int & p_space, 
                            Real & norm_ex ) const ;    
    //    
    // compute and return the relative L^q_t(L^2) error on the gradient ( for q=-1, uniform norm in time ) | Rmk: do not take into account the initial time t=0
    Real Error_rel_grad_Lq_L2(  const Mesh * Th,
                                const int & q_time, 
                                Real & norm_ex ) const ;    
    //                                              
    // generate files with the long-time information (distance to the thermal equilibrium). Warning: do not use with non-Boltzmann stat !!!
    void save_file_longtime(const Mesh * Th, const int & nb_points_graphs , const std::string & name ="") const ;
    //                                              
    // give information about the positivity. Warning: do not use with non-Boltzmann stat !!!
    void give_positivity_information(const Mesh * Th , const bool & faces_info = false) const ;  
    
    
    // ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~
    // Debug features 
    //
    // generate visualisation files (profiles) from the exact solution 
    void debug_exact_sol(const Mesh * Th, const Real & T_end, const int & nb_points ) const;    
    //
    // performs a Jacobian test à la Gaudeul (global, takes into account the static condensation)
    void debug_Gaudeul_test(const Mesh * Th,
                            const Real & time_step,
                            const int nb_pts_base,
                            const int nb_points_dir) const;     
    //
    // performs a Jacobian test à la Gaudeul, with local static condensation
    void debug_loc_Gaudeul_test_condensed(const Mesh * Th,
                                const Real & time_step,
                                const int nb_pts_base,
                                const int nb_points_dir) const;       
    //
    // performs a Jacobian test à la Gaudeul (local, takes into account the static condensation)
    void debug_loc_Gaudeul_test(const Mesh * Th,
                                const Real & time_step,
                                const int nb_pts_base,
                                const int nb_points_dir) const;                          
    //
    // performs a Jacobian test à la Gaudeul (local, takes into account only one block of the Jacobian)
    void debug_loc_Gaudeul_test_by_block(   const Mesh * Th,
                                            const Real & time_step,
                                            const int id_block, 
                                            const int nb_pts_base,
                                            const int nb_points_dir) const;
    //
    // generate files from the computed solution for the study of the long-time behaviour 
    //void save_longtime_files(const Mesh * Th, const int & nb_pts_in_graph);    
    
    //


    private: 
    DataEvol m_data; 
    Mean_Fct m_mean; 

    Real m_eps_stab_ini; 
    Real m_add_integrator;
    Real m_ratio_dt_max;            
    Real m_increase_time_step;
    Real m_reduce_time_step; 
    
    Real m_r; 
    GC m_gc;
    DU m_Phi; 

    Real m_dt_critical; 

    Real m_T_end;

    std::vector<Real> m_time;
    std::vector<int> m_nb_iter_Newton;
    std::vector<int> m_nb_resol_cumul;
    int m_nb_resol_sys_lin ;
    std::vector<DU> m_Sol;
    


    Real m_time_tot;
    Real m_time_precomputation;
    Real m_time_computation; 

    bool m_global_contrib_computed; 
    bool m_computations_done; 

};



//------------------------------------------------------------------------------
// Implementation Scheme

template<std::size_t K>
Scheme<K>::Scheme(  const DataEvol & data, 
                    const Mean_Fct & mean,              
                    const Real & eps_stab, 
                    const Real & r, 
                    const int & add_integrator_val ,
                    const Real & ratio_dt_max , 
                    const Real & increase_time_step ,
                    const Real & reduce_time_step ,
                    const Real & dt_critical  )
{
    m_data = data;
    m_mean = mean; 
    
    m_add_integrator = add_integrator_val;
    m_ratio_dt_max = ratio_dt_max;
    m_increase_time_step = increase_time_step;
    m_reduce_time_step = reduce_time_step;
    m_dt_critical = dt_critical;

    m_r = r;     
    m_eps_stab_ini = eps_stab;  
    
    m_time_tot = 0.;
    m_time_precomputation = 0.;
    m_time_computation = 0.;    
    
    m_global_contrib_computed = false; 
    m_computations_done = false; 

} //Scheme, constructor 

template<std::size_t K>
void Scheme<K>::compute_solution(const Mesh * Th, const Real & T_end , const Real & time_step )
{
    //clear the scheme data - save some important data 
    m_T_end = T_end; 
    m_Sol.clear();
    m_time.clear(); 
    m_nb_iter_Newton.clear();
    m_nb_resol_cumul.clear();
    m_nb_resol_sys_lin = 0.; 
    m_computations_done = false; 

    // execute the scheme 

    std::string sep = "\n----------------------------------------\n";
    Eigen::IOFormat CleanFmt(4, 0, ", ", "\n", "[", "]");
    std::cout << "Mixed HHO("<< K <<","<< K+1 <<") version, with Lehrenfeld-Schöberl stabilisation and full gradient"  << std::endl;
    std::cout << "Non stationnary advection-diffusion problem solved with Nonlinear scheme\n" << std::endl;
    std::cout << "Desired / initial time step : \t" << time_step << std::endl;

    const Real D_t = m_ratio_dt_max * time_step; 
    std::cout << "Maximal time step allowed: \t" << D_t << std::endl;


    const int N_prev = std::ceil(T_end / time_step); // previsional number (ideal) of iterations

    const Real & h = Th->meshsize();
    

    //------------------------------------------------------------------------------
    // Extraction of statistics and tensor 

    const Statistics & stat = m_data.Stat;  
    const DiffusivityType & Lambda = m_data.Lambda; 

    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    // Precomputations  

    std::cout << "\n Precomputations begin"  << std::endl;  
    common::chrono c_comp;
    c_comp.start();  
    
    //  Initialization of discerete data (Global contribution and Newton method)  
    const GC & gc = GC(Th,m_data.f,m_data.g_n, m_data.Lambda,m_data.isDirichlet,m_data.isNeumann, m_r , m_add_integrator); 
   
    
    //  Discretisation of continuous electrostatic potential      
    DU Phi_disc = DU(Th,gc);
    Phi_disc.set_from_fonction(Th,gc, m_data.Phi);  
    m_Phi = Phi_disc;       
    
    //Nexton Method 
    NMethod Method(Th, gc, m_Phi, stat, Lambda, m_mean, m_eps_stab_ini );
    const Real & s_proj =  Method.get_thresold_proj(); //  pas tres elegant, faire un truc propre 


    // Creation of the initial seed of the Newton method (first time step)
    //Rmk : we use a projected data for the initialisation : 
    // instead of using u^0, we use P_{Ih_eps}   
    DU w_ini = DU(Th,gc);
    InitialDataType w_0;
    w_0 =[stat,this,s_proj](const Point & x) -> Real {
	    return stat.h( stat.proj_Ih_eps(this->m_data.u_0(x), s_proj) );
	};

    w_ini.set_from_fonction(Th,gc, w_0);    
    w_ini += m_r *Phi_disc;  


    c_comp.stop();
    std::cout << "Precomputations : DONE !"  << std::endl;  
    std::cout << FORMAT(50) << "Time of the precomputational step"  << c_comp.diff() << std::endl;
    m_time_precomputation = c_comp.diff();    
    
    w_ini.file_visu_poly("debug/w_ini",Th);
    w_ini.file_visu_density("debug/u_ini",Th, m_Phi, stat);

    //------------------------------------------------------------------------------
    // loop in time - preparation  
    
    m_Sol.reserve( 2 * N_prev ); 
    
    m_nb_resol_sys_lin =0;
    
    m_time.push_back(0.); 
    m_Sol.push_back(w_ini);    
    
    m_nb_iter_Newton.push_back(0.);
    m_nb_resol_cumul.push_back(0.);


    //------------------------------------------------------------------------------
    // loop in time 
    std::cout << "Computation of the solution (evolutive - Newton Method) begins"  << std::endl;    
    common::chrono c_evol;
    c_evol.start();  
    
    Real dt = time_step ;
    Real t = 0.;                
    
    DU w_seed = DU(w_ini);
    w_seed.filter_low_order(Th, gc);     
    // w_seed.file_visu_poly("debug/w_filtre_ini",Th);
    // w_seed.file_visu_density("debug/u_filtre_ini",Th, m_Phi, stat);


    bool Newton_is_CV = false;    
    int n_iter = 1;
    int nb_reduction = 0; // the number of sucessive time step reduction     
    Real dt_ini_reduc = dt; // the time step used  

    //set the solution at previous time for the first computation
    Method.set_previous_time_sol(gc, w_ini, m_data.u_0, false);    
    //Intialize the Newton method with the initial discrete data  (+ projection on Ih_eps)
    Method.set_strating_point( w_seed);
    //Method.set_strating_point( w_ini);

    while((t < T_end- 0.5*std::pow(10.,-15)) && (dt > m_dt_critical )){ //Time loop  
        std::cout <<"\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" <<  std::endl;
        std::cout << "Iteration n° : " << n_iter << "\t (do not account the aborted iterations)" <<  std::endl;

        //define the (used) time step and set it in the method
        Real max_step = min(T_end - t, D_t );
        dt = min(dt, max_step);
        Method.set_time_step(dt);            

        std::cout <<  "Newton go ! time step used : \t" << dt << endl;
        //launch the method to compute the solution at time t^{n+1} (and return true if the Method converge)
        Real ratio_line_search =1.; 
        if(n_iter == 1){
            //ratio_line_search = 0.5* std::pow(4., - nb_reduction ); 
        }

        Newton_is_CV = Method.launch_method(Th, gc, ratio_line_search);
        
        const int nb_resol_n = Method.get_number_iteration();
        m_nb_resol_sys_lin += nb_resol_n; 

        if(Newton_is_CV){
            // time reached 
            t += dt;
            m_time.push_back(t);
            std::cout << FORMAT(50) << "Computation at time \t" << m_time.back() << "  : OK ! "<< std::endl;
            std::cout << FORMAT(50) << "Corresponds to iteration n° : " << n_iter <<  std::endl;
            nb_reduction = 0;        
            
            // Save the solution and data 
            const DU & w_sol = Method.get_w();
            m_nb_iter_Newton.push_back( nb_resol_n );
            m_nb_resol_cumul.push_back( m_nb_resol_sys_lin);
            m_Sol.push_back( w_sol );

            //set the new iteration number     
            n_iter += 1;  

            // set the new (previsional) time step 
            // Rmk : we use the following adaptation procedure:
            //  - if the iteration was successfull without the need of any time step reduction, we increase the time step 
            //  - if the iteration needed some time step reduction, we use the same time step 
            if(nb_reduction == 0){ 
                dt = dt * (1. + m_increase_time_step);
            }
                    
            //set the solution at previous time for the next computation
            Method.set_previous_time_sol(gc, w_sol, m_data.u_0 , true); //here, discsrete data is true by default, u_0 has no impact 
            //Intialize the Newton method with the previous solution (+ projection on Ih_eps)
            w_seed = w_sol; 
            //Method.set_strating_point(w_seed );
            Method.set_strating_point(w_seed );
        } //if Newton CV

        else{
            dt = (1. - m_reduce_time_step ) * dt;
            nb_reduction += 1; 
            
            Method.set_strating_point( w_seed );

            // if(nb_reduction == 5){ w_seed.filter_low_order(Th, gc);  }
            // Method.set_strating_point( w_seed );  
            //
        } //else, if the method did not converge (reduce time step + set the staring point)

    } //boucle temps
    c_evol.stop();


    // In case of failure 
    if( (dt< m_dt_critical) && (t < T_end- 0.5*std::pow(10.,-15) ) ){
        std::cout << " \n \n "<< std::endl;
        std::cout << " !!!!!!!!!!!!!!!!!!!  !!!!!!!!!!!!!!!!!!!  !!!!!!!!!!!!!!!!!!!  !!!!!!!!!!!!!!!!!!! " << std::endl;
        std::cout << "Current time step : " << dt << std::endl;
        std::cout << "Resolution failure ! " << std::endl;
        std::cout << " !!!!!!!!!!!!!!!!!!!  !!!!!!!!!!!!!!!!!!!  !!!!!!!!!!!!!!!!!!!  !!!!!!!!!!!!!!!!!!! " << std::endl;

        m_computations_done = false; 

    } // if effective time step becomes too small
    else{
        m_computations_done = true; 
        n_iter -=1; // the n_iter was incemented by one at the end of the loop 
    }

    std::cout<<"\n\n~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~ ~~" <<std::endl;
    std::cout << "Computation of the solution (evolutive - Newton Method): DONE !"  << std::endl;  
    std::cout << FORMAT(50) << "Time of the computation: "  << c_evol.diff() << "\n\n" <<std::endl;
    m_time_computation = c_evol.diff(); 
    m_time_tot = m_time_computation + m_time_precomputation; 
    std::cout << FORMAT(50) << "Simulation total time : "  << m_time_tot  <<std::endl;
    std::cout << FORMAT(50) << "Number of linear system solved : "  << m_nb_resol_sys_lin  <<std::endl;

    m_gc = gc;     
    m_global_contrib_computed = true;

} //compute_solution 


template<std::size_t K>
Real Scheme<K>::get_time_cost() const 
{
    return m_time_computation;
} //get_time_cost

template<std::size_t K>
void Scheme<K>::save_visu_files(const Mesh * Th, const int & nb_points, const std::string & name ) const
{   
    std::cout <<  "Visualization files saving begins " << std::endl;
    const int nb_sol_computed = m_Sol.size();
    const int mod_visu =round( nb_sol_computed / nb_points) +1;                
    for(int n=0; n< nb_sol_computed; n++ ){
        if(n % mod_visu ==0){
            m_Sol[n].file_visu_density("profiles/" + name + "sol_"+to_string(n), Th, m_Phi, m_data.Stat );
        } // if n % mod_visu -> save visu file 
    } //for n 
    std::cout <<  "Visualization files saved ! " << "\n" << std::endl;

} // save_visu_files
    
    
    
template<std::size_t K>
Real Scheme<K>::Error_rel_Lq_Lp(const Mesh * Th,
                                const int & q_time, 
                                const int & p_space, 
                                Real & norm_ex ) const
{       
    
    std::cout <<  "Error computation (on the solution) begins " << std::endl;
    std::cout << "Norm (space -time) used: \t " ; 
    if(q_time ==-1){
        std::cout << "L^infinity"  ; 
    } // if q_time ==-1 (L^\infty norm in time)
    else{
        std::cout << "L^"+to_string(q_time) ; 
    } //else 
    std::cout << "_t ( L^"+to_string(p_space) + "_x )" << std::endl  ; 
    
    //Rmk : do not take into account the initial time ()
    Real norm_err = 0.; 
    Real norm_ex_temp =0.; 
    
    const int nb_sol_computed = m_Sol.size();
    for(int n=1; n< nb_sol_computed; n++ ){
        
        const Real t_n = m_time[n]; 
        const Real dt_n = t_n - m_time[n-1];
        
        FctContType u_ex_n;
        u_ex_n =[this,t_n] (const Point & x) -> Real {
            return  this->m_data.u(t_n,x); 
        };

        const DU & w_n = m_Sol[n]; 
        Real norm_ex_n = 0.; 
        const Real norm_err_n = w_n.Error_Lp( m_gc,m_Phi,  m_data.Stat,  p_space , u_ex_n, norm_ex_n );
        
        if(q_time ==-1){
            norm_err = std::max(norm_err, norm_err_n )  ;
            norm_ex_temp = std::max(norm_ex_temp, norm_ex_n);  
        } // if q_time ==-1 (L^\infty norm in time)
        else{
            norm_err += (dt_n) * std::pow(norm_err_n, q_time);
            norm_ex_temp += (dt_n) * std::pow(norm_ex_n, q_time);
        } //else 
    }// for n, time loop 

    std::cout <<  "Error computed ! " << "\n"  << std::endl;
    if(q_time ==-1){
        norm_ex = norm_ex_temp;     
        std::cout << "Norm of the exact solution : \t" << norm_ex << std::endl; 
        return norm_err/norm_ex_temp;
    } // if q_time ==-1 (L^\infty norm in time)
    else{
        norm_ex = std::pow(norm_ex_temp,1./q_time);        
        std::cout << "Norm of the exact solution : \t" << norm_ex << std::endl; 
        return std::pow(norm_err/norm_ex_temp, 1./q_time);
    } //else 

} //Error_rel_Lq_Lp
 
 
template<std::size_t K>
Real Scheme<K>::Error_rel_grad_Lq_L2(   const Mesh * Th,
                                        const int & q_time, 
                                        Real & norm_ex ) const
{
    std::cout <<  "Error computation (on the gradient solution) begins " << std::endl;
    std::cout << "Norm (space -time) used: \t " ; 
    if(q_time ==-1){
        std::cout << "L^infinity"  ; 
    } // if q_time ==-1 (L^\infty norm in time)
    else{
        std::cout << "L^"+to_string(q_time) ; 
    } //else 
    std::cout << "_t ( L^2_x )" << std::endl  ; 
    
    //Rmk : do not take into account the initial time ()
    Real norm_err = 0.; 
    Real norm_ex_temp =0.; 
    
    const int nb_sol_computed = m_Sol.size();
    for(int n=1; n< nb_sol_computed; n++ ){
        
        const Real & t_n = m_time[n]; 
        const Real dt_n = t_n - m_time[n-1];
        
        ExactGradientType G_ex_n;
        G_ex_n =[this,t_n] (const Point & x) -> Eigen::Matrix<Real, 2, 1> {
            return  this->m_data.G(t_n,x); 
        };

        const DU & w_n = m_Sol[n]; 
        Real norm_ex_n = 0.; 
        const Real norm_err_n = w_n.Error_grad_L2( m_gc , m_Phi,  m_data.Stat , G_ex_n, norm_ex_n );
        
        if(q_time ==-1){
            norm_err = std::max(norm_err, norm_err_n )  ;
            norm_ex_temp = std::max(norm_ex_temp, norm_ex_n);  
        } // if q_time ==-1 (L^\infty norm in time)
        else{
            norm_err += (dt_n) * std::pow(norm_err_n, q_time);
            norm_ex_temp += (dt_n) * std::pow(norm_ex_n, q_time);
        } //else 
    }// for n, time loop 

    std::cout <<  "Error computed ! " << "\n"  << std::endl;
    if(q_time ==-1){
        norm_ex = norm_ex_temp;         
        std::cout << "Norm of the exact solution (grad): \t" << norm_ex << std::endl; 
        return norm_err/norm_ex_temp;
    } // if q_time ==-1 (L^\infty norm in time)
    else{
        norm_ex = std::pow(norm_ex_temp,1./q_time);         
        std::cout << "Norm of the exact solution (grad): \t" << norm_ex << std::endl; 
        return std::pow(norm_err/norm_ex_temp, 1./q_time);
    } //else 

} //  Error_rel_grad_Lq_L2


template<std::size_t K>                           
void Scheme<K>::save_file_longtime( const Mesh * Th, const int & nb_points_graphs, const std::string & name ) const
{
    std::cout <<  "Error computation related to the long time behaviour begins " << std::endl;
    std::cout << "WARNING !!!! \nDo not use this feature with non-Boltzmann statistics or non pure homogeneous Neumann BC !!!" << std::endl ;      
    const int nb_sol_computed = m_Sol.size();

    common::chrono c_longtime;
    c_longtime.start();
    
    // Computation of the mass and of the "exact" thermal equilibrium 
    Real mass_omega = 0.; //mass of exp(-\Phi)
    Real mass_u_0 =0.;   

    for(int iT =0; iT < Th->numberOfCells(); iT++){
        const LC & lc = m_gc.getLC(iT);

        for(int iF_loc =0; iF_loc < lc.m ; iF_loc ++ ){
            const PyramidIntegrator & pim = *lc.pims[iF_loc];
            for(Integer iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
                const Point & xQN = pim.point(iQN);
                const Real & wQN = pim.weight(iQN);
                mass_omega += wQN * m_data.Stat.g( - m_data.Phi(xQN) ); 
                mass_u_0  += wQN * m_data.u_0(xQN);
            } // for iQN
        } // for iF_loc
    } //for iT    
    
    const Real rho_eq = mass_u_0 / mass_omega ;

    const FctContType u_equilibrium = [this, rho_eq ](const Point & x) -> Real {
        return rho_eq * this->m_data.Stat.g(- this->m_data.Phi(x) ); 
    };

    std::vector<Real> Diff_L2;
    std::vector<Real> Diff_L1;    
    std::vector<Real> Entro;
 
    for(int n = 0; n < nb_sol_computed; n ++){
    
        Real norm_eq = 0.; 
        const DU & w_n = m_Sol[n]; 
        Diff_L1.push_back( w_n.Error_Lp( m_gc,m_Phi,  m_data.Stat,  1, u_equilibrium, norm_eq) );
        Diff_L2.push_back( w_n.Error_Lp( m_gc,m_Phi,  m_data.Stat,  2, u_equilibrium, norm_eq) );
        Entro.push_back( w_n.get_relative_entropy( m_gc,m_Phi,  m_data.Stat, u_equilibrium) );
    } // for n

    std::cout << "Computation of entropy : DONE"  << std::endl;

    const int mod =round(nb_sol_computed  / nb_points_graphs) +1;

    std::ofstream myfile;
    myfile.open ("LongTime_nlhho_" + std::to_string(K)+name +".csv");
    myfile << " TEST ??.\n";
    myfile <<"Description "<<"," <<"Advection_diffusion.\n";
    myfile <<"Number of time iteration "<<"," << nb_sol_computed  << endl;    
    myfile <<"Meshsize"<<"," << Th->meshsize()  << endl;
    myfile <<"Total number of linear systems solved :"<<"," << m_nb_resol_sys_lin  << endl;
    myfile <<"" << endl;
    myfile << "Time,Entropy,Diff_L1,Diff_L2,Nb_iter_Newton,Nb_resol_cumul,\n";
    for(int n=0; n< nb_sol_computed ; n ++){
        myfile << m_time[n]<<","<< Entro[n] <<","<<Diff_L1[n] <<"," << Diff_L2[n] << ","<< m_nb_iter_Newton[n]  << ","  << m_nb_resol_cumul[n] <<  endl;
    }
    myfile.close();

    std::ofstream myfile2;
    myfile2.open ("time_nlhho_" + std::to_string(K)+name);
    myfile2 << "Time Entropy Diff_L1_square Diff_L2_square Diff_L1 Diff_L2 Nb_iter_Newton Nb_resol_cumul \n";
    for(int n=0; n< nb_sol_computed ; n ++){
        if(n % mod ==0){
        myfile2 << m_time[n] <<" "<< Entro[n] <<" "<< std::pow(Diff_L1[n],2) <<" " << std::pow(Diff_L2[n],2)  << " "<< Diff_L1[n] <<" " << Diff_L2[n] <<" "<< m_nb_iter_Newton[n] <<" " << m_nb_resol_cumul[n] << endl;
        }// if n % 100 ==0
    }//for n
    myfile2.close();
    
    c_longtime.stop();
    std::cout <<  "Generation of the long-time behaviour related files : DONE" << std::endl;
    std::cout <<  "Time for the computation : \t" << c_longtime.diff() << "\n" << std::endl;

} // save_file_longtime 


template<std::size_t K> 
void Scheme<K>::give_positivity_information(const Mesh * Th , const bool & faces_info ) const   
{   
    std::cout << "Computation of bounds (minimal values) of the computed solution" << std::endl;
    std::cout << "WARNING !!!! \nDo not use this feature with non-Boltzmann statistics ?? (only minimal values)" << std::endl ;      
    const int nb_sol_computed = m_Sol.size(); 
    const Statistics & stat = m_data.Stat; 

    //intitalize values for min
    const Real ini = 1.* std::pow(10,10); 

    Real min_pyr =  ini; // minimal value of the int_P u, where P are the pyramid of the mesh 
    Real min_cell =  ini;  // minimal value of the int_T u
    Real min_QN_cells =  ini; // minimal value of u one the quadrature nodes (for cells unknowns only)
    Real min_QN_faces = ini; // minimal value of u one the quadrature nodes (for cells unknowns only)
    Real min_faces =  ini;// the minimal value of int_F u_F 


    for(int n = 1; n < nb_sol_computed; n ++){
        const DU arg_n = m_Sol[n] - m_r * m_Phi; 
        
        for(int iT = 0; iT < m_gc.nb_cells; iT ++){
            const Cell & T = Th->cell(iT); 
            const Real & m_T = T.measure(); 
            const Point & x_T = T.center(); 

            const LC & lc = m_gc.getLC(iT); 
            const SolutionVectorType arg_n_TF = arg_n.get_local_dofs( m_gc, iT); 
            Real u_T =0.; // int_T u_T 
            
            for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
                const int & iF = T.faceId(iF_loc);
                const Face & F = Th->face(iF);
                const Real & m_F = F.measure();

                // quantities on cells        
                const Point & x_F = F.barycenter();
                const Point & n_F= F.normal(x_T);
                const Real & d_TF = inner_prod(n_F, x_F-x_T);
                const Real m_PTF = 0.5* d_TF* m_F;
                const PyramidIntegrator & pim = *lc.pims[iF_loc];
                Real u_PTF = 0.; // int_P u_T             
                for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
                    const Real & wQN = pim.weight(iQN);
                    //computation of the argument of g at xQN
                    Real arg_iqn = 0.;         
                    for(std::size_t i = 0; i < lc.nb_cell_dofs; i++) {
                        const typename LC::CellBasis::ValueType & phi_i_iqn = (*lc.feval_basisT_PTF[iF_loc])(i, iQN);
                        arg_iqn += arg_n_TF[i] * phi_i_iqn;
                    } // for i
                    const Real u_iqn = stat.g( arg_iqn); 
                    min_QN_cells = std::min(u_iqn, min_QN_cells); 
                    u_PTF += wQN * u_iqn; 
                }//for iQN 
                u_T += u_PTF; 
                u_PTF = u_PTF / m_PTF; 
                min_pyr = std::min(u_PTF, min_pyr); 

                if(faces_info){                

                    const SolutionVectorType & arg_F = lc.Extract_Faces[iF_loc] * arg_n_TF; 
                    const FaceIntegrator & fim = *lc.fims[iF_loc];
                    Real u_F = 0.; 
                    for(int iQN = 0; iQN < fim.numberOfPoints(); iQN++) {
                        const Real & wQN = fim.weight(iQN);            
                        Real arg_F_iqn =0.;
                        for(std::size_t i = 0; i < lc.nb_local_face_dofs; i++) {
                            const typename LC::FaceBasis::ValueType & phi_i_iqn = (*lc.feval_basisF[iF_loc])(i, iQN);
                            arg_F_iqn += arg_F(i) * phi_i_iqn;
                        } // for i
                        const Real u_F_iqn = stat.g(arg_F_iqn); 
                        min_QN_faces = std::min( u_F_iqn ,min_QN_faces); 
                        u_F += wQN * u_F_iqn;  
                    } // for iQN
                    u_F = u_F / m_F; 
                    min_faces = std::min( u_F, min_faces ); 
                } //if faces info 
            } // For iF_loc 
            u_T = u_T / m_T; 
            min_cell = std::min( u_T, min_cell );    

        } //for iT 
    } // for n 

    std::cout << "~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~~ ~~~~~~ ~~~~~ ~~~~~" << std::endl; 
    std::cout << FORMAT(50) << "Min on cells (mean) " << min_cell << std::endl;
    std::cout << FORMAT(50) << "Min on pyramids (mean) " << min_pyr << std::endl;    
    std::cout << FORMAT(50) << "Min on cells QN " << min_QN_cells << std::endl;
    
    if(faces_info){
        std::cout << FORMAT(50) << "Min on faces (mean) " << min_faces << std::endl;    
        std::cout << FORMAT(50) << "Min on faces QN " << min_QN_faces << std::endl;
    }// if faces info 
    std::cout << "~~~~~ ~~~~~ ~~~~~ ~~~~~ ~~~~~~ ~~~~~~ ~~~~~ ~~~~~\n\n" << std::endl; 
} //get_positivity_information 


template<std::size_t K> 
void Scheme<K>::debug_exact_sol(const Mesh * Th, const Real & T_end, const int & nb_points ) const
{   
    std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl; 
    std::cout << "DEBUG : testing the Jacobian (global)" << std::endl; 


    const Real dt = T_end / nb_points; 
    const Real & h = Th->meshsize();
    const Statistics & stat = m_data.Stat;  
    

    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    // Precomputations  
    
    std::cout << "\n Precomputations begin"  << std::endl;  
    GC gc; 
    DU Phi; 
    if(m_global_contrib_computed){
        gc = m_gc;
        Phi = m_Phi; 
    }
    else{
        //  Initialization of discerete data (Global contribution and Newton method)  
        gc = GC(Th,m_data.f,m_data.g_n, m_data.Lambda,m_data.isDirichlet,m_data.isNeumann, m_add_integrator);
        std::cout << "Global contributions computed"  << std::endl;        
        Phi = DU(Th,gc);
        Phi.set_from_fonction(Th,gc, m_data.Phi);  
    }

    

    NMethod Method(Th, gc, Phi, stat , m_data.Lambda, m_mean,m_eps_stab_ini );
    
    //  Discretization of continuous data   
    DU w_ini = DU(Th,gc);
    std::cout << "test DU ok"  << std::endl;
    InitialDataType w_0;
    w_0 =[stat,this](const Point & x) -> Real {
	        return stat.h( this->m_data.u_0 (x)   );
	    };

    w_ini.set_from_fonction(Th,gc, w_0);    
    // La grande question de savoir ce qu'il faut faire si u^0 s'annule quelque part ...
    //
    //
 
    std::cout << "Precomputations : DONE !"  << std::endl;  
  
    
    w_ini.file_visu_poly("debug/w_ini",Th);    
    w_ini.file_visu_density("debug/u_ini",Th, Phi, stat);

    Real t =0.;
    int n=0;
    while(t< T_end){    
        ExactSolutionType w_t;
        w_t =[stat,t,this](const Point & x) -> Real {
	        return stat.h( this->m_data.u(t, x)   );
	    };    
        

        DU w_t_discret = DU(Th,gc);
        w_t_discret.set_from_fonction(Th,gc, w_t);
        w_t_discret.file_visu_density("debug/u_"+to_string(n),Th, Phi, stat);        
        
        ExactSolutionType u_t;
        u_t =[t,this](const Point & x) -> Real {
	        return  this->m_data.u(t, x)   ;
	    };
        postProcessing_exactfct<boost::static_unsigned_min<K+1,3>::value>("debug/uex_"+to_string(n) + ".vtu", Th, u_t);

        t+=dt;
        n+=1;  
    } // while t < T_end

    std::cout << "Debug - visualisation of the exact solution - DONE ";
} // debug_exact_sol


template<std::size_t K> 
void Scheme<K>::debug_Gaudeul_test( const Mesh * Th, 
                                    const Real & time_step, 
                                    const int nb_pts_base, 
                                    const int nb_points_dir) const
{
    std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl; 
    std::cout << "DEBUG : Jacobian test" << std::endl; 


    const Real dt = time_step; 
    const Real & h = Th->meshsize();
    const Statistics stat = m_data.Stat;  
    

    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    // Precomputations  
    
    std::cout << "\n Precomputations begin"  << std::endl;  
    GC gc; 
    DU Phi; 
    if(m_global_contrib_computed){
        gc = m_gc;
        Phi = m_Phi; 
    }
    else{
        //  Initialization of discerete data (Global contribution and Newton method)  
        gc = GC(Th,m_data.f,m_data.g_n, m_data.Lambda,m_data.isDirichlet,m_data.isNeumann, m_add_integrator);
        std::cout << "Global contributions computed"  << std::endl;        
        Phi = DU(Th,gc);
        Phi.set_from_fonction(Th,gc, m_data.Phi);  
    }



    NMethod Method(Th, gc, Phi, stat ,  m_data.Lambda, m_mean, m_eps_stab_ini);
    
    //  Discretization of continuous data   
    DU w_ini = DU(Th,gc);
    InitialDataType w_0;
    w_0 =[stat,this](const Point & x) -> Real {
	        return stat.h( this->m_data.u_0 (x)   );
	    };
    w_ini.set_from_fonction(Th,gc, w_0);    
    // La grande question de savoir ce qu'il faut faire si u^0 s'annule quelque part ...
    //
    //
 
    std::cout << "Precomputations : DONE !"  << std::endl;      
    //set the solution at previous time for the first computation
    Method.set_previous_time_sol(gc, w_ini, m_data.u_0, false);    
    //Intialize the Newton method with the initial discrete data  (+ projection on Ih_eps)
    Method.set_strating_point( w_ini);

    Method.Gaudeul_test(Th, gc, nb_pts_base, nb_points_dir); 


} // debug_Gaudeul_test      



template<std::size_t K> 
void Scheme<K>::debug_loc_Gaudeul_test_condensed( const Mesh * Th,
                                        const Real & time_step,
                                        const int nb_pts_base,
                                        const int nb_points_dir) const
{
    std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl; 
    std::cout << "DEBUG : testing the Jacobian (local, only one block)" << std::endl; 


    const Real dt = time_step; 
    const Real & h = Th->meshsize();
    const Statistics stat = m_data.Stat;  
    

    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    // Precomputations  
    
    std::cout << "\n Precomputations begin"  << std::endl;  
    GC gc; 
    DU Phi; 
    if(m_global_contrib_computed){
        gc = m_gc;
        Phi = m_Phi; 
    }
    else{
        //  Initialization of discerete data (Global contribution and Newton method)  
        gc = GC(Th,m_data.f,m_data.g_n, m_data.Lambda,m_data.isDirichlet,m_data.isNeumann, m_add_integrator);
        std::cout << "Global contributions computed"  << std::endl;        
        Phi = DU(Th,gc);
        Phi.set_from_fonction(Th,gc, m_data.Phi);  
    }


    NMethod Method(Th, gc, Phi, stat,  m_data.Lambda, m_mean,m_eps_stab_ini );
    
    //  Discretization of continuous data   
    DU w_ini = DU(Th,gc);
    InitialDataType w_0;
    w_0 =[stat,this](const Point & x) -> Real {
	        return stat.h( this->m_data.u_0 (x)   );
	    };
    w_ini.set_from_fonction(Th,gc, w_0);    
    // La grande question de savoir ce qu'il faut faire si u^0 s'annule quelque part ...
    //
    //
 
    std::cout << "Precomputations : DONE !"  << std::endl;      
    //set the solution at previous time for the first computation
    Method.set_previous_time_sol(gc, w_ini, m_data.u_0, false);    
    //Intialize the Newton method with the initial discrete data  (+ projection on Ih_eps)
    Method.set_strating_point( w_ini);

    Method.local_Gaudeul_test_condensed(Th, gc,  nb_pts_base, nb_points_dir); 

}  //debug_loc_Gaudeul_test_condensed





template<std::size_t K> 
void Scheme<K>::debug_loc_Gaudeul_test( const Mesh * Th,
                                        const Real & time_step,
                                        const int nb_pts_base,
                                        const int nb_points_dir) const
{
    std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl; 
    std::cout << "DEBUG : testing the Jacobian (local, only one block)" << std::endl; 


    const Real dt = time_step; 
    const Real & h = Th->meshsize();
    const Statistics stat = m_data.Stat;  
    

    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    // Precomputations  
    
    std::cout << "\n Precomputations begin"  << std::endl;  
    GC gc; 
    DU Phi; 
    if(m_global_contrib_computed){
        gc = m_gc;
        Phi = m_Phi; 
    }
    else{
        //  Initialization of discerete data (Global contribution and Newton method)  
        gc = GC(Th,m_data.f,m_data.g_n, m_data.Lambda,m_data.isDirichlet,m_data.isNeumann, m_add_integrator);
        std::cout << "Global contributions computed"  << std::endl;        
        Phi = DU(Th,gc);
        Phi.set_from_fonction(Th,gc, m_data.Phi);  
    }


    NMethod Method(Th, gc, Phi, stat,  m_data.Lambda, m_mean,m_eps_stab_ini );
    
    //  Discretization of continuous data   
    DU w_ini = DU(Th,gc);
    InitialDataType w_0;
    w_0 =[stat,this](const Point & x) -> Real {
	        return stat.h( this->m_data.u_0 (x)   );
	    };
    w_ini.set_from_fonction(Th,gc, w_0);    
    // La grande question de savoir ce qu'il faut faire si u^0 s'annule quelque part ...
    //
    //
 
    std::cout << "Precomputations : DONE !"  << std::endl;      
    //set the solution at previous time for the first computation
    Method.set_previous_time_sol(gc, w_ini, m_data.u_0, false);    
    //Intialize the Newton method with the initial discrete data  (+ projection on Ih_eps)
    Method.set_strating_point( w_ini);

    Method.local_Gaudeul_test(Th, gc,  nb_pts_base, nb_points_dir); 

}  //debug_loc_Gaudeul_test    




template<std::size_t K> 
void Scheme<K>::debug_loc_Gaudeul_test_by_block(const Mesh * Th,
                                                const Real & time_step,
                                                const int id_block, 
                                                const int nb_pts_base,
                                                const int nb_points_dir) const
{
    std::cout << "~~~~~~~~~~~~~~~~~~~~~~~~~" << std::endl; 
    std::cout << "DEBUG : testing the Jacobian (local, only one block)" << std::endl; 


    const Real dt = time_step; 
    const Real & h = Th->meshsize();
    const Statistics stat = m_data.Stat;  
    

    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    // Precomputations  
    
    std::cout << "\n Precomputations begin"  << std::endl;  
    GC gc; 
    DU Phi; 
    if(m_global_contrib_computed){
        gc = m_gc;
        Phi = m_Phi; 
    }
    else{
        //  Initialization of discerete data (Global contribution and Newton method)  
        gc = GC(Th,m_data.f,m_data.g_n, m_data.Lambda,m_data.isDirichlet,m_data.isNeumann, m_add_integrator);
        std::cout << "Global contributions computed"  << std::endl;        
        Phi = DU(Th,gc);
        Phi.set_from_fonction(Th,gc, m_data.Phi);  
    }


    NMethod Method(Th, gc, Phi,  stat ,  m_data.Lambda, m_mean, m_eps_stab_ini);
    
    //  Discretization of continuous data   
    DU w_ini = DU(Th,gc);
    InitialDataType w_0;
    w_0 =[stat,this](const Point & x) -> Real {
	        return stat.h( this->m_data.u_0 (x)   );
	    };
    w_ini.set_from_fonction(Th,gc, w_0);    
    // La grande question de savoir ce qu'il faut faire si u^0 s'annule quelque part ...
    //
    //
 
    std::cout << "Precomputations : DONE !"  << std::endl;      
    //set the solution at previous time for the first computation
    Method.set_previous_time_sol(gc, w_ini, m_data.u_0, false);    
    //Intialize the Newton method with the initial discrete data  (+ projection on Ih_eps)
    Method.set_strating_point( w_ini);

    Method.local_Gaudeul_test_by_block(Th, gc,  nb_pts_base, nb_points_dir, id_block); 



}  //debug_loc_Gaudeul_test_by_block              

} //namespace NL
} //namespace ho

#endif