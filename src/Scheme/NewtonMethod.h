#ifndef NEWTONMETHOD_H
#define NEWTONMETHOD_H

#include "NLcontributions.h"
#include <random>


#ifndef max_size_coef
#define max_size_coef 100000.
#endif


namespace ho
{
namespace NL 
{
template<std::size_t K>
class NewtonMethod 
{    
    typedef ho::diffusion::LocalContrib<K> LC;
    typedef ho::diffusion::GlobalContrib<K> GC;  
    typedef ho::GlobalSys<K> GS; 
    typedef ho::DiscUnkw<K> DU;      
    typedef ho::NL::NLContrib<K> NLC;           
    
    public:  
    
    // NewtonMethod( const  Mesh & mesh,const Fixed_contrib & contribs ,   Flux & flux);
    //
    //constructor for the Newton's method
    NewtonMethod(   const Mesh * Th,
                    const GC & gc , 
                    const DU & Phi, 
                    const Statistics & stat,                         
                    const DiffusivityType & Lambda, 
                    const Mean_Fct & mean,  
                    const Real & epsilon_stab = 0., 
                    const bool & complete_infos =   false,  //true, //
                    const int & max_iterations = 50, 
                    const Real & stopping_crit = 5.* std::pow(10,-10),
                    const Real & proj_eps = std::pow(10,-8)
                );


    //
    // set the previous time step soultion (u^n) and enforce it in the NonLinear Contributions (bool type select the fully discrete data)
    void set_previous_time_sol( const GC & gc,   
                                const DU & w_n, 
                                const FctContType & u_n, 
                                const bool & discrete_type = true);

    //
    // Set the time step for the method
    void set_time_step(const Real & dt); 
    //
    //Set the starting point and initialize the method
    void set_strating_point(const DU & w_ini); 
    //
    // set the NonLocal contributions associated to the starting point and compute the associated global system and compute the goal function norm.
    void actualize_nlc(const Mesh* Th, const GC & gc, const bool & test_gaudeul = false);
    //
    // check the fact that the computed discrete DENSITY is in I_h (NOT IMPLEMENTED YET !!!) 
    void get_bounds_conformity(); 
    //
    //compute the relative norm of the residue (in quasi Fermi potential ??)
    void compute_relative_res_norm() ; 
    //
    // print the informations related to the last iteration
    void print_infos_iteration() const;    
    //
    // launch a (single) Newton iteration
    void launch_iteration(const Mesh * Th,const GC & gc, const Real & step_Newton_line_search = 1.);
    //
    // launch the full method (compute the solution at time n+1). Return true if the method converges, false if the method did not ...
    bool launch_method(const Mesh * Th,const GC & gc, const Real & step_Newton_line_search = 1.); 
    //
    // returns the current discrete unknown 
    DU get_w() const; 
    //
    // returns the currents number of iteration of the method 
    int get_number_iteration() const;
    //
    //perform a Jacobian test à la Gaudeul 
    void Gaudeul_test(const Mesh * Th, const GC & gc, const int & number_base_point, const int & number_dir );    
    //
    //perform a local Jacobian test à la Gaudeul with (local) static condensation 
    void local_Gaudeul_test_condensed(  const Mesh * Th,
                                        const GC & gc, 
                                        const int & number_base_point, 
                                        const int & number_dir );       
    //
    //perform a local Jacobian test à la Gaudeul (do not take into account the condensation) 
    void local_Gaudeul_test(const Mesh * Th,
                            const GC & gc, 
                            const int & number_base_point, 
                            const int & number_dir );     
    //
    //perform a local Jacobian test à la Gaudeul, test the block id_block_tested (do not take into account the condensation) 
    void local_Gaudeul_test_by_block(   const Mesh * Th,
                                        const GC & gc, 
                                        const int & number_base_point, 
                                        const int & number_dir , 
                                        const int & id_block_tested  );   

    //
    // return the value of the thresold of projection
    Real get_thresold_proj() const; 


    private:
    NLC m_NLcontrib; 

    Real m_dt;
    
    DU m_w;
    DU m_res; //the residue of the Newton Method

    
    Real m_norm_goal;
    Real m_norm_rel_residue;

    int m_nb_it;
    bool m_is_CV;
    bool m_can_continue;
    bool m_goal_is_small;
    bool m_res_is_small;
    bool m_res_is_very_small;
    bool m_bounds;
    bool m_linear_solver_ok; 
    bool m_data_size_reasonable; 

    Real m_r; 
    DU m_Phi; // the discrete electrostatic potential 
    Statistics m_stat;

    int m_max_it;
    Real m_stopping_crit;
    Real m_proj; // thresold for the projection of discrete unknowns 
    bool m_print_info;

};




//------------------------------------------------------------------------------
// Implementation NewtonMethod 

template<std::size_t K>
NewtonMethod<K>::NewtonMethod(  const Mesh * Th, 
                                const GC & gc , 
                                const DU & Phi, 
                                const Statistics & stat,                     
                                const DiffusivityType & Lambda, 
                                const Mean_Fct & mean,  
                                const Real & epsilon_stab ,
                                const bool & complete_infos , 
                                const int & max_iterations, 
                                const Real & stopping_crit,
                                const Real & proj_eps  )
{

    //method's parameters 
    m_print_info = complete_infos;
    m_max_it = max_iterations;
    m_stopping_crit = stopping_crit;   
    m_proj = proj_eps; 
    
    m_r = gc.r; 
    m_Phi = Phi, 
    m_stat = stat; 
    
    //initialize the discrete unknonwns and contributions 
    m_w = DU(Th,gc);
    m_res = DU(Th,gc);
    m_NLcontrib = NLC(Th, gc, Lambda, mean,  epsilon_stab); 

}// Constructor NewtonMethod


template<std::size_t K>
void NewtonMethod<K>::set_previous_time_sol(const GC & gc, 
                                            const DU & w_n, 
                                            const FctContType & u_n, 
                                            const bool & discrete_type)
{   
    m_NLcontrib.clear();
    if(discrete_type){
        m_NLcontrib.set_previous_time_density(gc,w_n, m_Phi, m_stat);
    }
    else{
        m_NLcontrib.set_previous_time_density_from_cont_fct(gc, u_n); 
    }

} //set_previous_time_sol



template<std::size_t K>
void NewtonMethod<K>::set_time_step(const Real & dt)
{
    m_dt = dt; 
    m_NLcontrib.set_time_step(m_dt); 

} //set_time_step


template<std::size_t K>
void NewtonMethod<K>::set_strating_point(const DU & w_ini)
{
    m_w = w_ini;
    //m_w.Proj_on_Ih_eps(m_stat, m_Phi, m_proj);

    m_res *= 0.;    
    
    m_can_continue = true; 
    m_is_CV = false;
    m_nb_it = 0; 

} //set_strating_point


template<std::size_t K>
void NewtonMethod<K>::actualize_nlc(const Mesh * Th, const GC & gc, const bool & test_gaudeul )
{   
    m_NLcontrib.set_current_point( m_w );        
    m_data_size_reasonable = m_NLcontrib.compute_NL_contributions( Th, gc, m_Phi, m_stat, test_gaudeul );  
    if(m_data_size_reasonable){
        m_norm_goal = m_NLcontrib.get_norm_goal();       
        m_goal_is_small = (m_norm_goal<m_stopping_crit);
    }//if m_bounds : the computation of the nlc went well  

    else{
        m_goal_is_small = false;
    }//else : the computation of the nlc did not succed ...  


} //actualize_nlc


template<std::size_t K>
void NewtonMethod<K>::get_bounds_conformity()
{
    // bool Ih_valued = true; 
    // //un truc pour savoir si la densité est ok 
    // // probablement savoir si w (le polynôme / potentiel de quasi Fermi) ne prend pas de trop grande valeurs 
    // // un jour faudra voir 
    const Real & max_coeffs = (m_w - m_r * m_Phi ).L_infty_norm_coefs();     
    //const Real & max_coeffs = m_w.L_infty_norm_coefs(); 
    std::cout << "max coeff :" << max_coeffs << std::endl; 
    m_bounds = m_data_size_reasonable &&  ( max_coeffs < max_size_coef);
}


template<std::size_t K>
void NewtonMethod<K>::compute_relative_res_norm()
{
    m_norm_rel_residue = m_res.L_infty_norm_coefs() / m_w.L_infty_norm_coefs();  // every coefs (faces and cells)   
    m_res_is_small = (m_norm_rel_residue < m_stopping_crit);
    m_res_is_very_small = (m_norm_rel_residue < 0.01 * m_stopping_crit);

} //compute_relative_res_norm

template<std::size_t K>
void NewtonMethod<K>::print_infos_iteration() const
{
    std::cout << "Information about the iteration number \t"<< m_nb_it << std::endl;
    std::cout << "Does the Newton method converge  ? \t"<< m_is_CV << std::endl;
    std::cout << "Is the discrete density computed I_h-valued  ? \t"<< m_bounds << std::endl;
    std::cout << "Norm of the goal function: \t"<< m_norm_goal << std::endl;
    std::cout << "Relative norm of the residue (L^infty on dofs): \t"<< m_norm_rel_residue << std::endl;
   

} // print_infos_iteration    


template<std::size_t K>
void NewtonMethod<K>::launch_iteration(const Mesh * Th, const GC & gc, const Real & step_Newton_line_search)
{
    std::cout << "\niteration begins"<< std::endl; 
    m_linear_solver_ok = false; 
    actualize_nlc(Th, gc);
    if(m_data_size_reasonable){
        const GS & gs = m_NLcontrib.get_globalsystem();   
        m_linear_solver_ok = m_res.set_from_GlobalSys_zero_boundary(gc,gs);
        if(m_linear_solver_ok){
            m_w += step_Newton_line_search * m_res;     
            m_nb_it+=1;
            compute_relative_res_norm();
            get_bounds_conformity();
        }   // if the linear resolution went well 
        else{
            m_res_is_small = false; 
            m_res_is_very_small = false; 
        } // if the linear resolution went bad 

    } // if the nlc computation went well 

    else{     
        m_res_is_small = false; 
        m_res_is_very_small = false; 
    } // if the nlc were not computed correctly

    //m_w.file_visu_poly("profil_w_"+ std::to_string(m_nb_it),)
    //m_w.print_values_debug(gc, true, true); 
} // launch_iteration

template<std::size_t K>
bool NewtonMethod<K>::launch_method(const Mesh * Th, const GC & gc, const Real & step_Newton_line_search )
{
    // launch iteration 
    do{ // Newton loop
        launch_iteration(Th, gc, step_Newton_line_search);   
       
        bool too_much_iter = (m_nb_it > m_max_it);
        bool CV_indicators_are_small = m_res_is_very_small || (m_res_is_small && m_goal_is_small);
        //bool CV_indicators_are_small = m_res_is_small || m_goal_is_small;
        
        m_is_CV = (!too_much_iter) && CV_indicators_are_small && m_bounds && m_linear_solver_ok;
        m_can_continue = m_bounds && (! CV_indicators_are_small) && (! too_much_iter) && m_linear_solver_ok;

        if(m_print_info){ print_infos_iteration();}
       
    }while(m_can_continue) ; //loop Newton

    return m_is_CV; 

} // launch_method


template<std::size_t K>
DiscUnkw<K> NewtonMethod<K>::get_w() const 
{
    return m_w;
} //get_w


template<std::size_t K>
int NewtonMethod<K>::get_number_iteration() const
{
    return m_nb_it;
} //get_number_iteration



template<std::size_t K>
void NewtonMethod<K>::Gaudeul_test(const Mesh * Th, const GC & gc, const int & number_base_point, const int & number_dir )
{
    std::cout << sep << std::endl;
    std::cout << "Performing a test of the Jacobian à la Gaudeul "<< std::endl;
    std::cout << "May it be a success ... " << std::endl;

    Real size_data = std::pow(0.5 , K);    
    
    cout << "New base point. "<< endl;
    for(int nb_base = 0; nb_base < number_base_point; nb_base ++){
        std::cout << "Base point number "<< nb_base +1 << std::endl;

        DU w_base = m_w;
        w_base.set_with_Random_values();
        w_base.file_visu_poly("debug/w_base_"+to_string(nb_base+1)+"beforescaling",Th);
        //w_base.print_local_values_debug(gc,0,false,true); 
        // for(int iT = 0; iT < gc.nb_cells; iT ++ ){
        //     const Cell & T = Th->cell(iT); 
        //     const Point & x_T = T.center(); 
        //     std::cout << "Value cell n°" << iT <<" :\t" << w_base.get_local_value_poly(gc,iT,x_T) <<std::endl; 
        // } // for iT 
       
        w_base *= size_data; 
        w_base.Proj_on_Ih_eps(m_stat, m_Phi, m_proj);
        w_base.file_visu_poly("debug/w_base_"+to_string(nb_base+1),Th);

        //w_base.print_values_debug(gc, false, true);

        m_w = w_base;
        std::cout << "base ok ? "<<  std::endl;

        
        

        actualize_nlc(Th,  gc, true ); 
        std::cout << "actu nlc "<<  std::endl;

        const GS & gs_base = m_NLcontrib.get_globalsystem(); 
        const SparseMatrixType & Jac_base = gs_base.get_global_matrix();     
        
        
        const SolutionVectorType & P_edges_base = m_NLcontrib.get_goal_edges() ;
        std::vector<SolutionVectorType> P_cells_base; 
        P_cells_base.resize( gc.nb_cells );
        for(int iT = 0; iT < gc.nb_cells; iT ++){
            P_cells_base[iT] = m_NLcontrib.get_goal_cell(iT);
        }  // for iT 
        std::cout << "P_edges "<<  std::endl;
        
        cout << "New direction !"<< endl;
        for(int nb_dir = 0; nb_dir <number_dir; nb_dir++){
            cout << "Direction number "<< nb_dir +1 << endl;
            cout << "   " << endl;        
            
            //DU w_dir = m_w;
            DU w_dir(Th, gc);
            
            const SolutionVectorType & X_dir =  size_data * SolutionVectorType::Random( gc.nb_faces_dofs);
            w_dir.set_from_edges_values_and_GS(gc,gs_base,X_dir);
            w_dir.file_visu_poly("debug/w_dir_"+to_string(nb_dir+1),Th);

            const SolutionVectorType J_dir = Jac_base *X_dir;

            for(int nb_h = 1; nb_h < 8; nb_h ++){
                Real h = 1. * std::pow(10.,  -nb_h);
             
                    
                m_w  = w_base + h * w_dir;
                m_w.file_visu_poly("debug/w_base"+to_string(nb_base+1)+"_dir"+to_string(nb_dir +1)+"_h"+to_string(nb_h),Th);
                
                actualize_nlc(Th,  gc, true );         
                
                const SolutionVectorType & P_edges = m_NLcontrib.get_goal_edges() ;
                std::vector<SolutionVectorType> P_cells; 
                P_cells.resize( gc.nb_cells );
                for(int iT = 0; iT < gc.nb_cells; iT ++){
                    P_cells[iT] = m_NLcontrib.get_goal_cell(iT);
                }  // for iT 
                
                const SolutionVectorType err_edges = P_edges - P_edges_base - h *J_dir;
                const Real Norm_Err_edges = err_edges.lpNorm<Eigen::Infinity>();
                Real Norm_Err_cell = 0.;
                for(int iT = 0; iT < gc.nb_cells; iT ++){
                    const SolutionVectorType err_cell_T = P_cells[iT] - P_cells_base[iT]; 
                    const Real norm_err_cell_T = err_cell_T.lpNorm<Eigen::Infinity>();
                    Norm_Err_cell = std::max(norm_err_cell_T, Norm_Err_cell ); 
                }  // for iT 
                const Real Norm_Err = max(Norm_Err_cell,Norm_Err_edges);

                cout << "Step h  :"<< h  <<", error / h^2  :" << Norm_Err/(h*h)   << endl;
                cout << "Error norm : " << Norm_Err   << endl;
            }//for nb_h
            cout << "\n "<< endl;
            
        } //for nb_dir
        cout << "\n"<< endl;


    } //for nb_base 

    std::cout << "Conclusion ?? " << std::endl;
    std::cout << "Reminder : the relative errors Er / h^2 should be (alsmot) independant of h (if the Jacobian and G are well computed)" << std::endl;
    std::cout << "If you see huge variations, go back to your Jacobian notebook :( " << std::endl;
    std::cout << "Remark : if the scheme under study is linear, just take a look at the values of the error vector ... It should be at the machine error " << std::endl;
    std::cout << "Remark : if the scheme is of (too) high order, pay attention to the saturation phenomenon, and look at the errors ..." <<std::endl; 
    std::cout << "It may be relevant to look at a lower order version of the scheme" << std::endl;

    std::cout << "\n" << sep << "\n"<< std::endl;
    exit(1); 

}  // Gaudeul_test  



template<std::size_t K>
void NewtonMethod<K>::local_Gaudeul_test_condensed( const Mesh * Th,
                                                    const GC & gc, 
                                                    const int & number_base_point, 
                                                    const int & number_dir  )
{   
    std::cout << sep << std::endl;
    std::cout << "Performing a local test of the Jacobian à la Gaudeul "<< std::endl;
    std::cout << "May it be a success ... " << std::endl;

    
    // choose the cell used for the tests 
    // std::srand(std::time(nullptr));
    // for (int i = 0; i < RAND_NUMS_TO_GENERATE; i++)
    //     cout << rand() % MAX  << "; ";


    random_device rd;   
    mt19937 gen(rd()); 
    uniform_int_distribution<> dist(0,gc.nb_cells); 
    int iT = dist(gen); 
    std::cout << "The test is performed on the cell n°" << iT << "\n" << std::endl;   
    
    // extract lc and local electrostatic potential
    const LC & lc = gc.getLC(iT);   
    const SolutionVectorType & Phi_TF = m_Phi.get_local_dofs(gc, iT); 
    Real size_data = std::pow(0.5 , K);    
    
    cout << "New base point. "<< endl;
    for(int nb_base = 0; nb_base < number_base_point; nb_base ++){
        std::cout << "Base point number "<< nb_base +1 << std::endl;

        SolutionVectorType w_base = SolutionVectorType::Random(lc.nb_tot_dofs); 
        w_base *= size_data; 

        //Create the jacobian and the goal function
        DenseMatrixType  J1; 
        DenseMatrixType  J2; 
        DenseMatrixType  J3; 
        DenseMatrixType  J4; 
        SolutionVectorType  P_base; 
        Real norm_P_cells; 

        
        m_NLcontrib.compute_local_NL_contributions(Th, iT, gc, w_base, Phi_TF,  m_stat, P_base, J1, J2, J3, J4, norm_P_cells );


        Eigen::PartialPivLU<DenseMatrixType> LU_J1; 
        LU_J1.compute(J1); 
        const DenseMatrixType inv_J1_J2 = LU_J1.solve(J2);
        const DenseMatrixType J_schur = J4 - J3 * inv_J1_J2; 
        
        cout << "New direction !"<< endl;
        for(int nb_dir = 0; nb_dir <number_dir; nb_dir++){
            cout << "Direction number "<< nb_dir +1 << endl;
            cout << "   " << endl;  
            
            //create the direction vector, according to the block under consideration 
            const SolutionVectorType w_dir_faces = SolutionVectorType::Random(lc.nb_tot_faces_dofs);
            const SolutionVectorType w_dir_cell = - inv_J1_J2 * w_dir_faces ; 
            SolutionVectorType w_dir = SolutionVectorType::Zero(lc.nb_tot_dofs); 
            w_dir.head(lc.nb_local_cell_dofs) += w_dir_cell;
            w_dir.tail(lc.nb_tot_faces_dofs) += w_dir_faces; 
            
            const SolutionVectorType J_dir_schur = J_schur * w_dir_faces;
            SolutionVectorType J_dir = SolutionVectorType::Zero(lc.nb_tot_dofs);
            J_dir.tail(lc.nb_tot_faces_dofs) += J_dir_schur; 
        
            for(int nb_h = 1; nb_h < 8; nb_h ++){
                Real h = 1. * std::pow(10.,  -nb_h);
             
                    
                SolutionVectorType w_pert = w_base + h * w_dir; // the pertubated point "base + h*dir" 
                        
                //Create the jacobian and the goal function
                DenseMatrixType  J1_p; 
                DenseMatrixType  J2_p; 
                DenseMatrixType  J3_p; 
                DenseMatrixType  J4_p; 
                SolutionVectorType  P_pert; 
                Real norm_P_pert_cells;        
                
                m_NLcontrib.compute_local_NL_contributions( Th, iT, gc, w_pert, Phi_TF, m_stat, P_pert, J1_p, J2_p, J3_p, J4_p, norm_P_pert_cells );

                const SolutionVectorType Err_glob_dofs = P_pert - P_base - h * J_dir; 
                
                Real Norm_Err = Err_glob_dofs.lpNorm<Eigen::Infinity>(); 
                
                cout << "Step h  :"<< h  <<", error / h^2  :" << Norm_Err/(h*h)   << endl;
                cout << "Error norm : " << Norm_Err   << endl;
            }//for nb_h
            cout << "\n "<< endl;
            
        } //for nb_dir
        cout << "\n"<< endl;


    } //for nb_base 

    std::cout << "Conclusion ?? " << std::endl;
    std::cout << "Reminder : the relative errors Er / h^2 should be (alsmot) independant of h (if the Jacobian and G are well computed)" << std::endl;
    std::cout << "If you see huge variations, go back to your Jacobian notebook :( " << std::endl;
    std::cout << "Remark : if the scheme under study is linear, just take a look at the values of the error vector ... It should be at the machine error " << std::endl;
    std::cout << "Remark : if the scheme is of (too) high order, pay attention to the saturation phenomenon, and look at the errors ..." <<std::endl; 
    std::cout << "It may be relevant to look at a lower order version of the scheme" << std::endl;

    std::cout << "\n" << sep << "\n"<< std::endl;
    exit(1); 

} // local_Gaudeul_test_condensed 






template<std::size_t K>
void NewtonMethod<K>::local_Gaudeul_test(   const Mesh * Th,
                                            const GC & gc, 
                                            const int & number_base_point, 
                                            const int & number_dir  )
{   
    std::cout << sep << std::endl;
    std::cout << "Performing a local test of the Jacobian à la Gaudeul "<< std::endl;
    std::cout << "May it be a success ... " << std::endl;

    
    // choose the cell used for the tests 
    // std::srand(std::time(nullptr));
    // for (int i = 0; i < RAND_NUMS_TO_GENERATE; i++)
    //     cout << rand() % MAX  << "; ";


    random_device rd;   
    mt19937 gen(rd()); 
    uniform_int_distribution<> dist(0,gc.nb_cells); 
    int iT = dist(gen); 
    std::cout << "The test is performed on the cell n°" << iT << "\n" << std::endl;   
    
    // extract lc and local electrostatic potential
    const LC & lc = gc.getLC(iT);   
    const SolutionVectorType & Phi_TF = m_Phi.get_local_dofs(gc, iT); 
    Real size_data = std::pow(0.5 , K);    
    
    cout << "New base point. "<< endl;
    for(int nb_base = 0; nb_base < number_base_point; nb_base ++){
        std::cout << "Base point number "<< nb_base +1 << std::endl;

        SolutionVectorType w_base = SolutionVectorType::Random(lc.nb_tot_dofs); 
        w_base *= size_data; 

        //Create the jacobian and the goal function
        DenseMatrixType  J1; 
        DenseMatrixType  J2; 
        DenseMatrixType  J3; 
        DenseMatrixType  J4; 
        SolutionVectorType  P_base; 
        Real norm_P_cells; 

        
        m_NLcontrib.compute_local_NL_contributions(Th, iT, gc, w_base, Phi_TF,  m_stat, P_base, J1, J2, J3, J4, norm_P_cells );

        DenseMatrixType Jac_base = DenseMatrixType::Zero(lc.nb_tot_dofs,lc.nb_tot_dofs); 
        Jac_base.topLeftCorner(lc.nb_cell_dofs,lc.nb_cell_dofs)  += J1; 
        Jac_base.topRightCorner(lc.nb_cell_dofs,lc.nb_tot_faces_dofs)  += J2; 
        Jac_base.bottomLeftCorner(lc.nb_tot_faces_dofs,lc.nb_cell_dofs)  += J3; 
        Jac_base.bottomRightCorner(lc.nb_tot_faces_dofs,lc.nb_tot_faces_dofs)  += J4; 
        
        cout << "New direction !"<< endl;
        for(int nb_dir = 0; nb_dir <number_dir; nb_dir++){
            cout << "Direction number "<< nb_dir +1 << endl;
            cout << "   " << endl;  
            
            //create the direction vector, according to the block under consideration 
            const SolutionVectorType w_dir = SolutionVectorType::Random(lc.nb_tot_dofs);
            const SolutionVectorType J_dir = Jac_base * w_dir;
        
            for(int nb_h = 1; nb_h < 8; nb_h ++){
                Real h = 1. * std::pow(10.,  -nb_h);
             
                    
                SolutionVectorType w_pert = w_base + h * w_dir; // the pertubated point "base + h*dir" 
                        
                //Create the jacobian and the goal function
                DenseMatrixType  J1_p; 
                DenseMatrixType  J2_p; 
                DenseMatrixType  J3_p; 
                DenseMatrixType  J4_p; 
                SolutionVectorType  P_pert; 
                Real norm_P_pert_cells;        
                
                m_NLcontrib.compute_local_NL_contributions( Th, iT, gc, w_pert, Phi_TF, m_stat, P_pert, J1_p, J2_p, J3_p, J4_p, norm_P_pert_cells );

                const SolutionVectorType Err_glob_dofs = P_pert - P_base - h * J_dir; 
                
                Real Norm_Err = Err_glob_dofs.lpNorm<Eigen::Infinity>(); 
                
                cout << "Step h  :"<< h  <<", error / h^2  :" << Norm_Err/(h*h)   << endl;
                cout << "Error norm : " << Norm_Err   << endl;
            }//for nb_h
            cout << "\n "<< endl;
            
        } //for nb_dir
        cout << "\n"<< endl;


    } //for nb_base 

    std::cout << "Conclusion ?? " << std::endl;
    std::cout << "Reminder : the relative errors Er / h^2 should be (alsmot) independant of h (if the Jacobian and G are well computed)" << std::endl;
    std::cout << "If you see huge variations, go back to your Jacobian notebook :( " << std::endl;
    std::cout << "Remark : if the scheme under study is linear, just take a look at the values of the error vector ... It should be at the machine error " << std::endl;
    std::cout << "Remark : if the scheme is of (too) high order, pay attention to the saturation phenomenon, and look at the errors ..." <<std::endl; 
    std::cout << "It may be relevant to look at a lower order version of the scheme" << std::endl;

    std::cout << "\n" << sep << "\n"<< std::endl;
    exit(1); 

} // local_Gaudeul_test




template<std::size_t K>
void NewtonMethod<K>::local_Gaudeul_test_by_block(  const Mesh * Th,
                                                    const GC & gc, 
                                                    const int & number_base_point, 
                                                    const int & number_dir , 
                                                    const int & id_block_tested  )
{   
    std::cout << sep << std::endl;
    std::cout << "Performing a local test of the Jacobian à la Gaudeul "<< std::endl;
    std::cout << "May it be a success ... " << std::endl;
    const int & id = id_block_tested; 
    const bool id_valid = (id == 1) || (id == 2) || (id == 3) || (id == 4); 
    if(!id_valid){
        std::cout << "You ask for an invalid value of the block" << std::endl; 
        exit(1);
    } //if the id is not valid 

    std::cout << "Jacobian block tested : " << id_block_tested << std::endl;     
    
    // choose the cell used for the tests 
    // std::srand(std::time(nullptr));
    // for (int i = 0; i < RAND_NUMS_TO_GENERATE; i++)
    //     cout << rand() % MAX  << "; ";


    random_device rd;   
    mt19937 gen(rd()); 
    uniform_int_distribution<> dist(0,gc.nb_cells); 
    int iT = dist(gen); 
    std::cout << "The test is performed on the cell n°" << iT << std::endl;   
    
         
    // extract lc and local electrostatic potential
    const LC & lc = gc.getLC(iT);   
    const SolutionVectorType & Phi_TF = m_Phi.get_local_dofs(gc, iT); 
         

    const bool first_row = (id == 1) || (id == 2); // test for a block corresponding to the fisrt row/line (J_1 or J_2 )
    const bool first_col =  (id == 1) || (id == 3); // test for a block corresponding to the fisrt column  (J_1 or J_3 )

    Real size_data = std::pow(0.5 , K);    
    
    cout << "New base point. "<< endl;
    for(int nb_base = 0; nb_base < number_base_point; nb_base ++){
        std::cout << "Base point number "<< nb_base +1 << std::endl;

        SolutionVectorType w_base = SolutionVectorType::Random(lc.nb_tot_dofs); 
        w_base *= size_data; 

        //Create the jacobian and the goal function
        DenseMatrixType  J1; 
        DenseMatrixType  J2; 
        DenseMatrixType  J3; 
        DenseMatrixType  J4; 
        SolutionVectorType  P_base; 
        Real norm_P_cells; 

        m_NLcontrib.compute_local_NL_contributions(Th, iT, gc, w_base, Phi_TF,  m_stat, P_base, J1, J2, J3, J4, norm_P_cells );

        DenseMatrixType Jac_base = DenseMatrixType::Zero(lc.nb_tot_dofs,lc.nb_tot_dofs); 
        Jac_base.topLeftCorner(lc.nb_cell_dofs,lc.nb_cell_dofs)  += J1; 
        Jac_base.topRightCorner(lc.nb_cell_dofs,lc.nb_tot_faces_dofs)  += J2; 
        Jac_base.bottomLeftCorner(lc.nb_tot_faces_dofs,lc.nb_cell_dofs)  += J3; 
        Jac_base.bottomRightCorner(lc.nb_tot_faces_dofs,lc.nb_tot_faces_dofs)  += J4; 
        
        cout << "New direction !"<< endl;
        for(int nb_dir = 0; nb_dir <number_dir; nb_dir++){
            cout << "Direction number "<< nb_dir +1 << endl;
            cout << "   " << endl;  
            
            //create the direction vector, according to the block under consideration 
            SolutionVectorType w_dir = SolutionVectorType::Zero(lc.nb_tot_dofs);

            if(first_col){
                w_dir.head(lc.nb_cell_dofs) = SolutionVectorType::Random(lc.nb_cell_dofs); 
            } // if first_col, perturbation only on the cells dofs 
            else{
                w_dir.tail(lc.nb_tot_faces_dofs) = SolutionVectorType::Random(lc.nb_tot_faces_dofs);
            }// else, second col, perturbation only on the faces dofs 
    
            const SolutionVectorType J_dir = Jac_base * w_dir;
        
            for(int nb_h = 1; nb_h < 8; nb_h ++){
                Real h = 1. * std::pow(10.,  -nb_h);
             
                    
                SolutionVectorType w_pert = w_base + h * w_dir; // the pertubated point "base + h*dir" 
                        
                //Create the jacobian and the goal function
                DenseMatrixType  J1_p; 
                DenseMatrixType  J2_p; 
                DenseMatrixType  J3_p; 
                DenseMatrixType  J4_p; 
                SolutionVectorType  P_pert; 
                Real norm_P_pert_cells;        
                
                m_NLcontrib.compute_local_NL_contributions(Th, iT, gc, w_pert, Phi_TF,  m_stat, P_pert, J1_p, J2_p, J3_p, J4_p, norm_P_pert_cells );

                const SolutionVectorType Err_glob_dofs = P_pert - P_base - h * J_dir; 
                const SolutionVectorType Err_cell = Err_glob_dofs.head(lc.nb_cell_dofs);
                const SolutionVectorType Err_faces = Err_glob_dofs.tail(lc.nb_tot_faces_dofs );
                
                const Real err_cell = Err_cell.lpNorm<Eigen::Infinity>() ;
                const Real err_faces = Err_faces.lpNorm<Eigen::Infinity>() ; 
                
                Real Norm_Err = 0.;

                if(first_row){
                    Norm_Err = err_cell; 
                }                
                else{
                    Norm_Err = err_faces; 
                }
                
                cout << "Step h  :"<< h  <<", error / h^2  :" << Norm_Err/(h*h)   << endl;
                cout << "Error norm : " << Norm_Err   << endl;
            }//for nb_h
            cout << "\n "<< endl;
            
        } //for nb_dir
        cout << "\n"<< endl;


    } //for nb_base 

    std::cout << "Conclusion ?? " << std::endl;
    std::cout << "Reminder : the relative errors Er / h^2 should be (alsmot) independant of h (if the Jacobian and G are well computed)" << std::endl;
    std::cout << "If you see huge variations, go back to your Jacobian notebook :( " << std::endl;
    std::cout << "Remark : if the scheme under study is linear, just take a look at the values of the error vector ... It should be at the machine error " << std::endl;
    std::cout << "Remark : if the scheme is of (too) high order, pay attention to the saturation phenomenon, and look at the errors ..." <<std::endl; 
    std::cout << "It may be relevant to look at a lower order version of the scheme" << std::endl;

    std::cout << "\n" << sep << "\n"<< std::endl;

} // local_Gaudeul_test_by_block 

template<std::size_t K>
Real NewtonMethod<K>::get_thresold_proj() const 
{
    return m_proj; 
} // get_thresold_proj

} //namespace NL
} //namespace ho 


#endif