#ifndef DISCRETEDATA_H
#define DISCRETEDATA_H


#include "GlobalSystem.h"
#include "Mean.h"
#include "Postproc/postProcessing.h"



// #include "Eigen/SparseCore"
// #include "Eigen/SparseLU"
// #include <unsupported/Eigen/SparseExtra>
// #include <boost/integer/static_min_max.hpp>

// #include "Common/chrono.hpp"
// typedef Eigen::MatrixXd DenseMatrixType;  
// typedef Eigen::SparseMatrix<Real> SparseMatrixType;
// typedef Eigen::SparseLU<SparseMatrixType> SolverType;
// typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> SolutionVectorType;
// typedef Eigen::Triplet<Real> TripletType;
// typedef std::vector<TripletType> TripletsVectorType;



namespace ho {
    
// Class representing the discrete unknowns used by the scheme
//
// The spatial part of the equation is of the form 
//     -div (u \lambda nabla (h(u) + \Phi) ).
//
// The quantity QFP = h(u) + \Phi is named quasi-Fermi potential,
// and u is the density 
//
// In the scheme, we use a QFP-like unknown, in the followng sense:
// given some real parameter s \in [0,1], we define 
//     w_r = w = h(u) + r*\Phi
// (the subscript _r will be omited in the following)
//
// This quantity w will be the "real" unknown of the scheme, 
// and we will call this w "pseudo quasi fermi potential", PQFP in short.
// It will be designated by either "w", "poly" (for polynomial) of PQFP.
//
// The class implemented here correspond to the data of the cells and faces dofs of w. 
//
// NB : the parameter r is defined in the Global contribution
//
// RMK : from a theoritical point of view (analysis of the scheme), 
// the value of r do not have any impact in the scheme
//

template<std::size_t K>
class DiscUnkw
{        
    typedef ho::diffusion::LocalContrib<K> LC;
    typedef ho::diffusion::GlobalContrib<K> GC;  
    typedef ho::GlobalSys<K> GS;        
    
    typedef ho::HierarchicalVectorBasis2d<K> CellVecBasis; 
    typedef ho::HierarchicalScalarBasis2d<K+1> CellBasis;    
    typedef ho::HierarchicalScalarBasis1d<K> FaceBasis; 
 

    public:     
    // construction of an empty DU (default constructor)
    DiscUnkw(); 
    //
    // construction of a constant (= val) unknown
    DiscUnkw(const Mesh * Th, const  GC & gc,  const Real & val = 0. ); 
    // !! This is not a vector unknown with constant dofs equal to val, but the unknown corresponding to a constant polynomial on faces and cells
    //
    //copy constructor
    DiscUnkw(const DiscUnkw & u); 

    //
    //return the discrete vector of dofs (u_F)_{F \in E_T}, where the id of T is iT 
    SolutionVectorType get_local_faces_dofs_vec(const GC & gc , const int & iT ) const;     
    //
    //return the discrete vector of dofs \underline{u}_T = (u_T, (u_F)_F ) 
    SolutionVectorType get_local_dofs(const GC & gc , const int & iT ) const; 
    //
    // return the coefficients of u_T (id iT)  
    SolutionVectorType get_cell_coefs(const int & iT) const; 
    //
    // return the coefficients of u_T (id iT) 
    std::vector<SolutionVectorType> get_list_cells_coefs() const; 
    
    //
    //return the vector m_X, list of the coefficient of the faces unkw 
    SolutionVectorType get_X_vector() const; 

    //
    // creation of a discrete unknown which is obtained from a Global System, with a corresponding solution which vanishes on gamma^D. Returns false if the resolution of the linear system is not sucessful 
    bool set_from_GlobalSys_zero_boundary(  const GC & gc,
                                            const GS & gs );  
    //
    //creation of a discrete unknown by interpolation
    void set_from_fonction( const Mesh * Th, 
                            const  GC & gc, 
                            const ExactSolutionType & u );    
    //
    //creation of a discrete lifting by interpolation of a continuous boundary data
    void set_lift_from_boundary_values( const Mesh * Th,
                                        const  GC & gc,
                                        const DefBoundaryConditionType & isDir,
                                        const BoundaryDirichletType & u_D ); // construction from a continuous fonction    
    //
    //creation of a discrete unknown by projection of another one on Ih_eps (Rmk : very naive implementation)
    void set_from_DU_with_proj_Ih_eps(  const Mesh * Th,
                                        const GC & gc, 
                                        const DiscUnkw & w_non_proj,                                  
                                        const DiscUnkw & Phi, 
                                        const Statistics & stat,   
                                        const Real & eps);    
    //
    //creation of a discrete unknown with random values (for Gaudeul test), faces and cells
    void set_with_Random_values( );    
    //
    //creation of a discrete unknown from edges values, using a precomputed condensation for the cells (for Gaudeul test)
    void set_from_edges_values_and_GS(const GC & gc, const GS & gs, const SolutionVectorType & X );    
    
    //
    //returns the value of the polynomial unknown at x (point of the cell T <--> iT )
    Real get_local_value_w(const GC & gc, const int & iT , const Point & x ) const;
        
    //
    // Unary operations +, - and * (product with scalar) 
    //
    DiscUnkw& operator+=(const DiscUnkw & v_disc );
    DiscUnkw& operator-=(const DiscUnkw & v_disc );
    DiscUnkw& operator*=(const Real &  alpha );
    
    //
    // Some debuguing and postprocessing features
    //
    void print_local_values_debug(  const  GC & gc,
                                    const int & iT ,
                                    const bool & faces = true,
                                    const bool & cell = true ) const;     
    
    void print_values_debug(const  GC & gc,
                            const bool & faces = true,
                            const bool & cell = true ) const; 
    
    void file_visu_poly(const std::string & name,
                        const Mesh * Th ) const; 


    void file_visu_density( const std::string & name, 
                            const Mesh * Th,
                            const DiscUnkw & Phi, 
                            const Statistics & stat ) const; 


    // --- --- --- --- --- --- --- --- 
    //nonlinear features - link betwenn quasi-fermi potentials and densities

    //
    //returns the (local) density fonction 
    FctContType get_local_density_fct(  const GC & gc, 
                                        const int iT,
                                        const DiscUnkw & Phi, 
                                        const Statistics & stat ) const;   
    //
    //returns the (local) vector (int_K u * \phi_{K,i})_i , where u is the density and phi the cell basis                                     
    SolutionVectorType get_local_density_against_test_vec(  const GC & gc, 
                                                            const int iT,
                                                            const DiscUnkw & Phi,
                                                            const Statistics & stat ) const;                                                      
    //
    // Project the DU on I_h_epsilon (NON IMPLEMENTED YET !!!!!!)
    DiscUnkw get_proj_on_Ih_eps(const Mesh * Th,
                                const GC & gc, 
                                const DiscUnkw & Phi, 
                                const Statistics & stat,
                                const Real & eps) const ;                        
    //
    // Project the DU the space of constants DU
    void filter_low_order(const Mesh * Th, const GC & gc) ;
    //Une idée pour l'implémentation : 
    // passer en u = g(w - r\Phi), fonction continue 
    // puis créer u_eps, la fonction u tronquée (toujours au niveau continu)
    // interpoler h(u_eps)+r\Phi pour obtenir un w tronqué dans \R !!   

    // --- --- --- --- --- --- --- --- 
    //Some computation of norms

    //
    // returns the L^2 square norm of w 
    Real L2_square_norm_poly( const  GC & gc) const;      
    //
    // returns the L^2 square norm of u   
    Real L2_square_norm_density( const  GC & gc, const DiscUnkw & Phi, const Statistics & stat ) const;    
    //
    // returns the L^2 square norm of the discrete gradient of w   
    Real L2_square_norm_grad_poly( const  GC & gc) const;    
    //
    // returns the L^2 square norm of the discrete gradient of the Quasi-Fermi potential     
    Real L2_square_norm_grad_QFP( const  GC & gc, const DiscUnkw & Phi) const;    
    //
    // returns the Boltzman-like entropy (not the relative one, can be negative)    
    Real entropy( const  GC & gc, const DiscUnkw & Phi, const Statistics & stat) const;
    //
    // return the L^infty norm of the vector X (coefficients of the faces unknowns)
    Real L_infty_norm_coefs_faces() const;     
    //
    // return the L^infty norm of the coefficients of the cells unknowns)
    Real L_infty_norm_coefs_cells() const;     
    //
    // return the L^infty norm of the coefficients (cells and faces)
    Real L_infty_norm_coefs() const;

    // --- --- --- --- --- --- --- --- 
    // some errors computations

    //
    //returns the L_p error  (with respect to the continuous fonction u_ex)
    Real Error_Lp(  const GC & gc, 
                    const DiscUnkw & Phi, 
                    const Statistics & stat, 
                    const int & p, 
                    const FctContType & u_ex,
                    Real & norm_ex ) const;     
    //
    //returns the L_p error on the gradient  (with respect to the continuous gradient G_ex)
    Real Error_grad_L2(  const GC & gc, 
                    const DiscUnkw & Phi, 
                    const Statistics & stat, 
                    const ExactGradientType & G_ex,
                    Real & norm_grad_ex ) const; 
    //
    //returns the relative entropy ("Boltzmann-type")  (with respect to the continuous reference function u_ref)
    Real get_relative_entropy(  const GC & gc, 
                                const DiscUnkw & Phi, 
                                const Statistics & stat, 
                                const FctContType & u_ref ) const; 

    // void Apply_function(const RealFctType & fct);


    
    // Real Ks_value(const int & iK, const int & iE) const;
    // Real D_Ks(const int & iK, const int & iE) const; 
    // Real D_s( const int & iE) const; 
    // Real L2_square_norm() const;
    // Real false_L2_square_norm() const; // for WIAS strategy : do not account of the jump values
    // Real H1_square_norm() const;
    // Real L_infty_norm() const; 


    //~Discrete_u();

    private:
    Real m_r; // the "exponential fitting" parameter (w = h(u) + r*\Phi )
    int m_nb_faces_dofs;
    int m_nb_local_cell_dofs;
    int m_nb_cells; 
    SolutionVectorType m_X; // the faces dofs (global labelling)
    std::vector<SolutionVectorType> m_cells; // a vector of coefficient list for the cell dofs 

}; //class DiscUnkw


template<std::size_t K>
DiscUnkw<K> operator+(const  DiscUnkw<K> & u, const DiscUnkw<K> & v);
template<std::size_t K>
DiscUnkw<K> operator-(const  DiscUnkw<K> & u, const DiscUnkw<K> & v);
template<std::size_t K>
DiscUnkw<K> operator*(const Real & alpha , const  DiscUnkw<K> & u); 





//------------------------------------------------------------------------------
// Implementation DiscrUnkw

template<std::size_t K>
DiscUnkw<K>::DiscUnkw()
{   
    m_r = 0.; 
    m_nb_cells = 0; 
    m_nb_faces_dofs = 0;
    m_nb_local_cell_dofs = LC::nb_local_cell_dofs;
    m_X = SolutionVectorType::Zero(m_nb_faces_dofs);
    m_cells.resize( m_nb_cells );
    for(int iT = 0; iT < m_nb_cells; iT ++){
        m_cells[iT] = SolutionVectorType::Zero(m_nb_local_cell_dofs);
    }

} // construction of a constant (= val) unknown

template<std::size_t K>
DiscUnkw<K>::DiscUnkw(const Mesh * Th, const  GC & gc,  const Real & val )
{   
    m_r = gc.r; 
    m_nb_cells = gc.nb_cells; 
    m_nb_faces_dofs = gc.nb_faces_dofs;
    m_nb_local_cell_dofs = LC::nb_local_cell_dofs;
    m_X = SolutionVectorType::Zero(m_nb_faces_dofs);
    m_cells.resize( m_nb_cells );
    for(int iT = 0; iT < m_nb_cells; iT ++){
        m_cells[iT] = SolutionVectorType::Zero(m_nb_local_cell_dofs);
    }

    if(val != 0.){
        for(int iT = 0; iT < m_nb_cells; iT ++){
            m_cells[iT](0) = val;
        } // for iT        
        for(int iF = 0; iF < gc.nb_faces ; iF ++){
            const int offset_F = iF * LC::nb_local_face_dofs;
            m_X(offset_F) = val;
        } // for iT
    }// if val is not 0

} // construction of a constant (= val) unknown

template<std::size_t K>
DiscUnkw<K>::DiscUnkw(const DiscUnkw & u): 
    m_r (u.m_r),
    m_nb_faces_dofs(u.m_nb_faces_dofs),
    m_nb_local_cell_dofs(u.m_nb_local_cell_dofs),
    m_nb_cells(u.m_nb_cells),
    m_X(u.m_X), 
    m_cells(u.m_cells) 
    {} // copy constructor 
    //copy constructor

template<std::size_t K>
SolutionVectorType DiscUnkw<K>::get_local_faces_dofs_vec(const GC & gc , const int &  iT ) const
{
    const LC & lc = gc.getLC(iT); 
    SolutionVectorType loc_vec_faces = SolutionVectorType::Zero(lc.nb_tot_faces_dofs);
    const IndexVectorType & idx_T = lc.idx_T;  
    for(int i = 0; i < idx_T.size(); i++) { 
        loc_vec_faces(i) = m_X(idx_T(i)); 
    } // for i 
    return loc_vec_faces;
} // get_local_faces_dofs_vec 


template<std::size_t K>
SolutionVectorType DiscUnkw<K>::get_local_dofs(const GC & gc , const int &  iT ) const
{
    const LC & lc = gc.getLC(iT); 
    SolutionVectorType loc_vec = SolutionVectorType::Zero(lc.nb_tot_dofs);
    loc_vec.head(lc.nb_local_cell_dofs) = m_cells[iT];
    loc_vec.tail(lc.nb_tot_faces_dofs) = get_local_faces_dofs_vec( gc , iT );
    return loc_vec;
} // get_local_dofs


template<std::size_t K>
SolutionVectorType DiscUnkw<K>::get_cell_coefs(const int & iT) const
{
    return m_cells[iT];

} // cell_coefs 
    
    
template<std::size_t K>
std::vector<SolutionVectorType> DiscUnkw<K>::get_list_cells_coefs() const
{
    return m_cells; 

} // list_cells_coefs


template<std::size_t K>
SolutionVectorType DiscUnkw<K>::get_X_vector() const
{
    return m_X;
    
} // get_X_vector 


template<std::size_t K>
bool DiscUnkw<K>::set_from_GlobalSys_zero_boundary( const GC & gc,
                                                    const GS & gs )
{   
    bool success = true; 
    SolverType Solver_faces;    
    gs.get_LU_Solver_global(gc , Solver_faces );  

    // SolutionVectorType glob_RHS = gs.get_global_RHS();    std::cout << "test13"<< std::endl;
    // SolutionVectorType glob_RHS_resize = gc.resize_mat * glob_RHS ;    std::cout << "test14"<< std::endl;
    // SolutionVectorType Sol = Solver_faces.solve( glob_RHS_resize );  std::cout << "test15"<< std::endl;
    // m_X = (gc.resize_mat.transpose()) *Sol ; std::cout << "test16"<< std::endl;
    //   
    //std::cout << "RHS ?? \n" <<    gs.get_global_RHS() <<std::endl; 
    //std::cout << "Matrix ?? \n" <<    gs.get_global_matrix() <<std::endl; 
    if(Solver_faces.info()!= Eigen::ComputationInfo::Success) {
        std::cout << "Solver (face dofs) : failure" << std::endl; 
        return false;
    } // 

    m_X = (gc.resize_mat.transpose()) * Solver_faces.solve( gc.resize_mat * ( gs.get_global_RHS() ) ); // !!!
    //std::cout << "sol coeffs ??\n" << m_X << std::endl;
    //std::cout << "Computation sucess ??" << Solver_faces.info() << std::endl;
    // !!
    // !!
    // !!
    // here, the question of the boundary conditions !!!!
    // works only if the considered unknown is zero on the dirichlet boundary 
    for(int iT = 0; iT < m_nb_cells; iT ++){
        const SolutionVectorType & faces_T = get_local_faces_dofs_vec( gc, iT);  
        m_cells[iT] = gs.get_vec_reconstruct(iT) - gs.get_matrix_reconstruct(iT) * faces_T; 
        //std::cout << "sol coeffs  cell " << iT << "?? \n " << m_cells[iT] << std::endl;
    } // for iT     
    
    //if(Solver_faces.info()!= Eigen::ComputationInfo::Success) { success = false;} 
    return true; //success;  

}// set_from_GlobalSys_zero_boundary


template<std::size_t K>
void DiscUnkw<K>::set_from_fonction(const Mesh * Th, 
                                    const  GC & gc, 
                                    const ExactSolutionType & u )
{
    m_X =  SolutionVectorType::Zero(m_nb_faces_dofs);  
    std::vector<bool> face_non_asigned(  gc.nb_faces, true );  
    
    for(int iT = 0; iT < m_nb_cells; iT ++){
        const Cell & T = Th->cell(iT);
        const LC & lc = gc.getLC(iT); 
        Eigen::VectorXd g_iF = Eigen::VectorXd::Zero(lc.nb_tot_faces_dofs);
        Eigen::VectorXd u_T_I = lc.interpolate(Th, iT, u );
        m_cells[iT] = u_T_I.head(m_nb_local_cell_dofs);

        for(int iF_loc = 0; iF_loc < lc.m ; iF_loc++){
            const int iF = T.faceId(iF_loc);
            if(face_non_asigned[iF]){
                g_iF.segment(iF_loc * LC::nb_local_face_dofs, LC::nb_local_face_dofs) = u_T_I.segment(LC::nb_cell_dofs + iF_loc * LC::nb_local_face_dofs, LC::nb_local_face_dofs);
                face_non_asigned[iF] = false; 
            } // if F is non asigned
        } // for iF_loc
       
        
        for(int i = 0; i < lc.idx_T.size(); i++) {
            m_X(lc.idx_T(i)) += g_iF(i);
        } // for i
    } //for iT
}


template<std::size_t K>
void DiscUnkw<K>::set_lift_from_boundary_values(    const Mesh * Th,
                                                    const  GC & gc,
                                                    const DefBoundaryConditionType & isDir,
                                                    const BoundaryDirichletType & u_D )
{
    for(int iT = 0; iT < m_nb_cells; iT ++){
        m_cells[iT] =  SolutionVectorType::Zero(m_nb_local_cell_dofs); 
    } // for iT 

    m_X = gc.getInterpolationDirBoundary(Th, isDir, u_D);

} // construction of a discrete lifting from a continuous fonction



template<std::size_t K>
void DiscUnkw<K>::set_from_DU_with_proj_Ih_eps( const Mesh * Th,
                                                const GC & gc, 
                                                const DiscUnkw & w_non_proj,                                  
                                                const DiscUnkw & Phi, 
                                                const Statistics & stat,   
                                                const Real & eps)
{   
    m_X =  SolutionVectorType::Zero(m_nb_faces_dofs); 
    m_cells.resize( gc.nb_cells );  
    std::vector<bool> face_non_asigned(  gc.nb_faces, true );  
    
    const Real & r = gc.r;
    const DiscUnkw arg = w_non_proj - r * Phi; 
    
    for(int iT = 0; iT < m_nb_cells; iT ++){
        const LC & lc = gc.getLC(iT); 
        const SolutionVectorType & arg_TF = arg.get_local_dofs( gc, iT); 
        const SolutionVectorType & Phi_TF = Phi.get_local_dofs( gc, iT); 
        const std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > & feval_basisT_PTF = lc.feval_basisT_PTF; 
        const std::vector<std::shared_ptr<BasisFunctionEvaluation<FaceBasis> > > & feval_basisF = lc.feval_basisF; 

        SolutionVectorType bT = SolutionVectorType::Zero(lc.nb_local_cell_dofs); 
    
        for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {

            // projection of the cells dofs 
            const PyramidIntegrator & pim = *lc.pims[iF_loc];
            for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
                const Real & wQN = pim.weight(iQN);
                Real arg_iqn = 0.; 
                Real Phi_iqn = 0.; 
                for(int i =0; i< lc.nb_local_cell_dofs; i++){
                    const typename CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc]) (i, iQN);
                    arg_iqn += arg_TF(i) * phi_i_iqn;
                    Phi_iqn += Phi_TF(i) * phi_i_iqn; 
                } // for i 
                const Real u_iqn = stat.g(arg_iqn); 
                const Real proj_u_iqn = stat.proj_Ih_eps(u_iqn , eps); 
                const Real w_proj_iqn = stat.h(proj_u_iqn) + r * Phi_iqn;
                for(int i =0; i< lc.nb_local_cell_dofs; i++){
                    const typename CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc]) (i, iQN);
                    bT(i) += wQN * w_proj_iqn * phi_i_iqn;
                } // for i 
            } // for iQN            
            
            
            // projection of the faces dofs 
            const int iF = (Th->cell(iT)).faceId(iF_loc); //get the global id of the face 
            if(face_non_asigned[iF]){
                std::size_t offset_F = lc.nb_cell_dofs + iF_loc * lc.nb_local_face_dofs;
                const FaceIntegrator & fim = *lc.fims[iF_loc];
                Eigen::VectorXd bF = Eigen::VectorXd::Zero(lc.nb_local_face_dofs);
                for(int iQN = 0; iQN < fim.numberOfPoints(); iQN++) {
                    const Real & wQN = fim.weight(iQN);            
                    Real arg_iqn = 0.; 
                    Real Phi_iqn = 0.; 
                    for(std::size_t i = 0; i < lc.nb_local_face_dofs; i++) {
                        const typename FaceBasis::ValueType & phi_i_iqn = (*feval_basisF[iF_loc]) (i, iQN);
                        const int i_face = i + offset_F; 
                        arg_iqn += arg_TF(i_face) * phi_i_iqn;
                        Phi_iqn += Phi_TF(i_face) * phi_i_iqn; 
                    } // for i                
                    const Real u_iqn = stat.g(arg_iqn); 
                    const Real proj_u_iqn = stat.proj_Ih_eps(u_iqn , eps); 
                    const Real w_proj_iqn = stat.h(proj_u_iqn) + r * Phi_iqn;
                    for(std::size_t i = 0; i < lc.nb_local_face_dofs; i++) {
                        const typename FaceBasis::ValueType & phi_i_iqn = (*feval_basisF[iF_loc]) (i, iQN);
                        bF(i) += wQN * w_proj_iqn * phi_i_iqn;
                    } // for i            
                } // for iQN
                SolutionVectorType w_F = lc.LU_MFF[iF_loc].solve(bF);        
                for(int i = 0; i < lc.nb_local_face_dofs; i++) {
                    const int & i_f = i + iF_loc * lc.nb_local_face_dofs; 
                    m_X(lc.idx_T(i_f)) = w_F(i_f);
                } // for i
                face_non_asigned[iF] = false; 
            }// if face iF is non assigned

        } // for iF_loc
        
        m_cells[iT] = lc.LU_MTT.solve(bT); 
    } // for iT 

} // set_from_DU_with_proj_Ih_eps 


template<std::size_t K>
void DiscUnkw<K>::set_with_Random_values()
{
    m_X = SolutionVectorType::Random(m_nb_faces_dofs);
    for(int iT = 0; iT < m_nb_cells; iT ++){
        m_cells[iT] = SolutionVectorType::Random(m_nb_local_cell_dofs);
    } //for iT 
} //set_with_Random_values


template<std::size_t K>
void DiscUnkw<K>::set_from_edges_values_and_GS( const GC & gc,
                                                const GS & gs,
                                                const SolutionVectorType & X )
{
    m_X = X; 
    for(int iT = 0; iT < m_nb_cells; iT ++){
        const SolutionVectorType & faces_T = get_local_faces_dofs_vec( gc, iT);  
        m_cells[iT] =- gs.get_matrix_reconstruct(iT) * faces_T; 
    } // for iT 
}      

template<std::size_t K>
Real DiscUnkw<K>::get_local_value_w(const GC & gc, const int & iT , const Point & x ) const
{
    const LC & lc = gc.getLC(iT); 
    const CellBasis Basis_T =  *lc.basisT; 
    Real w_x = 0.; 
    for(int i = 0; i< lc.nb_cell_dofs; i ++){
        w_x += m_cells[iT](i) * Basis_T.phi(i).phi(x);
    } //for i 
    return w_x; 

} //get_local_value_poly

// 
// implementation of operations on the discrete unknowns (~ surcharge += ...)
//

template<std::size_t K>
DiscUnkw<K> & DiscUnkw<K>::operator+=(const DiscUnkw & v)
{   
    m_X += v.m_X;
    
    for(int iT =0; iT < m_nb_cells; iT ++){
        m_cells[iT] += v.m_cells[iT];
    } // for iT

    return *this;
}

template<std::size_t K>
DiscUnkw<K> & DiscUnkw<K>::operator-=(const DiscUnkw & v)
{   
    m_X -= v.m_X;
    
    for(int iT =0; iT < m_nb_cells; iT ++){
        m_cells[iT] -= v.m_cells[iT];
    } // for iT

    return *this;
}

template<std::size_t K>
DiscUnkw<K> & DiscUnkw<K>::operator*=(const Real &  alpha )
{
    m_X = alpha * m_X; 
    
    for(int iT =0; iT < m_nb_cells; iT ++){
        m_cells[iT] = alpha * m_cells[iT];
    } // for iT

    return *this;  
}


template<std::size_t K>
void DiscUnkw<K>::print_local_values_debug( const  GC & gc,
                                            const int & iT, 
                                            const bool & faces, 
                                            const bool & cell ) const
{
    std::cout <<"Values of the discrete vector [debug]\nLocal information: cell  n°" << iT << std::endl;
    if(faces){
        const SolutionVectorType & faces_dofs_vec =  get_local_faces_dofs_vec( gc, iT ); 
        std::cout <<"Faces coeffs:\n" << std::endl;
        std::cout << faces_dofs_vec <<std::endl;    
    } //if faces

    if(cell){
        std::cout << "Cell coeffs:\n" << std::endl; 
        std::cout << get_cell_coefs(iT); 
    } //if cells

    std::cout <<"That's all [end debug] \n"  <<  std::endl;
} // print_local_values_debug


template<std::size_t K>
void DiscUnkw<K>::print_values_debug(   const  GC & gc,
                                        const bool & faces, 
                                        const bool & cell ) const
{
    std::cout <<"Values of the discrete vector [debug]\nGlobal information" << std::endl;
    if(faces){
        std::cout <<"Faces coeffs (global vector X):\n" << std::endl;
        std::cout << m_X <<std::endl;    
    } //if faces

    if(cell){
        std::cout << "Cell coeffs:\n" << std::endl;
        for(int iT = 0; iT < m_nb_cells; iT ++){
            std::cout <<"Cells n°"<< iT <<" coeffs: \n" << m_cells[iT] <<  std::endl;
        } // for iT 
    } //if cells

    std::cout <<"That's all [end debug] \n"  <<  std::endl;
} // print_local_values_debug




template<std::size_t K>
void DiscUnkw<K>::file_visu_poly(const std::string & name , const Mesh * Th )const
{
    postProcessing_poly<ho::HierarchicalScalarBasis2d<K+1>, boost::static_unsigned_min<K+1,3>::value>(name, Th, m_cells);
} // file_visu_poly


template<std::size_t K>
void DiscUnkw<K>::file_visu_density(const std::string & name, 
                                    const Mesh * Th, 
                                    const DiscUnkw & Phi, 
                                    const Statistics & stat) const
{
    postProcessing_density<ho::HierarchicalScalarBasis2d<K+1>, boost::static_unsigned_min<K+1,3>::value>(name, Th, stat, m_cells, Phi.get_list_cells_coefs(), m_r);
} //file_visu_g_unkw


template<std::size_t K>
FctContType DiscUnkw<K>::get_local_density_fct( const GC & gc, 
                                                const int iT,
                                                const DiscUnkw & Phi, 
                                                const Statistics & stat) const
{
    const LC & lc = gc.getLC(iT); 
    const SolutionVectorType & coeff_T = m_cells[iT] - m_r * Phi.get_cell_coefs(iT);
    const CellBasis &  basisT =  *lc.basisT;  
    const int & size = basisT.size;
    
    FctContType density_T =[stat,basisT,coeff_T,size](const Point & x) -> Real {
        Real arg = 0.; 
        for(int i = 0; i < size; i++){
            arg += coeff_T[i] * basisT.phi(i).phi(x);
        } //for i
        return stat.g(arg); 
    }; // density_T

    return density_T; 
} //get_local_density_fct

template<std::size_t K>
SolutionVectorType DiscUnkw<K>::get_local_density_against_test_vec( const GC & gc, 
                                                                    const int iT,
                                                                    const DiscUnkw & Phi, 
                                                                    const Statistics & stat) const
{
    const LC & lc = gc.getLC(iT); 
    const SolutionVectorType & coeff_T = m_cells[iT] - m_r * Phi.get_cell_coefs(iT);
    SolutionVectorType Vec_density_vs_test = SolutionVectorType::Zero(lc.nb_cell_dofs);
    
    std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > feval_basisT_PTF = lc.feval_basisT_PTF; 
    for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
        const PyramidIntegrator & pim = *lc.pims[iF_loc];            
        for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
            const Point & xQN = pim.point(iQN);
            const Real & wQN = pim.weight(iQN);
            Real arg_iqn = 0 ;         
            for(std::size_t i = 0; i < lc.nb_cell_dofs; i++) {
                const typename LC::CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
                arg_iqn += coeff_T[i] * phi_i_iqn;
            } // for i
            for(std::size_t j = 0; j < lc.nb_cell_dofs; j ++){
                const typename LC::CellBasis::ValueType & phi_j_iqn = (*feval_basisT_PTF[iF_loc])(j, iQN);
                Vec_density_vs_test(j) += wQN *stat.g(arg_iqn) * phi_j_iqn;
            } //for j 
        }//for iQN 
    } // For iF_loc
    return Vec_density_vs_test;

}//get_local_density_against_test_vec



template<std::size_t K>
DiscUnkw<K> DiscUnkw<K>::get_proj_on_Ih_eps(const Mesh * Th,
                                            const GC & gc, 
                                            const DiscUnkw & Phi, 
                                            const Statistics & stat,
                                            const Real & eps) const
{   

    // DiscUnkw proj = 0.*DiscUnkw(Phi);  
    // proj.set_from_DU_with_proj_Ih_eps(gc, *this, Phi, stat, eps); 
    //return proj; 
    return *this; 
} // get_proj_on_Ih_eps 


template<std::size_t K>
void DiscUnkw<K>::filter_low_order(const Mesh * Th, const GC & gc)
{
    std::vector<bool> face_non_asigned(  gc.nb_faces, true );  
    
    
    for(int iT = 0; iT < m_nb_cells; iT ++){
        const LC & lc = gc.getLC(iT);        
        const std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > & feval_basisT_PTF = lc.feval_basisT_PTF; 
        const std::vector<std::shared_ptr<BasisFunctionEvaluation<FaceBasis> > > & feval_basisF = lc.feval_basisF;  
        
        const SolutionVectorType & w_TF = get_local_dofs(gc,iT); 

        Real m_w_T = 0; // mass of w_T

        for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {

            // computation of the mass of the cell unknown
            const PyramidIntegrator & pim = *lc.pims[iF_loc];
            for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++) {                     
                const Real & wQN = pim.weight(iQN);            
                for(int i =0; i< lc.nb_local_cell_dofs; i++){
                    const typename CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc]) (i, iQN);
                    m_w_T +=  wQN * w_TF(i) *  phi_i_iqn; 
                } // for i 
            } // for iQN            
            
            
            // projection of the faces dofs 
            
            const Cell & T = Th->cell(iT); 
            const int & iF = T.faceId(iF_loc); //get the global id of the face 
            if(face_non_asigned[iF]){
                Real m_w_F = 0.; // mass of the face unknown
                std::size_t offset_F = lc.nb_cell_dofs + iF_loc * lc.nb_local_face_dofs;
                const FaceIntegrator & fim = *lc.fims[iF_loc];
                for(int iQN = 0; iQN < fim.numberOfPoints(); iQN++) {
                    const Real & wQN = fim.weight(iQN);                     
                    for(std::size_t i = 0; i < lc.nb_local_face_dofs; i++) {
                        const typename FaceBasis::ValueType & phi_i_iqn = (*feval_basisF[iF_loc]) (i, iQN);
                        const int i_face = i + offset_F; 
                        m_w_F += wQN * w_TF(i_face) * phi_i_iqn; 
                    } // for i            
                } // for iQN                
                
                const int offset_F_loc  = iF_loc * lc.nb_local_face_dofs; 
                m_X(lc.idx_T(offset_F_loc)) = m_w_F; 
                for(int i = 1; i < lc.nb_local_face_dofs; i++) {
                    const int & i_f = i + iF_loc * lc.nb_local_face_dofs; 
                    m_X(lc.idx_T(i +offset_F_loc)) = 0.;
                } // for i
                
                face_non_asigned[iF] = false; 

            }// if face iF is non assigned

        } // for iF_loc
        
        m_cells[iT] *= 0.;
        m_cells[iT](0) = m_w_T; 
    } // for iT 

} // filter_low_order

template<std::size_t K>
Real DiscUnkw<K>::L2_square_norm_poly(const GC & gc) const
{
    Real norm_square = 0.;
    for(int iT = 0; iT < m_nb_cells ; iT ++){
        const LC & lc = gc.getLC(iT); 
        norm_square +=  m_cells[iT].transpose()  * lc.MTT * m_cells[iT].transpose(); 
    } // for iT 
    return norm_square; 
} // L2_square_norm_poly


template<std::size_t K>
Real DiscUnkw<K>::L2_square_norm_density( const  GC & gc, const DiscUnkw & Phi ,const Statistics & stat ) const
{   
    Real norm_square = 0.;
    for(int iT = 0; iT < m_nb_cells ; iT ++){
        const LC & lc = gc.getLC(iT);
        const SolutionVectorType & coeff_T = m_cells[iT] - m_r * Phi.get_cell_coefs(iT); 
        std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > feval_basisT_PTF = lc.feval_basisT_PTF; 
        for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
            const PyramidIntegrator & pim = *lc.pims[iF_loc];            
            for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
                const Real & wQN = pim.weight(iQN);
                Real arg_iqn = 0 ;
                for(std::size_t i = 0; i < lc.nb_cell_dofs; i++) {
                    const typename LC::CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
                    arg_iqn += coeff_T[i] * phi_i_iqn;
                } // for i
                norm_square += wQN * std::pow(stat.g(arg_iqn),2);
            }//for iQN 
        } // For iF_loc
    } // for iT 
    return norm_square;
} // L2_square_norm_density


template<std::size_t K>
Real DiscUnkw<K>::L2_square_norm_grad_poly(const GC & gc) const
{
    Real norm_square_grad = 0.;
    for(int iT = 0; iT < m_nb_cells ; iT ++){
        const LC & lc = gc.getLC(iT);    
        std::size_t NG_full = lc.NG_full;    
        // Coefficients
        Eigen::VectorXd G_coeffs = lc.G_full_T * get_local_dofs( gc ,  iT);             
        std::vector<std::shared_ptr<BasisFunctionEvaluation<CellVecBasis> > > feval_vec_basisT_PTF = lc.feval_vec_basisT_PTF;
        for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
            const PyramidIntegrator & pim = *lc.pims[iF_loc];            
            for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
                const Point & xQN = pim.point(iQN);
                const Real & wQN = pim.weight(iQN);
                Eigen::Matrix<Real, 2, 1> G_iqn = Eigen::Matrix<Real, 2, 1>::Zero();
                for(std::size_t i = 0; i < NG_full; i++) {
                    const typename LC::CellVecBasis::ValueType & Tau_i_iqn = (*feval_vec_basisT_PTF[iF_loc])(i, iQN);
                    G_iqn += G_coeffs(i) * Tau_i_iqn;
                } // for i
                norm_square_grad += wQN * G_iqn.squaredNorm();
            }//for iQN 
        } // For iF_loc
    } // for iT 

    return norm_square_grad; 
}  // L2_square_norm_grad_poly 


template<std::size_t K>
Real DiscUnkw<K>::L2_square_norm_grad_QFP( const  GC & gc, const DiscUnkw & Phi) const
{
    Real norm_square_grad = 0.;
    for(int iT = 0; iT < m_nb_cells ; iT ++){
        const LC & lc = gc.getLC(iT);    
        std::size_t NG_full = lc.NG_full;    
        // Coefficients
        const SolutionVectorType QFP_TF = get_local_dofs( gc ,  iT) + (1. - m_r ) *Phi.get_local_dofs( gc ,  iT); 
        Eigen::VectorXd G_coeffs = lc.G_full_T * QFP_TF;             
        std::vector<std::shared_ptr<BasisFunctionEvaluation<CellVecBasis> > > feval_vec_basisT_PTF = lc.feval_vec_basisT_PTF;
        for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
            const PyramidIntegrator & pim = *lc.pims[iF_loc];            
            for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
                const Point & xQN = pim.point(iQN);
                const Real & wQN = pim.weight(iQN);
                Eigen::Matrix<Real, 2, 1> G_iqn = Eigen::Matrix<Real, 2, 1>::Zero();
                for(std::size_t i = 0; i < NG_full; i++) {
                    const typename LC::CellVecBasis::ValueType & Tau_i_iqn = (*feval_vec_basisT_PTF[iF_loc])(i, iQN);
                    G_iqn += G_coeffs(i) * Tau_i_iqn;
                } // for i
                norm_square_grad += wQN * G_iqn.squaredNorm();
            }//for iQN 
        } // For iF_loc
    } // for iT 

    return norm_square_grad; 

}  
    
    
template<std::size_t K>
Real DiscUnkw<K>::entropy( const  GC & gc, const DiscUnkw & Phi, const Statistics & stat) const
{
    Real entropy = 0.;
    for(int iT = 0; iT < m_nb_cells; iT++){
        Real entropy_T =0.;
        const LC & lc = gc.getLC(iT); 
        const SolutionVectorType & coeff_arg_T = m_cells[iT] - m_r * Phi.get_cell_coefs(iT); 
        const SolutionVectorType & coeff_Phi_T = Phi.get_cell_coefs(iT); 
        std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > feval_basisT_PTF = lc.feval_basisT_PTF; 
        for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
            const PyramidIntegrator & pim = *lc.pims[iF_loc];            
            for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
                const Point & xQN = pim.point(iQN);
                const Real & wQN = pim.weight(iQN);
                Real arg_iqn = 0.;
                Real Phi_iqn = 0.; 
                for(std::size_t i = 0; i < lc.nb_cell_dofs; i++) {
                    const typename LC::CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
                    arg_iqn += coeff_arg_T(i) * phi_i_iqn;
                    Phi_iqn += coeff_Phi_T(i) * phi_i_iqn;
                } // for i
                const Real u_iqn = stat.g(arg_iqn); 
                entropy_T += wQN * (stat.H(u_iqn) + u_iqn * Phi_iqn  ) ;   
            }//for iQN 
        } // For iF_loc
        entropy += entropy_T; 
    } //for iT  
    return entropy;    

} //entropy 

template<std::size_t K>
Real DiscUnkw<K>::L_infty_norm_coefs_faces() const
{
    return m_X.lpNorm<Eigen::Infinity>(); 
} //L_infty_norm_coefs_faces


template<std::size_t K>
Real DiscUnkw<K>::L_infty_norm_coefs_cells() const
{
    Real max_cells =  m_cells[0].lpNorm<Eigen::Infinity>();
    for(int iT = 1; iT < m_nb_cells; iT ++){
        max_cells = std::max(max_cells,  m_cells[iT].lpNorm<Eigen::Infinity>());
    } // for iT 
    return max_cells; 
} //L_infty_norm_coefs_cells


template<std::size_t K>
Real DiscUnkw<K>::L_infty_norm_coefs() const
{
    return std::max(L_infty_norm_coefs_cells(), L_infty_norm_coefs_faces() );  
} //L_infty_norm_coefs


template<std::size_t K>
Real DiscUnkw<K>::Error_Lp( const GC & gc,  
                            const DiscUnkw & Phi, 
                            const Statistics & stat,                     
                            const int & p, 
                            const FctContType & u_ex,
                            Real & norm_ex) const
{   
    Real norm_err = 0.;
    Real norm_ex_temp = 0.; 
    for(int iT = 0; iT < m_nb_cells; iT++){
        Real norm_err_T =0.;
        Real norm_ex_T =0.; 
        const LC & lc = gc.getLC(iT); 
        const SolutionVectorType & coeff_T = m_cells[iT] - m_r * Phi.get_cell_coefs(iT); 
        std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > feval_basisT_PTF = lc.feval_basisT_PTF; 
        for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
            const PyramidIntegrator & pim = *lc.pims[iF_loc];            
            for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
                const Point & xQN = pim.point(iQN);
                const Real & wQN = pim.weight(iQN);
                Real arg_iqn = 0 ;
                for(std::size_t i = 0; i < lc.nb_cell_dofs; i++) {
                    const typename LC::CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
                    arg_iqn += coeff_T(i) * phi_i_iqn;
                } // for i
                norm_err_T += wQN * std::pow( std::fabs(stat.g(arg_iqn) - u_ex(xQN)),p);            
                norm_ex_T += wQN * std::pow( std::fabs(u_ex(xQN)),p);
            }//for iQN 
        } // For iF_loc

        norm_err += norm_err_T; 
        norm_ex_temp += norm_ex_T; 
    } //for iT 
    
    norm_ex = std::pow(norm_ex_temp, 1./p); 

    return std::pow(norm_err, 1./p); 

} // Error_L_p_loc


template<std::size_t K>
Real DiscUnkw<K>::Error_grad_L2(const GC & gc, 
                                const DiscUnkw & Phi, 
                                const Statistics & stat, 
                                const ExactGradientType & G_ex,
                                Real & norm_grad_ex ) const
{
   
    Real norm_err = 0.;
    Real norm_ex_temp = 0.; 

    for(int iT = 0; iT < m_nb_cells ; iT ++){
        const LC & lc = gc.getLC(iT);          
        Real norm_err_T =0.;
        Real norm_ex_T =0.;       
        // Coefficients
        const SolutionVectorType arg_TF = get_local_dofs( gc ,  iT) - m_r * Phi.get_local_dofs(gc, iT); 
        const SolutionVectorType G_coeffs = lc.G_full_T * arg_TF ;             
        std::vector<std::shared_ptr<BasisFunctionEvaluation<CellVecBasis> > > feval_vec_basisT_PTF = lc.feval_vec_basisT_PTF;
        std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > feval_basisT_PTF = lc.feval_basisT_PTF;

        for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
            const PyramidIntegrator & pim = *lc.pims[iF_loc];            
            for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
                const Point & xQN = pim.point(iQN);
                const Real & wQN = pim.weight(iQN);
                
                // compute the discrete gradient of w - r*Phi
                Eigen::Matrix<Real, 2, 1> G_arg_iqn = Eigen::Matrix<Real, 2, 1>::Zero();
                for(std::size_t i = 0; i < lc.NG_full; i++) {
                    const typename LC::CellVecBasis::ValueType & Tau_i_iqn = (*feval_vec_basisT_PTF[iF_loc])(i, iQN);
                    G_arg_iqn += G_coeffs(i) * Tau_i_iqn;
                } // for i

                //compute w - r*Phi and the density
                Real arg_iqn = 0.; 
                for(std::size_t i = 0; i < lc.nb_local_cell_dofs; i++) {
                    const typename LC::CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
                    arg_iqn += arg_TF(i) * phi_i_iqn;
                } // for i
                
                const Eigen::Matrix<Real, 2, 1> Grad_u_iqn = stat.dg( arg_iqn ) *  G_arg_iqn ; 
                
                const Eigen::Matrix<Real, 2, 1> & G_ex_iqn = G_ex(xQN);   
                const Eigen::Matrix<Real, 2, 1> diff_grad = Grad_u_iqn - G_ex_iqn; 
                
                norm_err_T += wQN *  diff_grad.squaredNorm();
                norm_ex_T +=wQN * G_ex_iqn.squaredNorm();  
            }//for iQN 
        } // For iF_loc
        norm_ex_temp += norm_ex_T; 
        norm_err += norm_err_T; 
    } // for iT 

    norm_grad_ex = std::sqrt(norm_ex_temp); 

    return std::sqrt(norm_err);  

} // Error_grad_L2

template<std::size_t K>
Real DiscUnkw<K>::get_relative_entropy( const GC & gc, 
                                        const DiscUnkw & Phi, 
                                        const Statistics & stat, 
                                        const FctContType & u_ref ) const
{
    Real entropy = 0.;
    for(int iT = 0; iT < m_nb_cells; iT++){
        Real entropy_T =0.;
        const LC & lc = gc.getLC(iT); 
        const SolutionVectorType & coeff_T = m_cells[iT] - m_r * Phi.get_cell_coefs(iT); 
        std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > feval_basisT_PTF = lc.feval_basisT_PTF; 
        for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
            const PyramidIntegrator & pim = *lc.pims[iF_loc];            
            for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
                const Point & xQN = pim.point(iQN);
                const Real & wQN = pim.weight(iQN);
                Real arg_iqn = 0 ;
                for(std::size_t i = 0; i < lc.nb_cell_dofs; i++) {
                    const typename LC::CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
                    arg_iqn += coeff_T(i) * phi_i_iqn;
                } // for i
                const Real u_iqn = stat.g(arg_iqn);
                const Real u_ref_iqn = u_ref(xQN); 
                const Real entro_iqn = stat.H(u_iqn) - stat.H(u_ref_iqn) - stat.h(u_ref_iqn) * (u_iqn - u_ref_iqn); 
                entropy_T += wQN * std::fabs( entro_iqn );            
            }//for iQN 
        } // For iF_loc
        entropy += entropy_T; 
    } //for iT  

    return entropy; 

} // get_relative_entropy





/////
/////
/////
///// 
///// implementation of DiscUnkw : DONE 


// Implementation of binary operations

template<std::size_t K>
DiscUnkw<K> operator+(const  DiscUnkw<K> & u, const DiscUnkw<K> & v)
{
    DiscUnkw w(u);
    w+=v;
    return w ;
}

template<std::size_t K>
DiscUnkw<K> operator-(const  DiscUnkw<K> & u, const DiscUnkw<K> & v)
{
    DiscUnkw w(u);
    w-=v;
    return w ;
}

template<std::size_t K>
DiscUnkw<K> operator*(const  Real & alpha , const DiscUnkw<K> & u)
{
    DiscUnkw w(u);
    w*=alpha;
    return w ;
}


} //namespace hO

#endif