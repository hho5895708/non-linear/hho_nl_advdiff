#include "DiscreteData.h"

/*
namespace ho{
namespace diffusion{

//------------------------------------------------------------------------------
// Implementation GlobalContrib 

template<std::size_t K>
GlobalContrib<K>::GlobalContrib(const Mesh * Th,
                                const LoadType & load,
                                const BoundaryNeumannType & g_n,
                                const DiffusivityType & Lambda,
                                const DefBoundaryConditionType & isDir, 
                                const DefBoundaryConditionType & isNeu,
                                const int & add_integrator ,
                                const Real & eta )
{
    
    std::cout << "\nPrecomputation : generation of the Global Contributions for diffusion problem begins" << std::endl;  
    common::chrono c_comp;
    c_comp.start();
    
    nb_cells = Th->numberOfCells();
    nb_faces = Th->numberOfFaces();
    nb_faces_dofs = nb_faces * LC::nb_local_face_dofs;
    
    nb_Dirichlet_faces = 0;
    for(Integer iF = 0; iF < nb_faces; iF++) {
        const Face & F = Th->face(iF);
        if(isDir(F)){ 
            nb_Dirichlet_faces += 1; 
        } // if F is Dirichlet
    } // for iF 


    nb_Dirichlet_dofs = nb_Dirichlet_faces * LC::nb_local_face_dofs; 
  
    std::cout << "Number of faces with Dirichlet boundary condition " << nb_Dirichlet_faces << std::endl;
    std::cout << "Number of unknowns associated to Dirichlet boundary condition " << nb_Dirichlet_dofs << std::endl;    
    
 
    // creation of the resize matrix 
    resize_mat.resize(nb_faces_dofs - nb_Dirichlet_dofs, nb_faces_dofs);  
    std::vector<TripletType> triplets_resize;
    triplets_resize.reserve(nb_faces_dofs);  
    
    int counter_Fb_dir = 0;
    for(Integer iF = 0; iF < Th->numberOfFaces(); iF++){
        const Face F = Th->face(iF);
        if(!(isDir(F))){
            for(int i = 0; i < LC::nb_local_face_dofs; i++){
            triplets_resize.push_back(TripletType((iF - counter_Fb_dir) * LC::nb_local_face_dofs + i, iF * LC::nb_local_face_dofs + i, 1.));
            }// for i
        }//if Face is not Dirichlet 
        else{counter_Fb_dir++;}        
    } // for iF 
    
    resize_mat.setFromTriplets(triplets_resize.begin(), triplets_resize.end());

    
    Local_Contribs.resize(nb_cells);
    for(int iT = 0; iT < nb_cells; iT++) {    
        const LC & lc(Th, iT, load , g_n , Lambda , isNeu , eta );
        Local_Contribs[iT] = lc; 
    }  // for iT
    
    c_comp.stop();
    std::cout << "Precomputations (Global contribs) : DONE !"  << std::endl;  
    std::cout << FORMAT(50) << "Time of the precomputational step for Global contribution : \t"  << c_comp.diff() << std::endl;

    time_computation = c_comp.diff();

} //GlobalContrib, constructor 

template<std::size_t K>
ho::diffusion::LocalContrib<K> GlobalContrib<K>::getLC(const int & iT) const 
{
    return Local_Contribs[iT]; 
}

template<std::size_t K>
SolutionVectorType GlobalContrib<K>::getInterpolationDirBoundary(   const Mesh * Th,
                                                                    const DefBoundaryConditionType & isDir,
                                                                    const BoundaryDirichletType & u_D) const
{
    SolutionVectorType X_D =  SolutionVectorType::Zero(nb_faces_dofs);    
    for(int iT = 0; iT < nb_cells; iT ++){
        const Cell & T = Th->cell(iT);
        const LC & lc = getLC(iT); 
        Eigen::VectorXd g_Dirichlet_iF = Eigen::VectorXd::Zero(lc.nb_tot_faces_dofs);
        for(int iF_loc = 0; iF_loc < (Th->cell(iT)).numberOfFaces(); iF_loc++){
            const Face & F = Th->face(T.faceId(iF_loc));
            if(isDir(F)) {
                g_Dirichlet_iF.segment(iF_loc * LC::nb_local_face_dofs, LC::nb_local_face_dofs) += lc.interpolate(Th, iT, u_D ).segment(LC::nb_cell_dofs + iF_loc * LC::nb_local_face_dofs, LC::nb_local_face_dofs);
            } // if Face is dirichlet
        } // for iF_loc
        
        for(int i = 0; i < lc.idx_T.size(); i++) {
            X_D(lc.idx_T(i)) += g_Dirichlet_iF(i);
        } // for i
    } //for iT

    return X_D;
}

template<std::size_t K>
SolutionVectorType GlobalContrib<K>::getInterpolationFaces( const Mesh * Th,
                                                            const ExactSolutionType & u) const
{
    SolutionVectorType X =  SolutionVectorType::Zero(nb_faces_dofs);  
    std::vector<bool> face_non_asigned( nb_faces , true );  
    for(int iT = 0; iT < nb_cells; iT ++){
        const Cell & T = Th->cell(iT);
        const LC & lc = getLC(iT); 
        Eigen::VectorXd g_iF = Eigen::VectorXd::Zero(lc.nb_tot_faces_dofs);
        Eigen::VectorXd u_T_I = lc.interpolate(Th, iT, u );
        for(int iF_loc = 0; iF_loc < nb_cells ; iF_loc++){
            const int iF = T.faceId(iF_loc);
            if(face_non_asigned[iF]){
                g_iF.segment(iF_loc * LC::nb_local_face_dofs, LC::nb_local_face_dofs) = u_T_I.segment(LC::nb_cell_dofs + iF_loc * LC::nb_local_face_dofs, LC::nb_local_face_dofs);
                face_non_asigned[iF] = false; 
            } // if F is non asigned
        } // for iF_loc
        
        for(int i = 0; i < lc.idx_T.size(); i++) {
            X(lc.idx_T(i)) += g_iF(i);
        } // for i
    } //for iT

    return X;
}



}// namespace diffusion
} //namespace ho

namespace ho{


//------------------------------------------------------------------------------
// Implementation GlobalSys

template<std::size_t K>
GlobalSys<K>::GlobalSys(const Mesh * Th, const  GC & gc)
{       
    const int nuntrp = (Th->maximumNumberOfFaces() * gc.nb_cells + gc.nb_faces) * LC::nb_local_face_dofs;
    m_Triplets_global_system.reserve(nuntrp); //triplets of the global system   
    m_RHS = SolutionVectorType::Zero(gc.nb_faces_dofs);
    m_invJ1_J2.resize(gc.nb_cells); 
    m_invJ1_B1.resize(gc.nb_cells); 

    m_nb_unkw = gc.nb_faces_dofs; 

} //GlobalSys, constructor


template<std::size_t K>
void GlobalSys<K>::clear_gs()
{   
    const int & nuntrp = m_Triplets_global_system.size();
    m_Triplets_global_system.clear();
    m_Triplets_global_system.reserve(nuntrp);  
    m_RHS = SolutionVectorType::Zero(m_nb_unkw); 
    const int & nb_cells = m_invJ1_B1.size();     
    m_invJ1_J2.clear();
    m_invJ1_J2.resize(nb_cells); 
    m_invJ1_B1.clear();
    m_invJ1_B1.resize(nb_cells); 

} // clear_gs


template<std::size_t K>
void GlobalSys<K>::set_local_problem(   const int & iT, 
                                        const GC & gc, 
                                        const DenseMatrixType & J1,
                                        const DenseMatrixType & J2,
                                        const DenseMatrixType & J3,
                                        const DenseMatrixType & J4,
                                        const SolutionVectorType & B)
{   
    const LC & lc = gc.getLC(iT); 
    //initialize and generate local blocks   
    const int & n_vol = lc.nb_cell_dofs; 
    const int & n_edge = lc.nb_tot_faces_dofs; 


    //[ J_4 - J_3 * inv_J1 * J_2 ] 
    //compute inv_J1
    Eigen::LDLT<Eigen::MatrixXd> LU_J1; 
    LU_J1.compute(J1);
    const Eigen::MatrixXd inv_J1_J2 = LU_J1.solve(J2);     
    
    // save the contribution for the recosntruction  
    m_invJ1_J2[iT] = inv_J1_J2 ; //vector of matrices  inv_J_1 *J_2 for the reconstruction 
    m_invJ1_B1[iT] = LU_J1.solve(B.head(n_vol)); 
    
    // compute the local schur complement and the local RHS (after condensation)
    const Eigen::MatrixXd Schur_T = J4 - J3 * inv_J1_J2;    
    SolutionVectorType rhs_T = B.tail(n_edge) - J3 * m_invJ1_B1[iT]; 
    
    // add triplets in the global matrix and global RHS 
    const IndexVectorType & idx_T = lc.idx_T; 
    for(int i = 0; i < idx_T.size(); i++) {
      m_RHS(idx_T(i)) += rhs_T(i);
      for(int j = 0; j < idx_T.size(); j++) {
         m_Triplets_global_system.push_back(TripletType(idx_T(i), idx_T(j), Schur_T(i, j) ) );
      } // for j
    } // for i

} // set_local_problem

template<std::size_t K>
SparseMatrixType GlobalSys<K>::get_global_matrix() const
{
    SparseMatrixType Mat_sys(m_nb_unkw,m_nb_unkw);
    Mat_sys.setFromTriplets(m_Triplets_global_system.begin(),  m_Triplets_global_system.end());
    return Mat_sys; 

} // get_global_matrix


template<std::size_t K>
void GlobalSys<K>::compute_LU_Decompo_global( const GC & gc )
{   
    //compute the global matrix (resized)
    SparseMatrixType Mat_global_resize =  gc.resize_mat *   get_global_matrix() * (gc.resize_mat.transpose()); 
    // Compute the ordering permutation vector from the structural pattern of A
    Solver_global.analyzePattern(Mat_global_resize);
    // Compute the numerical factorization
    Solver_global.factorize(Mat_global_resize);

} //compute_LU_Decompo_global


template<std::size_t K>
SolutionVectorType  GlobalSys<K>::get_global_RHS() const
{
    return m_RHS;

} // get_global_RHS


template<std::size_t K>
SolutionVectorType  GlobalSys<K>::get_vec_reconstruct(const int & iT) const
{
    return m_invJ1_B1[iT];

} // get_vec_reconstruct

template<std::size_t K>
Eigen::MatrixXd GlobalSys<K>::get_matrix_reconstruct(const int & iT) const
{
    return m_invJ1_J2[iT]; 

} // get_matrix_reconstruct

template<std::size_t K>
void GlobalSys<K>::set_varying_RHS( const int & iT, 
                                    const  GC & gc, 
                                    const SolutionVectorType & B)
{


} //set_varying_RHS

//------------------------------------------------------------------------------
// Implementation DiscrUnkw

template<std::size_t K>
DiscUnkw<K>::DiscUnkw(const Mesh * Th, const  GC & gc,  const Real & val )
{   
    m_nb_cells = gc.nb_cells; 
    m_nb_faces_dofs = gc.nb_faces_dofs;
    m_nb_local_cell_dofs = LC::nb_local_cell_dofs;
    m_X = SolutionVectorType::Zero(m_nb_faces_dofs);
    m_cells.resize( m_nb_cells );
    for(int iT = 0; iT < m_nb_cells; iT ++){
        m_cells[iT] = SolutionVectorType::Zero(m_nb_local_cell_dofs);
    }

    if(val != 0.){
        for(int iT = 0; iT < m_nb_cells; iT ++){
            m_cells[iT](0) = val;
        } // for iT        
        for(int iF = 0; iF < gc.nb_faces ; iF ++){
            const int offset_F = iF * LC::nb_local_face_dofs;
            m_X(offset_F) = val;
        } // for iT
    }// if val is not 0

} // construction of a constant (= val) unknown

template<std::size_t K>
DiscUnkw<K>::DiscUnkw(const DiscUnkw & u):    
    m_nb_faces_dofs(u.m_nb_faces_dofs),
    m_nb_local_cell_dofs(u.m_nb_local_cell_dofs),
    m_nb_cells(u.m_nb_cells),
    m_X(u.m_X), 
    m_cells(u.m_cells) 
    {} // copy constructor 
    //copy constructor

template<std::size_t K>
SolutionVectorType DiscUnkw<K>::get_local_faces_dofs_vec(const GC & gc , const int &  iT ) const
{
    const LC & lc = gc.getLC(iT); 
    SolutionVectorType loc_vec_faces = SolutionVectorType::Zero(lc.nb_tot_faces_dofs);
    const IndexVectorType & idx_T = lc.idx_T;  
    for(int i = 0; i < idx_T.size(); i++) { 
        loc_vec_faces(i) = m_X(idx_T(i)); 
    } // for i 
    return loc_vec_faces;
} // get_local_faces_dofs_vec 


template<std::size_t K>
SolutionVectorType DiscUnkw<K>::get_local_dofs(const GC & gc , const int &  iT ) const
{
    const LC & lc = gc.getLC(iT); 
    SolutionVectorType loc_vec = SolutionVectorType::Zero(lc.nb_tot_dofs);
    loc_vec.head(lc.nb_local_cell_dofs) = m_cells[iT];
    loc_vec.tail(lc.nb_tot_faces_dofs) = get_local_faces_dofs_vec( gc , iT );
    return loc_vec;
} // get_local_dofs


template<std::size_t K>
SolutionVectorType DiscUnkw<K>::get_cell_coefs(const int & iT) const
{
    return m_cells[iT];

} // cell_coefs 
    
    
template<std::size_t K>
std::vector<SolutionVectorType> DiscUnkw<K>::get_list_cells_coefs() const
{
    return m_cells; 

} // list_cells_coefs


template<std::size_t K>
SolutionVectorType DiscUnkw<K>::get_X_vector() const
{
    return m_X;
    
} // get_X_vector 

template<std::size_t K>
void DiscUnkw<K>::Proj_on_Ih_eps(const Statistics & stat, const Real & eps)
{
    // riche impelementation ici    
} // get_X_vector 


template<std::size_t K>
void DiscUnkw<K>::set_from_GlobalSys_zero_boundary( const GC & gc,
                                                    const GS & gs )
{
    m_X = (gc.resize_mat.transpose()) * gs.Solver_global.solve( gc.resize_mat * ( gs.get_global_RHS() ) ); // !!!
    // !!
    // !!
    // !!
    // here, the question of the boundary conditions !!!!
    // works only if the considered unknown is zero on the dirichlet boundary 
    for(int iT = 0; iT < m_nb_cells; iT ++){
        const SolutionVectorType & faces_T = get_local_faces_dofs_vec( gc, iT);  
        m_cells[iT] = gs.get_vec_reconstruct(iT) - gs.get_matrix_reconstruct(iT) * faces_T; 
    } // for iT 
}  


template<std::size_t K>
void DiscUnkw<K>::set_from_fonction( const Mesh * Th, 
                            const  GC & gc, 
                            const ExactSolutionType & u )
{
    m_X =  SolutionVectorType::Zero(m_nb_faces_dofs);  
    std::vector<bool> face_non_asigned(  gc.nb_faces, true );  
    for(int iT = 0; iT < m_nb_cells; iT ++){
        const Cell & T = Th->cell(iT);
        const LC & lc = gc.getLC(iT); 
        Eigen::VectorXd g_iF = Eigen::VectorXd::Zero(lc.nb_tot_faces_dofs);
        Eigen::VectorXd u_T_I = lc.interpolate(Th, iT, u );
        m_cells[iT] = u_T_I.head(m_nb_local_cell_dofs);
        for(int iF_loc = 0; iF_loc < m_nb_cells ; iF_loc++){
            const int iF = T.faceId(iF_loc);
            if(face_non_asigned[iF]){
                g_iF.segment(iF_loc * LC::nb_local_face_dofs, LC::nb_local_face_dofs) = u_T_I.segment(LC::nb_cell_dofs + iF_loc * LC::nb_local_face_dofs, LC::nb_local_face_dofs);
                face_non_asigned[iF] = false; 
            } // if F is non asigned
        } // for iF_loc
        
        for(int i = 0; i < lc.idx_T.size(); i++) {
            m_X(lc.idx_T(i)) += g_iF(i);
        } // for i
    } //for iT
}


template<std::size_t K>
void DiscUnkw<K>::set_lift_from_boundary_values(    const Mesh * Th,
                                                    const  GC & gc,
                                                    const DefBoundaryConditionType & isDir,
                                                    const BoundaryDirichletType & u_D )
{
    for(int iT = 0; iT < m_nb_cells; iT ++){
        m_cells[iT] =  SolutionVectorType::Zero(m_nb_local_cell_dofs); 
    } // for iT 

    m_X = gc.getInterpolationDirBoundary(Th, isDir, u_D);

} // construction of a discrete lifting from a continuous fonction



template<std::size_t K>
void DiscUnkw<K>::set_with_Random_values()
{
    m_X = SolutionVectorType::Random(m_nb_faces_dofs);
    for(int iT = 0; iT < m_nb_cells; iT ++){
        m_cells[iT] = SolutionVectorType::Random(m_nb_local_cell_dofs);
    } //for iT 
} //set_with_Random_values


template<std::size_t K>
void DiscUnkw<K>::set_from_edges_values_and_GS( const GC & gc,
                                                const GS & gs,
                                                const SolutionVectorType & X )
{
    m_X = X; 
    for(int iT = 0; iT < m_nb_cells; iT ++){
        const SolutionVectorType & faces_T = get_local_faces_dofs_vec( gc, iT);  
        m_cells[iT] =- gs.get_matrix_reconstruct(iT) * faces_T; 
    } // for iT 
}  

// 
// implementation of operations on the discrete unknowns (~ surcharge += ...)
//

template<std::size_t K>
DiscUnkw<K> & DiscUnkw<K>::operator+=(const DiscUnkw & v)
{   
    m_X += v.m_X;
    
    for(int iT =0; iT < m_nb_cells; iT ++){
        m_cells[iT] += v.m_cells[iT];
    } // for iT

    return *this;
}

template<std::size_t K>
DiscUnkw<K> & DiscUnkw<K>::operator-=(const DiscUnkw & v)
{   
    m_X -= v.m_X;
    
    for(int iT =0; iT < m_nb_cells; iT ++){
        m_cells[iT] -= v.m_cells[iT];
    } // for iT

    return *this;
}

template<std::size_t K>
DiscUnkw<K> & DiscUnkw<K>::operator*=(const Real &  alpha )
{
    m_X = alpha * m_X; 
    
    for(int iT =0; iT < m_nb_cells; iT ++){
        m_cells[iT] = alpha * m_cells[iT];
    } // for iT

    return *this;  
}


template<std::size_t K>
void DiscUnkw<K>::print_local_values_debug( const  GC & gc,
                                            const int & iT, 
                                            const bool & faces, 
                                            const bool & cell ) const
{
    std::cout <<"Values of the discrete vector [debug]\nLocal information: cell  n°" << iT << std::endl;
    if(faces){
        const SolutionVectorType & faces_dofs_vec =  get_local_faces_dofs_vec( gc, iT ); 
        std::cout <<"Faces coeffs:\n" << std::endl;
        std::cout << faces_dofs_vec <<std::endl;    
    } //if faces

    if(cell){
        std::cout << "Cell coeffs:\n" << std::endl; 
        std::cout << get_cell_coefs(iT); 
    } //if cells

    std::cout <<"That's all [end debug] \n"  <<  std::endl;
} // print_local_values_debug


template<std::size_t K>
void DiscUnkw<K>::print_values_debug(   const  GC & gc,
                                        const bool & faces, 
                                        const bool & cell ) const
{
    std::cout <<"Values of the discrete vector [debug]\nGlobal information" << std::endl;
    if(faces){
        std::cout <<"Faces coeffs (global vector X):\n" << std::endl;
        std::cout << m_X <<std::endl;    
    } //if faces

    if(cell){
        std::cout << "Cell coeffs:\n" << std::endl;
        for(int iT = 0; iT < m_nb_cells; iT ++){
            std::cout <<"Cells n°"<< iT <<" coeffs: \t " << m_cells[iT] <<  std::endl;
        } // for iT 
    } //if cells

    std::cout <<"That's all [end debug] \n"  <<  std::endl;
} // print_local_values_debug




template<std::size_t K>
void DiscUnkw<K>::file_visu_poly(const std::string & name , const Mesh * Th )const
{
    postProcessing_poly<ho::HierarchicalScalarBasis2d<K+1>, boost::static_unsigned_min<K+1,3>::value>(name, Th, m_cells);
} // file_visu_poly

template<std::size_t K>
void DiscUnkw<K>::file_visu_g_unkw( const std::string & name, 
                                    const Mesh * Th, 
                                    const Statistics & stat) const
{
    postProcessing_g_h_u<ho::HierarchicalScalarBasis2d<K+1>, boost::static_unsigned_min<K+1,3>::value>(name, Th, stat.g_fct(), m_cells);
} //file_visu_g_unkw


template<std::size_t K>
FctContType DiscUnkw<K>::get_local_density_fct( const GC & gc, 
                                                const int iT,
                                                const Statistics & stat) const
{
    const LC & lc = gc.getLC(iT); 
    const SolutionVectorType & coeff_T = m_cells[iT];
    const CellBasis &  basisT =  *lc.basisT;  
    const int & size = basisT.size;
    
    FctContType density_T =[stat,basisT,coeff_T](const Point & x) -> Real {
        Real arg = 0.; 
        for(int i = 0; i < size; i++){
            arg += coeff_T[i] * basisT.phi(i).phi(x);
        } //for i
        return stat.g(arg); 
    }; // density_T

    return density_T; 
} //get_local_density_fct

template<std::size_t K>
SolutionVectorType DiscUnkw<K>::get_local_density_against_test_vec( const GC & gc, 
                                                                    const int iT,
                                                                    const Statistics & stat) const
{
    const LC & lc = gc.getLC(iT); 
    const SolutionVectorType & coeff_T = m_cells[iT];
    SolutionVectorType Vec_density_vs_test = SolutionVectorType::Zero(lc.nb_cell_dofs);
    
    std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > feval_basisT_PTF = lc.feval_basisT_PTF; 
    for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
        const PyramidIntegrator & pim = *lc.pims[iF_loc];            
        for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
            const Point & xQN = pim.point(iQN);
            const Real & wQN = pim.weight(iQN);
            Real arg_iqn = 0 ;         
            for(std::size_t i = 0; i < lc.nb_cell_dofs; i++) {
                const typename LC::CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
                arg_iqn += coeff_T[i] * phi_i_iqn;
            } // for i
            for(std::size_t j = 0; j < lc.nb_cell_dofs; j ++){
                const typename LC::CellBasis::ValueType & phi_j_iqn = (*feval_basisT_PTF[iF_loc])(j, iQN);
                Vec_density_vs_test(j) += wQN *stat.g(arg_iqn) * phi_j_iqn;
            } //for j 
        }//for iQN 
    } // For iF_loc
    return Vec_density_vs_test;

}//get_local_density_against_test_vec

template<std::size_t K>
Real DiscUnkw<K>::L2_square_norm_poly(const GC & gc) const
{
    Real norm_square = 0.;
    for(int iT = 0; iT < m_nb_cells ; iT ++){
        const LC & lc = gc.getLC(iT); 
        norm_square +=  m_cells[iT].transpose()  * lc.MTT * m_cells[iT].transpose(); 
    } // for iT 
    return norm_square; 
} // L2_square_norm_poly


template<std::size_t K>
Real DiscUnkw<K>::L2_square_norm_density( const  GC & gc, const Statistics & stat) const
{   
    Real norm_square = 0.;
    for(int iT = 0; iT < m_nb_cells ; iT ++){
        const LC & lc = gc.getLC(iT); 
        std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > feval_basisT_PTF = lc.feval_basisT_PTF; 
        for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
            const PyramidIntegrator & pim = *lc.pims[iF_loc];            
            for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
                const Point & xQN = pim.point(iQN);
                const Real & wQN = pim.weight(iQN);
                Real arg_iqn = 0 ;
                for(std::size_t i = 0; i < lc.nb_cell_dofs; i++) {
                    const typename LC::CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
                    arg_iqn += m_cells[iT][i] * phi_i_iqn;
                } // for i
                norm_square += wQN * std::pow(stat.g(arg_iqn),2);
            }//for iQN 
        } // For iF_loc
    } // for iT 
    return norm_square;
} // L2_square_norm_density


template<std::size_t K>
Real DiscUnkw<K>::L2_square_norm_grad_poly(const GC & gc) const
{
    Real norm_square_grad = 0.;
    for(int iT = 0; iT < m_nb_cells ; iT ++){
        const LC & lc = gc.getLC(iT);    
        std::size_t NG_full = lc.NG_full;    
        // Coefficients
        Eigen::VectorXd G_coeffs = lc.G_full_T * get_local_dofs( gc ,  iT);             
        std::vector<std::shared_ptr<BasisFunctionEvaluation<CellVecBasis> > > feval_vec_basisT_PTF = lc.feval_vec_basisT_PTF;
        for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
            const PyramidIntegrator & pim = *lc.pims[iF_loc];            
            for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
                const Point & xQN = pim.point(iQN);
                const Real & wQN = pim.weight(iQN);
                Eigen::Matrix<Real, 2, 1> G_iqn = Eigen::Matrix<Real, 2, 1>::Zero();
                for(std::size_t i = 0; i < NG_full; i++) {
                    const typename LC::CellVecBasis::ValueType & Tau_i_iqn = (*feval_vec_basisT_PTF[iF_loc])(i, iQN);
                    G_iqn += G_coeffs(i) * Tau_i_iqn;
                } // for i
                norm_square_grad += wQN * G_iqn.squaredNorm();
            }//for iQN 
        } // For iF_loc
    } // for iT 

    return norm_square_grad; 
}  // L2_square_norm_grad_poly 


template<std::size_t K>
Real DiscUnkw<K>::L_infty_norm_coefs_faces() const
{
    return m_X.lpNorm<Eigen::Infinity>(); 
} //L_infty_norm_coefs_faces


template<std::size_t K>
Real DiscUnkw<K>::L_infty_norm_coefs_cells() const
{
    Real max_cells =  m_cells[0].lpNorm<Eigen::Infinity>();
    for(int iT = 1; iT < m_nb_cells; iT ++){
        max_cells = std::max(max_cells,  m_cells[iT].lpNorm<Eigen::Infinity>());
    } // for iT 
    return max_cells; 
} //L_infty_norm_coefs_cells


template<std::size_t K>
Real DiscUnkw<K>::L_infty_norm_coefs() const
{
    return std::max(L_infty_norm_coefs_cells(), L_infty_norm_coefs_faces() );  
} //L_infty_norm_coefs

/////
/////
/////
///// 
///// implementation of DiscUnkw : DONE 


// Implementation of binary operations

template<std::size_t K>
DiscUnkw<K> operator+(const  DiscUnkw<K> & u, const DiscUnkw<K> & v)
{
    DiscUnkw w(u);
    w+=v;
    return w ;
}

template<std::size_t K>
DiscUnkw<K> operator-(const  DiscUnkw<K> & u, const DiscUnkw<K> & v)
{
    DiscUnkw w(u);
    w-=v;
    return w ;
}

template<std::size_t K>
DiscUnkw<K> operator*(const  Real & alpha , const DiscUnkw<K> & u)
{
    DiscUnkw w(u);
    w*=alpha;
    return w ;
}


}//namespace ho

*/