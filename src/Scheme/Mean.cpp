#include "Mean.h"


#ifndef s_regul
#define s_regul 0.5*std::pow(10., -4)
#endif


//Mean_Fct (mean), implementation 

Mean_Fct::Mean_Fct() //default constructor, arithemtic mean
{
    MeanFctType m;
    MeanFctType dm;
    m =[this](const Real & x, const Real & y) -> Real {
        return 0.5 * (x+y);
    };
    dm =[this](const Real & x, const Real & y) -> Real {
        return 0.5;
    };
    m_mean = m;
    m_d1_mean = dm;
    m_d2_mean = dm;
}


Mean_Fct::Mean_Fct(const std::string & type_mean)
{
    if(type_mean =="ari"){
        MeanFctType m;
        MeanFctType dm;
        m =[this](const Real & x, const Real & y) -> Real {
            return 0.5 * (x+y);
        };
        dm =[this](const Real & x, const Real & y) -> Real {
            return 0.5;
        };
        m_mean = m;
        m_d1_mean = dm;
        m_d2_mean = dm;
    }
    else if(type_mean == "geo"){
        MeanFctType m;
        MeanFctType d1m;
        MeanFctType d2m;
        m =[this](const Real & x, const Real & y) -> Real {
            return sqrt(x*y);
        };
        d1m =[this](const Real & x, const Real & y) -> Real {
            return 0.5*sqrt(y/x);
        };
         d2m =[this](const Real & x, const Real & y) -> Real {
            return 0.5 *sqrt(x/y);
        };
        m_mean = m;
        m_d1_mean = d1m;
        m_d2_mean = d2m;
    }
    else if(type_mean == "log"){
        MeanFctType m;
        MeanFctType d1m;
        MeanFctType d2m;
        m =[this](const Real & x, const Real & y) -> Real {
            const Real h = y-x;
            if(fabs(h)>s_regul){
                return -h/log(x/y);
            }
            else{
                Real h_2 = h*h;
                Real x_inv = 1./x;
                return x + 0.5 * h - (x_inv*h_2)/(12.) + (x_inv*x_inv*h*h_2)/(24.);
            }
        };
        d1m =[this](const Real & x, const Real & y) -> Real {
            const Real h = y-x;           
            if(fabs(h)>s_regul){
                const Real ratio = x/y; 
                const Real l_ = log(ratio);
                return (l_ - 1. + (1./ratio) )/(l_ * l_);
            }
            else{
                const Real r = h/x;
                Real d = 0.5 + r /6.;
                const Real r_2 = r*r;
                d+= r_2/24.;
                d+= (r*r_2)/45.;
                return d;
            }
        };
        d2m =[d1m](const Real & x, const Real & y) -> Real {
            return d1m(y,x);
        };
        m_mean = m;
        m_d1_mean = d1m;
        m_d2_mean = d2m;
    }
    else if(type_mean == "harmo")
    {
        MeanFctType m;
        MeanFctType d1m;
        MeanFctType d2m;
        m =[this](const Real & x, const Real & y) -> Real {
            return (2. * x* y) / (x+y);
        };
        d1m =[this](const Real & x, const Real & y) -> Real {
            const Real quo = 1 - (x /(x+y));
            return 2. * quo*quo;
        };
        d2m =[d1m](const Real & x, const Real & y) -> Real {
            return d1m(y,x);
        };
        m_mean = m;
        m_d1_mean = d1m;
        m_d2_mean = d2m;
    }

    else{
        std::cout <<"You asked for a unknown type of mean function \n"  <<  std::endl;
        std::cout <<"Arithmetic mean will be used instead \n"  <<  std::endl;
        MeanFctType m;
        MeanFctType dm;
        m =[this](const Real & x, const Real & y) -> Real {
            return 0.5 * (x+y);
        };
        dm =[this](const Real & x, const Real & y) -> Real {
            return 0.5;
        };
        m_mean = m;
        m_d1_mean = dm;
        m_d2_mean = dm;
    }   

}

Real Mean_Fct::m(const Real & x, const Real y) const
{
    return m_mean(x,y);
} 

Real Mean_Fct::d1_m(const Real & x, const Real y) const
{
    return m_d1_mean(x,y);
}

Real Mean_Fct::d2_m(const Real & x, const Real y) const
{
    return m_d2_mean(x,y);
}

