#ifndef GLOBALSYSTEM_H
#define GLOBALSYSTEM_H

#include "GlobalContributions.h"
#include <boost/integer/static_min_max.hpp>

typedef Eigen::SparseLU<SparseMatrixType> SolverType;

namespace ho
{ 
//
// Description 
//

// class representing any linear (global) system of the scheme
//
// The local systems under consideration are of the form 
//      | J_1   J_2 |   |   U   |   -   |   B_1 |
//      | J_3   J_4 |   |   X   |   -   |   B_2 |
//
// Therefore, performing a local static condensation, we get (with inv_J1 the inverse of J_1)
//      U = inv_J_1 * ( B_1 - J_2 * X ) 
// and 
//      [ J_4 - J_3 * inv_J_1 * J_2 ] * X = B_2 - J_3 * inv_J_1 * B_1
//
// This class represent this structure at the global level :
// given, for any cell T,  matrices J_1 , J_2, J_3 and J_4,  and the rhs B = (B_1, B2)
// we compute the global matrix of the system, its LU decomposition (through a solver), the global RHS  
// as well as the vector inv_J_1 *  B_1   and the matrix  inv_J_1 *J_2 
// in order to reconstruct (locally) the vector U 
//
//    

template<std::size_t K>
class GlobalSys
{
    typedef ho::diffusion::LocalContrib<K> LC;
    typedef ho::diffusion::GlobalContrib<K> GC;      
 
    
    public:
    GlobalSys(); 
    GlobalSys(const Mesh * Th, const  GC & gc); 

    //Clear the Global system (to use the gs it for the next Newton iteration)
    void clear_gs();
    //
    //set the local problem on cell n°iT (Block Jacobian and )
    void set_local_problem( const int & iT, 
                            const  GC & gc, 
                            const DenseMatrixType & J1,
                            const DenseMatrixType & J2,
                            const DenseMatrixType & J3,
                            const DenseMatrixType & J4,
                            const SolutionVectorType & B);
    //
    // returns the global matrix (after condensation) for the test of the jacobian !
    SparseMatrixType get_global_matrix() const; 
    //
    //computes the LU decomposition of the resized global matrix and set the Solver_global passed by reference  (!! for zero boundary condition problems)
    void get_LU_Solver_global( const GC & gc, SolverType & Solver_global) const;
    //
    //returns the RHS of the global system (after condensation)
    SolutionVectorType get_global_RHS() const; 
    //
    //returns the L^\infinity norm of the RHS (coefficient-wise, without Dirichlet coefs)
    Real get_norm_RHS(const GC & gc) const; 

    SolutionVectorType get_vec_reconstruct(const int & iT) const; 
    Eigen::MatrixXd get_matrix_reconstruct(const int & iT) const; 
    
    void set_varying_RHS(   const int & iT, 
                            const  GC & gc, 
                            const SolutionVectorType & B); // for linear (and evolutive) problems, 
    //we just set a new rhs (local), but the global matrix of the system is still the same 
    ////
    //// Non implementé pour le moment !!
    //// Pose certaines questions d'autre part 
    //// Faire une classe fille pour ce genre de problème (car il faut garder les matrices locales je pense)

    private:
    int m_nb_unkw; // size of the global matrix
    TripletsVectorType m_Triplets_global_system; // triplets of the global system 
    SolutionVectorType m_RHS; // the global RHS (after condensation)
    std::vector<Eigen::MatrixXd> m_invJ1_J2; //vector of matrices  inv_J_1 *J_2 for the reconstruction 
    std::vector<SolutionVectorType> m_invJ1_B1; // vector of the vector inv_J_1 * B1 for the reconstruction 
    

}; // class Global system 



//------------------------------------------------------------------------------
// Implementation GlobalSys


template<std::size_t K>
GlobalSys<K>::GlobalSys()
{       
    m_Triplets_global_system.resize(0); //triplets of the global system   
    m_RHS = SolutionVectorType::Zero(0);
    m_invJ1_J2.resize(0); 
    m_invJ1_B1.resize(0); 

    m_nb_unkw = 1; 

} //GlobalSys, default constructor

template<std::size_t K>
GlobalSys<K>::GlobalSys(const Mesh * Th, const  GC & gc)
{       
    const int nuntrp = (Th->maximumNumberOfFaces() * gc.nb_cells + gc.nb_faces) * LC::nb_local_face_dofs;
    m_Triplets_global_system.reserve(nuntrp); //triplets of the global system   
    m_RHS = SolutionVectorType::Zero(gc.nb_faces_dofs);
    m_invJ1_J2.resize(gc.nb_cells); 
    m_invJ1_B1.resize(gc.nb_cells); 

    m_nb_unkw = gc.nb_faces_dofs; 

} //GlobalSys, constructor


template<std::size_t K>
void GlobalSys<K>::clear_gs()
{   
    const int & nuntrp = m_Triplets_global_system.size();
    m_Triplets_global_system.clear();
    m_Triplets_global_system.reserve(nuntrp);  
    m_RHS = SolutionVectorType::Zero(m_nb_unkw); 
    const int & nb_cells = m_invJ1_B1.size();     
    m_invJ1_J2.clear();
    m_invJ1_J2.resize(nb_cells); 
    m_invJ1_B1.clear();
    m_invJ1_B1.resize(nb_cells); 

} // clear_gs


template<std::size_t K>
void GlobalSys<K>::set_local_problem(   const int & iT, 
                                        const GC & gc, 
                                        const DenseMatrixType & J1,
                                        const DenseMatrixType & J2,
                                        const DenseMatrixType & J3,
                                        const DenseMatrixType & J4,
                                        const SolutionVectorType & B)
{   
    const LC & lc = gc.getLC(iT); 
    //initialize and generate local blocks   
    const int & n_vol = lc.nb_cell_dofs; 
    const int & n_edge = lc.nb_tot_faces_dofs; 


    //[ J_4 - J_3 * inv_J1 * J_2 ] 
    //compute inv_J1
    Eigen::PartialPivLU<Eigen::MatrixXd> LU_J1; 
    LU_J1.compute(J1);
    const Eigen::MatrixXd inv_J1_J2 = LU_J1.solve(J2);     
    
    // save the contribution for the reconstruction  
    m_invJ1_J2[iT] = inv_J1_J2 ; //vector of matrices  inv_J_1 *J_2 for the reconstruction 
    m_invJ1_B1[iT] = LU_J1.solve(B.head(n_vol)); 
    
    // compute the local schur complement and the local RHS (after condensation)
    const Eigen::MatrixXd Schur_T = J4 - J3 * inv_J1_J2;    
    SolutionVectorType rhs_T = B.tail(n_edge) - J3 * m_invJ1_B1[iT]; 
    
    // add triplets in the global matrix and global RHS 
    const IndexVectorType & idx_T = lc.idx_T; 
    for(int i = 0; i < idx_T.size(); i++) {
      m_RHS(idx_T(i)) += rhs_T(i);
      for(int j = 0; j < idx_T.size(); j++) {
         m_Triplets_global_system.push_back(TripletType(idx_T(i), idx_T(j), Schur_T(i, j) ) );
      } // for j
    } // for i

} // set_local_problem

template<std::size_t K>
SparseMatrixType GlobalSys<K>::get_global_matrix() const
{
    SparseMatrixType Mat_sys(m_nb_unkw,m_nb_unkw);
    Mat_sys.setFromTriplets(m_Triplets_global_system.begin(),  m_Triplets_global_system.end());
    return Mat_sys; 

} // get_global_matrix


template<std::size_t K>
void GlobalSys<K>::get_LU_Solver_global( const GC & gc ,SolverType & Solver_global ) const
{   
    //compute the global matrix (resized)
    SparseMatrixType Mat_global_resize =  gc.resize_mat *   get_global_matrix() * (gc.resize_mat.transpose()); 
    // Compute the ordering permutation vector from the structural pattern of A
    Solver_global.analyzePattern(Mat_global_resize);
    // Compute the numerical factorization
    Solver_global.factorize(Mat_global_resize);

} //get_LU_Solver_global


template<std::size_t K>
SolutionVectorType  GlobalSys<K>::get_global_RHS() const
{    
    return m_RHS;

} // get_global_RHS


template<std::size_t K>
Real  GlobalSys<K>::get_norm_RHS(const GC & gc) const
{   
    //std::cout<< "HEY!! Norm Goal :\t " << m_RHS.lpNorm<Eigen::Infinity>()<< std::endl; 
    const SolutionVectorType rhs_dir = gc.resize_mat * m_RHS;
    return rhs_dir.lpNorm<Eigen::Infinity>();

} // get_norm_RHS


template<std::size_t K>
SolutionVectorType  GlobalSys<K>::get_vec_reconstruct(const int & iT) const
{
    return m_invJ1_B1[iT];

} // get_vec_reconstruct

template<std::size_t K>
Eigen::MatrixXd GlobalSys<K>::get_matrix_reconstruct(const int & iT) const
{
    return m_invJ1_J2[iT]; 

} // get_matrix_reconstruct

template<std::size_t K>
void GlobalSys<K>::set_varying_RHS( const int & iT, 
                                    const  GC & gc, 
                                    const SolutionVectorType & B)
{


} //set_varying_RHS


} // namespace ho

#endif