#ifndef NLcontributions_H
#define NLcontributions_H

#include "DiscreteData.h"


#ifndef max_size_arg
#define max_size_arg 100.
#endif

namespace ho
{
namespace NL
{

// Class representing the nonlinear contributions of the scheme
//
// At the local level, the scheme is defined as a the following :
// Find w_T \in V_T such that P_{n,dt}(w_T) = 0, where P_{n,dt} : V_T --> V_T depends on u^n, the density computed at the previous time
// This class, given dt, u^n and  w_T, compute P_n(w_T), 
// as well as the Jacobian (by bloc) of P_n at the point w_T
//
// The contributions are splitted as the following : 
//     # evol  ->  part of the time-derivative depending on w_T (without the time step divison)
//     # _n    ->  part of the time-derivative depending on u^n (without the time step divison)
//     # spa   ->  "stationary scheme" / spatial discretisation depending on w_T
//     # cst   ->  constant part, corresponding to the RHS, cosntant in time, only depends on the data (~ are already computed in the LC)  
//
// The vector field P_{n,dt} is therefore decomposed as 
//     P_{n,dt} = {P_evol - P_n } / dt + P_spa - P_cst
//     
// Since P_n does not depend on w_T, 
//  the associated contributions are precomputed and saved when the previous time density u_n is setted 
// Note that, since P_n and P_cst do not depend on w_T, the contributions associated to the Jacobian in 0  
// 
//  About the constribution P_spa ?? (the real one)
//
//  We write P_spa as a sum of other contributions: 
//      P_spa = P_cons + eta_nl * P_stab + eps_stab * P_stab_lin   
// 
//  Here, 
//      # cons -> consistent part (nonlinear)
//      # stab -> nonlinear stabilisation 
//      # stab_lin -> linear stabilisation (for the proof of existence) 
//  Each of these contributions have non null Jacobian  
//
//

template<std::size_t K>
class NLContrib
{   
    
    
    typedef ho::diffusion::LocalContrib<K> LC;
    typedef ho::diffusion::GlobalContrib<K> GC;  
    typedef ho::GlobalSys<K> GS; 
    typedef ho::DiscUnkw<K> DU;            
    
    typedef ho::HierarchicalVectorBasis2d<K> CellVecBasis; 
    typedef ho::HierarchicalScalarBasis2d<K+1> CellBasis; 
    typedef ho::HierarchicalScalarBasis1d<K> FaceBasis; 


    public :     
    //default constructor for NonLinear contributions
    NLContrib();   
    //
    //constructor for NonLinear contributions
    NLContrib(  const Mesh * Th, 
                const GC & gc, 
                const DiffusivityType & Lambda, 
                const Mean_Fct & mean, 
                const Real & eps_stab = 0. , 
                const Real & eta_nl = 1.  );    
    // clear the NLContrib to use it at a new time step (new u^n) 
    void clear(); 
    //
    //set the density at time n (u^n) and precompute the associated contributions P_n 
    void set_previous_time_density( const GC & gc, 
                                    const DU & w_n,
                                    const DU & Phi,  
                                    const Statistics & stat);    
    //
    //set the density at time n (u^n) and precompute the associated contributions P_n, using a fully continuous data
    void set_previous_time_density_from_cont_fct(const GC & gc, const FctContType & u_n); 
    //
    //set the time step dt
    void set_time_step(const Real & dt);    
    //
    //set the stabilisation parameter epsilon (linear term in w)
    void set_espilon_stab(const Real & eps);
    //
    //set the point w used to compute the values of P and its Jacobian 
    void set_current_point(const DU & w);     
    //
    //compute the local contribution for the NL scheme at the point w (the goal function P_T, and the blocks of J_K). Returns false if the arguments computed are too big 
    bool compute_local_NL_contributions(const Mesh * Th, 
                                        const int & iT, 
                                        const GC & gc ,
                                        const SolutionVectorType & w_loc_T, 
                                        const SolutionVectorType & Phi_loc_T, 
                                        const Statistics & stat, 
                                        SolutionVectorType & P,
                                        DenseMatrixType & J1,
                                        DenseMatrixType & J2,
                                        DenseMatrixType & J3,
                                        DenseMatrixType & J4,
                                        Real & norm_P_cells) const ;
    //
    //compute the contribution for the NL scheme and set the GlobalSystem associated. Returns false if some arguments computed are too big 
    bool compute_NL_contributions(  const Mesh * Th, 
                                    const GC & gc,
                                    const DU & Phi,  
                                    const Statistics & stat, 
                                    const bool & test_gaudeul = false);    
    //
    //get the global system 
    GS get_globalsystem() const;     
    //
    //get the norm of the goal function 
    Real get_norm_goal() const; 

    ////for debug (Gaudeul test)
    
    //
    //returns the goal function edges values
    SolutionVectorType get_goal_edges() const; 
    //
    //returns the goal function cell n°iT values
    SolutionVectorType get_goal_cell( const int & iT ) const; 



    private :
    bool m_computation_done; 

    Real m_goal_norm_infty;

    Real m_eps_sta; // stabilisation paremeter (linear part in w) for the existence
    Real m_eta_nl; // stabilisation parameter for the nonlinear stabilisation term 

    Mean_Fct m_mean; 
    DiffusivityType m_Lambda; 

    Real m_dt;
    DU m_w; //the current point at which the contributions are computated 
    GS m_global_system; //the global system associated with the 
    std::vector<SolutionVectorType> P_n ; // the contributions P_inde_w, which do not depend on m_w 

    SolutionVectorType m_P_edges; // the values of the goal function on edges (for Jacobian Test)
    std::vector<SolutionVectorType> m_P_cells; // the values of the goal function on cells (for Jacobian Test)


};  // class NLContrib



  
//------------------------------------------------------------------------------
// Implementation NLContrib 

template<std::size_t K>
NLContrib<K>::NLContrib()
{
    m_computation_done = false; 
    m_dt = 1.;   
    m_eps_sta = 0.;
    m_eta_nl = 1.; 
    //m_global_system = GlobalSys(Th, gc); 
    P_n.resize(0);
    m_w = DU();
    m_goal_norm_infty = 0.; 

    //for debug (Jacobian test)
    m_P_edges = SolutionVectorType::Zero(0);
    m_P_cells.resize(0);
} // constructor NLContrib


template<std::size_t K>
NLContrib<K>::NLContrib(const Mesh * Th, 
                        const GC & gc,                  
                        const DiffusivityType & Lambda, 
                        const Mean_Fct & mean,  
                        const Real & eps_stab , 
                        const Real & eta_nl )
{
    m_computation_done = false; 
    
    m_eps_sta = eps_stab;
    m_eta_nl = eta_nl; 
    m_Lambda = Lambda; 
    m_mean = mean; 

    m_dt = 1.;
    m_global_system = GlobalSys(Th, gc); 
    P_n.resize(gc.nb_cells);
    m_w = DU(Th, gc, 0.);
    m_goal_norm_infty = 0.; 

    //for debug (Jacobian test)
    m_P_edges = SolutionVectorType::Zero(gc.nb_faces_dofs);
    m_P_cells.resize(gc.nb_cells);
} // constructor NLContrib

template<std::size_t K>   
void NLContrib<K>::clear()
{
    m_computation_done = false; 
    m_dt = 1.;
    m_global_system.clear_gs();
    const int size = P_n.size();
    P_n.clear();
    P_n.resize(size);
    m_w = 0. * m_w;
    m_goal_norm_infty = 0.;

    //for debug (Jacobian test)
    m_P_edges *= 0.;
    m_P_cells.clear();     
    m_P_cells.resize(size);

} // clear


template<std::size_t K>
void NLContrib<K>::set_previous_time_density(   const GC & gc, 
                                                const DU & w_n,
                                                const DU & Phi, 
                                                const Statistics & stat)
{
    for(int iT = 0; iT < gc.nb_cells; iT ++){
        const LC & lc = gc.getLC(iT);
        P_n[iT] = SolutionVectorType::Zero(lc.nb_tot_dofs);
        P_n[iT].head(lc.nb_cell_dofs)=w_n.get_local_density_against_test_vec(gc, iT,Phi ,  stat );
    } // for iT 

} //set_previous_time_density     

template<std::size_t K>
void NLContrib<K>::set_previous_time_density_from_cont_fct(const GC & gc, const FctContType & u_n)
{
    for(int iT = 0; iT < gc.nb_cells; iT ++){
        const LC & lc = gc.getLC(iT);
        P_n[iT] = SolutionVectorType::Zero(lc.nb_tot_dofs);

        std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > feval_basisT_PTF = lc.feval_basisT_PTF; 
        for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
            const PyramidIntegrator & pim = *lc.pims[iF_loc];            
            for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
                const Point & xQN = pim.point(iQN);
                const Real & wQN = pim.weight(iQN);
                Real u_iqn = u_n(xQN) ;         
                for(std::size_t i = 0; i < lc.nb_cell_dofs; i++) {
                    const typename LC::CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
                    P_n[iT](i) += wQN * u_iqn * phi_i_iqn;
                } // for i
            }//for iQN 
        } // For iF_loc
    } // for iT 

} //set_previous_time_density_from_cont_fct


template<std::size_t K>
void NLContrib<K>::set_time_step(const Real & dt)
{
    m_dt = dt;

    //!! 
    //RMK 
    // if one want to add a loading term depending on time, 
    // it has to add it here ...
    // BUT, to do that, we need to known the effective time where the solution is computed 
    // Then, one can precompute the term (interpolation of f_evol) at the beginning, 
    // and use it to compute all the P(w) 
    // (we do not need to compute the interpolation at every evaluation of P)
    // QUESTION : what time should we use ? t^n or t^{n+1} ??

} // set_time_step    

template<std::size_t K>
void NLContrib<K>::set_espilon_stab(const Real & eps)
{
    m_eps_sta = eps;

} // set_epsilon_stab


template<std::size_t K>
void NLContrib<K>::set_current_point(const DU & w)
{
    m_w = w; 
} // set_current_point    



template<std::size_t K>
bool NLContrib<K>::compute_local_NL_contributions(  const Mesh * Th, 
                                                    const int & iT, 
                                                    const GC & gc ,
                                                    const SolutionVectorType & w_TF, 
                                                    const SolutionVectorType & Phi_TF, 
                                                    const Statistics & stat, 
                                                    SolutionVectorType & P,
                                                    DenseMatrixType & J1,
                                                    DenseMatrixType & J2,
                                                    DenseMatrixType & J3,
                                                    DenseMatrixType & J4, 
                                                    Real & norm_P_cells ) const 
{
    
    bool arg_is_too_big = false; 
    // get the local contribution and the local dofs
    const LC & lc = gc.getLC(iT); 
    const Cell & T = Th->cell(iT);
    const Real & h_T = cell_diameter(Th, iT); 

    const Real r = gc.r; 
        
    const SolutionVectorType QF_TF = w_TF + (1.-r) * Phi_TF;  // dofs of the local quasi-Fermi potential h(u) + \Phi 
    const SolutionVectorType & coeff_w_T = w_TF.head(lc.nb_cell_dofs);
    const SolutionVectorType & coeff_Phi_T = Phi_TF.head(lc.nb_cell_dofs);
    const SolutionVectorType & coeff_T = coeff_w_T - r * coeff_Phi_T; 

    //Inititialize the jacobian and the function
    J1 = DenseMatrixType::Zero(lc.nb_cell_dofs,lc.nb_cell_dofs);
    J2 = DenseMatrixType::Zero(lc.nb_cell_dofs,lc.nb_tot_faces_dofs);
    J3 = DenseMatrixType::Zero(lc.nb_tot_faces_dofs,lc.nb_cell_dofs);
    J4 = DenseMatrixType::Zero(lc.nb_tot_faces_dofs,lc.nb_tot_faces_dofs);
    P = SolutionVectorType::Zero(lc.nb_tot_dofs);     

    //Get the polynomial Basis evaluation 
    const std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > & feval_basisT_PTF = lc.feval_basisT_PTF; 
    const std::vector<std::shared_ptr<BasisFunctionEvaluation<CellBasis> > > & feval_basisT_F = lc.feval_basisT_F;
    const std::vector<std::shared_ptr<BasisFunctionEvaluation<FaceBasis> > > & feval_basisF = lc.feval_basisF; 
    const std::vector<std::shared_ptr<BasisFunctionEvaluation<CellVecBasis> > > & feval_vec_basisT_PTF = lc.feval_vec_basisT_PTF;

    //Precompute some quantities used many times

    std::vector<std::vector<Real>> Arg_QN;  // values of arg_iqn = h(u(x_qn)) on cells 
    Arg_QN.resize(lc.m);    
    for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
        const PyramidIntegrator & pim = *lc.pims[iF_loc];  
        Arg_QN[iF_loc].resize(pim.numberOfPoints());           
        for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
            const Real & wQN = pim.weight(iQN);
            //computation of the argument of g  at xQN
            Real arg_iqn = 0.;         
            for(std::size_t i = 0; i < lc.nb_cell_dofs; i++) {
                const typename LC::CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
                arg_iqn += coeff_T[i] * phi_i_iqn;
            } // for i
            Arg_QN[iF_loc][iQN]= arg_iqn;
            if(std::fabs(arg_iqn) > max_size_arg ){
                std::cout << "Argument with excessive size (cells) :\t" <<  std::fabs(arg_iqn) <<std::endl; 
                return false; 
            } // if arg is too big
        }//for iQN 
    } // For iF_loc 


    
    // // // // // // // // // // // //     
    // // // // // // // // // // // //              
    //contribution #_n (Jacobian = 0 )

    P += - (1./m_dt) * P_n[iT]; 
    
    
    // // // // // // // // // // // // 
    // // // // // // // // // // // //        
    //contribution #cst  (Jacobian = 0 )
    
    P += - lc.bTF; 


    // // // // // // // // // // // //    
    // // // // // // // // // // // //
    //contribution #evol        

    DenseMatrixType  J1_evol = DenseMatrixType::Zero(lc.nb_cell_dofs,lc.nb_cell_dofs);
    //NB : the others blocs of the jacobian are zeros 
    SolutionVectorType  P_evol = SolutionVectorType::Zero(lc.nb_tot_dofs); 

    for(int iF_loc = 0; iF_loc < lc.m; iF_loc++) {
        const PyramidIntegrator & pim = *lc.pims[iF_loc];            
        for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++){
            const Real & wQN = pim.weight(iQN);
            const Real & arg_iqn = Arg_QN[iF_loc][iQN];          

            //set the contribution (fonction and jacobian)
            for(std::size_t i = 0; i < lc.nb_cell_dofs; i++) {
                const typename LC::CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
                P_evol(i) += wQN * stat.g(arg_iqn) * phi_i_iqn; 
                for(std::size_t j = 0; j < lc.nb_cell_dofs; j++) {
                    const typename LC::CellBasis::ValueType & phi_j_iqn = (*feval_basisT_PTF[iF_loc])(j, iQN);
                    J1_evol(i,j) += wQN * phi_j_iqn *  stat.dg(arg_iqn) * phi_i_iqn;
                } //for j
            } //for i

        }//for iQN 
    } // For iF_loc 

    P += (1./m_dt) * P_evol;
    J1 += (1./m_dt) * J1_evol; 
    
    
    // // // // // // // // // // // //
    // // // // // // // // // // // //
    //contribution #stab_lin  (Rmk: easy,linear in w ?)       
     
    ////////
    // Here, the scaling of the linear stabilisation !!
    // !!!!!!!!!!
    // !!!!!!!!!!
    const Real size_stab = std::pow(h_T, K+2); 
    // !!!!!!!!!!
    // !!!!!!!!!!

    const Real s_lin_T = m_eps_sta *  size_stab; 
    
    P   +=  s_lin_T * lc.ATF * QF_TF;  
    J1  +=  s_lin_T * lc.ATF.topLeftCorner(lc.nb_cell_dofs,lc.nb_cell_dofs); 
    J2  +=  s_lin_T * lc.ATF.topRightCorner(lc.nb_cell_dofs,lc.nb_tot_faces_dofs);
    J3  +=  s_lin_T * lc.ATF.bottomLeftCorner(lc.nb_tot_faces_dofs,lc.nb_cell_dofs); 
    J4  +=  s_lin_T * lc.ATF.bottomRightCorner(lc.nb_tot_faces_dofs,lc.nb_tot_faces_dofs); 


    // // // // // // // // // // // //
    // // // // // // // // // // // //
    //contribution #cons  

    const SolutionVectorType Grad_QF_T = lc.G_full_T * QF_TF; 

     
    DenseMatrixType S_Lambda_u = DenseMatrixType::Zero(lc.NG_full,lc.NG_full); // the matrix of \int_T u_K \Lambda \Tauj . \Tau_i 
    DenseMatrixType S_Lambda_u_diff = DenseMatrixType::Zero(lc.NG_full, lc.nb_tot_dofs);	// the matrix of \int_T g'(w_K - r \Phi_K) \Lambda \Tauj . \Tau_i 

    for(int iF_loc = 0 ; iF_loc < lc.m; iF_loc++) {	
        const PyramidIntegrator & pim = *lc.pims[iF_loc];									

        for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
            const Point & xQN = pim.point(iQN);										
            const Real & wQN = pim.weight(iQN);	
            const Real & arg_iqn = Arg_QN[iF_loc][iQN];
            const TensorType & Lambda_iqn = m_Lambda(xQN); 
            
            //compute the gradient of the quasi-Fermi potential at the quadrature node 
            typename CellVecBasis::ValueType grad_QF_iqn;           
            grad_QF_iqn <<
            0. ,
            0. ;
            for(std::size_t i = 0; i < lc.NG_full; i++) {
                const typename CellVecBasis::ValueType & tau_i_iqn = (*feval_vec_basisT_PTF[iF_loc])(i, iQN);
                grad_QF_iqn += Grad_QF_T(i) * tau_i_iqn;
            } //for i 
             

            for(std::size_t i = 0; i < lc.NG_full; i++) {
                const typename CellVecBasis::ValueType & tau_i_iqn = (*feval_vec_basisT_PTF[iF_loc])(i, iQN);
            
                // S_Lambda_u : \int_T u_K \Lambda \Tauj . \Tau_i 
                for(std::size_t j = 0; j< lc.NG_full; j ++){
                    const typename CellVecBasis::ValueType & tau_j_iqn = (*feval_vec_basisT_PTF[iF_loc])(j, iQN);
                    S_Lambda_u(i,j) += wQN * stat.g(arg_iqn) *  tau_i_iqn.dot(Lambda_iqn * tau_j_iqn);
                } // for j
                

                // S_Lambda_u_diff : \int_T g'(w_K - r \Phi_K) phi_j \Lambda \grad_QF . \Tau_i 
                for(std::size_t j = 0; j < lc.nb_local_cell_dofs; j++) {
                    const typename CellBasis::ValueType & phi_j_iqn = (*feval_basisT_PTF[iF_loc])(j, iQN);
                    S_Lambda_u_diff(i,j) +=  wQN * stat.dg(arg_iqn) *  phi_j_iqn * tau_i_iqn.dot(Lambda_iqn *grad_QF_iqn );              
                } // for j
            } // for i         
        } // for iQN    
    } // for iF_loc

    const SolutionVectorType P_cons = lc.G_full_T.transpose() * ( S_Lambda_u * Grad_QF_T ) ; 
    const DenseMatrixType J_cons = lc.G_full_T.transpose() * (  S_Lambda_u * lc.G_full_T  +  S_Lambda_u_diff  ); 

    P   +=  P_cons;   
    J1  +=  J_cons.topLeftCorner(lc.nb_cell_dofs,lc.nb_cell_dofs); 
    J2  +=  J_cons.topRightCorner(lc.nb_cell_dofs,lc.nb_tot_faces_dofs);
    J3  +=  J_cons.bottomLeftCorner(lc.nb_tot_faces_dofs,lc.nb_cell_dofs); 
    J4  +=  J_cons.bottomRightCorner(lc.nb_tot_faces_dofs,lc.nb_tot_faces_dofs); 

    // std::cout << "P cons : \n " << P_cons << std::endl; 
    // std::cout << "Jac_1 cons :\n " << J_cons.topLeftCorner(lc.nb_cell_dofs,lc.nb_cell_dofs) <<std::endl; 
    // std::cout << "Jac_2 cons :\n " << J_cons.topRightCorner(lc.nb_cell_dofs,lc.nb_tot_faces_dofs) <<std::endl; 
    // std::cout << "Jac_3 cons :\n " << J_cons.bottomLeftCorner(lc.nb_tot_faces_dofs,lc.nb_cell_dofs) <<std::endl; 
    // std::cout << "Jac_4 cons :\n " << J_cons.bottomRightCorner(lc.nb_tot_faces_dofs,lc.nb_tot_faces_dofs) <<std::endl; 

   // // // // // // // // // // // //
    // // // // // // // // // // // //
    //contribution #stab : the final boss !!
    
    DenseMatrixType Stab_mat  = DenseMatrixType::Zero(lc.nb_tot_dofs,lc.nb_tot_dofs); 
    DenseMatrixType J_stab_2  = DenseMatrixType::Zero(lc.nb_tot_dofs,lc.nb_tot_dofs); 
    //SolutionVectorType P_stab  = SolutionVectorType::Zero(lc.nb_tot_dofs);  



   for(int iF_loc = 0 ; iF_loc < lc.m; iF_loc++) {	        
        // get the size of the face
        const int & iF = T.faceId(iF_loc);											
        const Face & F = Th->face(iF);													
        const Real & hF = F.measure();

        const FaceIntegrator & fim = *lc.fims[iF_loc];

        
        
        // get some relevant matrices 		
        const Real & lambda_intensity_F = lc.Lambda_intensity[iF_loc]; 
        const DenseMatrixType & Extract_F = lc.Extract_Faces[iF_loc]; 
        const DenseMatrixType & Proj_TF = lc.Proj_cell_to_faces[iF_loc];   
        const DenseMatrixType J_F = Proj_TF * lc.Extract_T - Extract_F; // matrix representing the jump \pi_F^k(v_T) - v_F
        
        const SolutionVectorType Arg_F_coeffs = Extract_F *(w_TF - r * Phi_TF ); 

        //initialise matrices
        DenseMatrixType S_F = DenseMatrixType::Zero(lc.nb_local_face_dofs,lc.nb_local_cell_dofs); // the matrix of \int_F m( ... )  \phi_j . \phi_i 
        DenseMatrixType S_F_d1 = DenseMatrixType::Zero(lc.nb_local_face_dofs,lc.nb_local_cell_dofs);	// the matrix of \int_F d1_m( ... )  \phi_j . \phi_i 
        DenseMatrixType S_F_d2 = DenseMatrixType::Zero(lc.nb_local_face_dofs,lc.nb_local_cell_dofs);	// the matrix of \int_F d1_m( ... )  \phi_j . \phi_i 
        

        for(int iQN = 0; iQN < fim.numberOfPoints(); iQN++) {
            //const Point & xQN = fim.point(iQN);
            const Real & wQN = fim.weight(iQN);            
            
            //compute the jump of the quasi-Fermi potential, and the argument of the m_g mean at the quadrature node 
            const SolutionVectorType JF_QF_coeffs = J_F * QF_TF;
            Real JF_QF_iqn = 0.;           
            const SolutionVectorType Arg_T_coeffs = Proj_TF * lc.Extract_T * (w_TF - r * Phi_TF ); 
            Real arg_T_iqn =0.; 
            Real arg_F_iqn =0.;
            
            for(std::size_t i = 0; i < lc.nb_local_face_dofs; i++) {
                const typename FaceBasis::ValueType & phi_i_iqn = (*feval_basisF[iF_loc])(i, iQN);
                JF_QF_iqn += JF_QF_coeffs(i) * phi_i_iqn;
                arg_T_iqn += Arg_T_coeffs(i) * phi_i_iqn;
                arg_F_iqn += Arg_F_coeffs(i) * phi_i_iqn;
            } // for i            
            
            if(std::max(std::fabs(arg_T_iqn),std::fabs(arg_F_iqn))  > max_size_arg ){
                std::cout << "Argument with excessive size (faces):\t" <<  std::max(std::fabs(arg_T_iqn),std::fabs(arg_F_iqn)) <<std::endl;
                 return false; 
            } // if argument is too big

            //computation of the mean_g 

            const Real mean_g_iqn = m_mean.m( stat.g(arg_T_iqn), stat.g(arg_F_iqn)); 
            const Real d1_mean_g_iqn = stat.dg( arg_T_iqn )  * m_mean.d1_m( stat.g(arg_T_iqn) , stat.g(arg_F_iqn) ); 
            const Real d2_mean_g_iqn = stat.dg( arg_F_iqn )  * m_mean.d2_m( stat.g(arg_T_iqn) , stat.g(arg_F_iqn) ); 
            
            // computation of the matrices 
            for(std::size_t i = 0; i < lc.nb_local_face_dofs; i++) {
                const typename FaceBasis::ValueType & phi_i_iqn = (*feval_basisF[iF_loc])(i, iQN);
                for(std::size_t j = 0; j < FaceBasis::size; j++) {
                    const typename FaceBasis::ValueType  & phi_j_iqn = (*feval_basisF[iF_loc])(j, iQN);
                    const Real prod_ij_iqn = phi_i_iqn * phi_j_iqn; 
                    S_F(i,j)    += wQN * mean_g_iqn * prod_ij_iqn; 
                    S_F_d1(i,j) += wQN * d1_mean_g_iqn * JF_QF_iqn * prod_ij_iqn; 
                    S_F_d2(i,j) += wQN * d2_mean_g_iqn * JF_QF_iqn * prod_ij_iqn; 
                } // for j
            } // for i
        
        } // for iQN  

        const Real stab_prefact =  m_eta_nl * (lambda_intensity_F/ hF); 

        Stab_mat += stab_prefact * J_F.transpose() * S_F * J_F; 
        J_stab_2 += stab_prefact * J_F.transpose() * ( S_F_d1 * Proj_TF * lc.Extract_T  + S_F_d2 * Extract_F ); 
    } // for iF_loc

    
    const SolutionVectorType P_stab = Stab_mat * QF_TF; 
    const DenseMatrixType  J_stab = Stab_mat + J_stab_2;

    P   +=  P_stab;   
    J1  +=  J_stab.topLeftCorner(lc.nb_cell_dofs,lc.nb_cell_dofs); 
    J2  +=  J_stab.topRightCorner(lc.nb_cell_dofs,lc.nb_tot_faces_dofs);
    J3  +=  J_stab.bottomLeftCorner(lc.nb_tot_faces_dofs,lc.nb_cell_dofs); 
    J4  +=  J_stab.bottomRightCorner(lc.nb_tot_faces_dofs,lc.nb_tot_faces_dofs); 


    // std::cout << "P : \n " << P << std::endl; 
    // std::cout << "Jac_1  :\n " << J1 <<std::endl; 
    // std::cout << "Jac_2  :\n " << J2 <<std::endl; 
    // std::cout << "Jac_3  :\n " << J3 <<std::endl; 
    // std::cout << "Jac_4  :\n " << J4 <<std::endl; 


    //Last but not least
    //Compute the norm of the goal function on cells 

    const SolutionVectorType & Goal_T =  P.head(lc.nb_cell_dofs); 
    norm_P_cells = Goal_T.lpNorm<Eigen::Infinity>();         

    return true; 

} //compute_local_NL_contributions



template<std::size_t K>
bool NLContrib<K>::compute_NL_contributions(const Mesh * Th, 
                                            const GC & gc,
                                            const DU & Phi,
                                            const Statistics & stat, 
                                            const bool & test_gaudeul )
{   
    //clear the GS in order to compute the new one     
    m_global_system.clear_gs(); 
    m_goal_norm_infty = 0.;

    if(test_gaudeul){
        m_P_edges = SolutionVectorType::Zero(gc.nb_faces_dofs);
        m_P_cells.clear();
        m_P_cells.resize(gc.nb_cells);
    } //if test_gaudeul, clear and initialize the data 
     
    for(int iT = 0; iT <gc.nb_cells; iT ++){
        
        //Create the jacobian and the goal function
        DenseMatrixType  J1; 
        DenseMatrixType  J2; 
        DenseMatrixType  J3; 
        DenseMatrixType  J4; 
        SolutionVectorType  P; 
        Real norm_P_cells; 

        //Compute the local non linear contribution on cell T 
        const SolutionVectorType w_TF = m_w.get_local_dofs(gc,iT);
        const SolutionVectorType Phi_TF = Phi.get_local_dofs(gc,iT);
        
        const bool arg_size_ok = compute_local_NL_contributions( Th, iT, gc, w_TF, Phi_TF, stat, P, J1, J2, J3, J4, norm_P_cells );            
        if(!arg_size_ok){ 
            return false; 
        } // if the computation was not possible 
        m_goal_norm_infty = std::max(m_goal_norm_infty , norm_P_cells);          
        
        
        // set the local system
        m_global_system.set_local_problem(iT, gc, J1, J2, J3, J4, - P);    
        //RMK : Newton method : JP * res = - P  


        //save the goal functions (non condensed) for the Gaudeul test
        if(test_gaudeul){
            const LC & lc = gc.getLC(iT); 
            m_P_cells[iT] = P.head(lc.nb_cell_dofs);

            const SolutionVectorType & P_edges_T = P.tail(lc.nb_tot_faces_dofs );
            const IndexVectorType & idx_T = lc.idx_T; 
            for(int i = 0; i < idx_T.size(); i++) {
                m_P_edges(idx_T(i)) += P_edges_T(i);
            } // for i

        } //if test_gaudeul, computation of non condensed quantities

    } // for iT         
    
    //compute the norm of the goal function
    m_goal_norm_infty = std::max(m_goal_norm_infty ,m_global_system.get_norm_RHS(gc));

    //m_global_system.compute_LU_Decompo_global( gc );
    m_computation_done= true; 
    return true; 

} // compute_NL_contribution


template<std::size_t K>
GlobalSys<K> NLContrib<K>::get_globalsystem() const
{   
    if(not(m_computation_done)){
        std::cout<<"Computation of the Nonlinear Contributions was not performed" <<std::endl;
        std::cout<<"Please perform the computation before calling get_globalsystem !" <<std::endl; 
        exit( 0 ); 
    }
    return m_global_system;
} // get_globalsystem

template<std::size_t K>
Real NLContrib<K>::get_norm_goal() const
{
    return m_goal_norm_infty;
}    


template<std::size_t K>
SolutionVectorType NLContrib<K>::get_goal_edges() const
{
    return m_P_edges;

} // get_goal_egdes

template<std::size_t K>
SolutionVectorType NLContrib<K>::get_goal_cell( const int & iT ) const
{
    return m_P_cells[iT];

} //get_goal_cell






} //namespace NL
} //namespace ho


#endif