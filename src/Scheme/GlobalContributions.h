#ifndef GLOBALCONTRIBUTIONS_H
#define GLOBALCONTRIBUTIONS_H

#include "ContinuousData/Data.h"
#include "LocalContributions.h"
#include "Common/chrono.hpp"

typedef Eigen::MatrixXd DenseMatrixType;  
typedef Eigen::SparseMatrix<Real> SparseMatrixType;
typedef Eigen::Matrix<Real, Eigen::Dynamic, 1> SolutionVectorType;
typedef Eigen::Triplet<Real> TripletType;
typedef std::vector<TripletType> TripletsVectorType;


namespace ho{
namespace diffusion{



template<std::size_t K>
class GlobalContrib
{
    typedef ho::diffusion::LocalContrib<K> LC;  
    
    public:
    //
    //Default constructor
    GlobalContrib(); 

    GlobalContrib(  const Mesh * Th,
                    const LoadType & load,
                    const BoundaryNeumannType & g_n,
                    const DiffusivityType & Lambda,
                    const DefBoundaryConditionType & isDir, 
                    const DefBoundaryConditionType & isNeu,
                    const Real & _r = 0., 
                    const int & add_integrator = 3,
                    const Real & eta = 1.);    
                    
    int nb_cells;
    int nb_faces; 
    int nb_faces_dofs; 
    int nb_Dirichlet_faces;
    int nb_Dirichlet_dofs; 
    
    // the parameter defining the contribution of the electrostatic potential in the PQFP
    Real r; 
    
    //resize matrix for enforcing Dirichlet boundary conditions
    SparseMatrixType resize_mat;
    //access to the local contribution of the cell iT
    LC getLC(const int & iT ) const;  
    //
    //creation of the interpolation on $\Gamma^D$
    SolutionVectorType getInterpolationDirBoundary( const Mesh * Th,
                                                    const DefBoundaryConditionType & isDir, 
                                                    const BoundaryDirichletType & u_D ) const;    
    //creation of the interpolation on the faces of the mesh only 
    SolutionVectorType getInterpolationFaces(   const Mesh * Th,
                                                const ExactSolutionType & u) const;


    Real time_computation; 

    private:  
    std::vector<LC> Local_Contribs; // vectors of local contributions
    


};  // class GlobalContribs


//------------------------------------------------------------------------------
// Implementation GlobalContrib 

template<std::size_t K>
GlobalContrib<K>::GlobalContrib()
{

} //default constructor 

template<std::size_t K>
GlobalContrib<K>::GlobalContrib(const Mesh * Th,
                                const LoadType & load,
                                const BoundaryNeumannType & g_n,
                                const DiffusivityType & Lambda,
                                const DefBoundaryConditionType & isDir, 
                                const DefBoundaryConditionType & isNeu,
                                const Real & _r,
                                const int & add_integrator ,
                                const Real & eta )
{
    
    std::cout << "\nPrecomputation : generation of the Global Contributions for diffusion problem begins" << std::endl;  
    common::chrono c_comp;
    c_comp.start();

    r = _r;  
    
    nb_cells = Th->numberOfCells();
    nb_faces = Th->numberOfFaces();
    nb_faces_dofs = nb_faces * LC::nb_local_face_dofs;
    
    nb_Dirichlet_faces = 0;
    for(Integer iF = 0; iF < nb_faces; iF++) {
        const Face & F = Th->face(iF);
        if(isDir(F)){ 
            nb_Dirichlet_faces += 1; 
        } // if F is Dirichlet
    } // for iF 


    nb_Dirichlet_dofs = nb_Dirichlet_faces * LC::nb_local_face_dofs; 
  
    std::cout << "Number of faces with Dirichlet boundary condition " << nb_Dirichlet_faces << std::endl;
    std::cout << "Number of unknowns associated to Dirichlet boundary condition " << nb_Dirichlet_dofs << std::endl;    
    
 
    // creation of the resize matrix 
    resize_mat.resize(nb_faces_dofs - nb_Dirichlet_dofs, nb_faces_dofs);  
    std::vector<TripletType> triplets_resize;
    triplets_resize.reserve(nb_faces_dofs);  
    
    int counter_Fb_dir = 0;
    for(Integer iF = 0; iF < Th->numberOfFaces(); iF++){
        const Face F = Th->face(iF);
        if(!(isDir(F))){
            for(int i = 0; i < LC::nb_local_face_dofs; i++){
            triplets_resize.push_back(TripletType((iF - counter_Fb_dir) * LC::nb_local_face_dofs + i, iF * LC::nb_local_face_dofs + i, 1.));
            }// for i
        }//if Face is not Dirichlet 
        else{counter_Fb_dir++;}        
    } // for iF 
    
    resize_mat.setFromTriplets(triplets_resize.begin(), triplets_resize.end());

    
    //Local_Contribs.resize(nb_cells);
    Local_Contribs.clear();
    for(int iT = 0; iT < nb_cells; iT++) {    
        const LC lc(Th, iT, load , g_n , Lambda , isNeu , add_integrator, eta );     
        //Local_Contribs[iT] = lc;
        Local_Contribs.push_back(lc);  
    }  // for iT
    
    c_comp.stop();
    std::cout << "Precomputations (Global contribs) : DONE !"  << std::endl;  
    std::cout << FORMAT(50) << "Time of the precomputational step for Global contribution : \t"  << c_comp.diff() << std::endl;

    time_computation = c_comp.diff();

} //GlobalContrib, constructor 

template<std::size_t K>
ho::diffusion::LocalContrib<K> GlobalContrib<K>::getLC(const int & iT) const 
{
    return Local_Contribs[iT]; 
}

template<std::size_t K>
SolutionVectorType GlobalContrib<K>::getInterpolationDirBoundary(   const Mesh * Th,
                                                                    const DefBoundaryConditionType & isDir,
                                                                    const BoundaryDirichletType & u_D) const
{
    SolutionVectorType X_D =  SolutionVectorType::Zero(nb_faces_dofs);    
    for(int iT = 0; iT < nb_cells; iT ++){
        const Cell & T = Th->cell(iT);
        const LC & lc = getLC(iT); 
        Eigen::VectorXd g_Dirichlet_iF = Eigen::VectorXd::Zero(lc.nb_tot_faces_dofs);
        for(int iF_loc = 0; iF_loc < (Th->cell(iT)).numberOfFaces(); iF_loc++){
            const Face & F = Th->face(T.faceId(iF_loc));
            if(isDir(F)) {
                g_Dirichlet_iF.segment(iF_loc * LC::nb_local_face_dofs, LC::nb_local_face_dofs) += lc.interpolate(Th, iT, u_D ).segment(LC::nb_cell_dofs + iF_loc * LC::nb_local_face_dofs, LC::nb_local_face_dofs);
            } // if Face is dirichlet
        } // for iF_loc
        
        for(int i = 0; i < lc.idx_T.size(); i++) {
            X_D(lc.idx_T(i)) += g_Dirichlet_iF(i);
        } // for i
    } //for iT

    return X_D;
}

template<std::size_t K>
SolutionVectorType GlobalContrib<K>::getInterpolationFaces( const Mesh * Th,
                                                            const ExactSolutionType & u) const
{
    SolutionVectorType X =  SolutionVectorType::Zero(nb_faces_dofs);  
    std::vector<bool> face_non_asigned( nb_faces , true );  
    for(int iT = 0; iT < nb_cells; iT ++){
        const Cell & T = Th->cell(iT);
        const LC & lc = getLC(iT); 
        Eigen::VectorXd g_iF = Eigen::VectorXd::Zero(lc.nb_tot_faces_dofs);
        Eigen::VectorXd u_T_I = lc.interpolate(Th, iT, u );
        for(int iF_loc = 0; iF_loc < nb_cells ; iF_loc++){
            const int iF = T.faceId(iF_loc);
            if(face_non_asigned[iF]){
                g_iF.segment(iF_loc * LC::nb_local_face_dofs, LC::nb_local_face_dofs) = u_T_I.segment(LC::nb_cell_dofs + iF_loc * LC::nb_local_face_dofs, LC::nb_local_face_dofs);
                face_non_asigned[iF] = false; 
            } // if F is non asigned
        } // for iF_loc
        
        for(int i = 0; i < lc.idx_T.size(); i++) {
            X(lc.idx_T(i)) += g_iF(i);
        } // for i
    } //for iT

    return X;
}




} // namespace diffusion
} //namespace ho


#endif