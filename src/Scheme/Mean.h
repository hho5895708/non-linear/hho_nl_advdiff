#ifndef MEAN_H
#define MEAN_H


#include "Common/defs.h"

typedef std::function<Real(const Real &)> RealFctType;
typedef std::function<Real(const Real &,const Real & )> MeanFctType;


class Mean_Fct
{
    public:
    Mean_Fct();
    Mean_Fct(const std::string & type_mean);
    Real m(const Real & x, const Real y) const; 
    Real d1_m(const Real & x, const Real y) const;
    Real d2_m(const Real & x, const Real y) const;


    private:
    MeanFctType m_mean;
    MeanFctType m_d1_mean;
    MeanFctType m_d2_mean;


};

#endif