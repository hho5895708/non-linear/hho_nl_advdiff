#include "LocalContributions.h"
/*
namespace ho
{

  namespace diffusion
  {


    //------------------------------------------------------------------------------
    // Implementation LocalContributions

    template<std::size_t K>
    LocalContrib<K>::LocalContrib(  const Mesh * Th,
                                    const Integer & iT,
                                    const LoadType & load,
                                    const BoundaryNeumannType & g_n,
                                    const DiffusivityType & Lambda,
                                    const DefBoundaryConditionType & isNeu,
                                    const int & add_integrator,
                                    const Real & eta)
    {
        T = Th->cell(iT);
        m = T.numberOfFaces(); 
      
      
        const Point & xT = T.center();

        // Estimate hT
        Real hT = cell_diameter(Th, iT);

        std::vector<Real> Lambda_intensity; 

        //------------------------------------------------------------------------------
        // Precompute bases

        basisT.reset(new CellBasis(T.center(), hT));		
        basis_vec_T.reset(new CellVecBasis(T.center(), hT));		
        basisF.resize(m);
        for(int iF_loc = 0; iF_loc < m; iF_loc++) {
            const Face & F = Th->face(T.faceId(iF_loc));	
            const Point & xF = F.barycenter();		
            const Real & hF = F.measure();	
            basisF[iF_loc].reset(new FaceBasis(F.point(0).first, xF, hF));
        }

        // Evaluate bases at quadrature nodes
        feval_basisT_PTF.resize(m);
        deval_basisT_PTF.resize(m);
        feval_basisT_F.resize(m);
        deval_basisT_F.resize(m);
        feval_basisF.resize(m);     
        feval_vec_basisT_PTF.resize(m);
        feval_vec_basisT_F.resize(m);

        pims.resize(m);
        fims.resize(m);

        for(int iF_loc = 0; iF_loc < m; iF_loc++) {
            Integer iF = T.faceId(iF_loc);			

            pims[iF_loc].reset(new PyramidIntegrator(Th, iT, iF_loc, 2*(K+1) + add_integrator));
            const PyramidIntegrator & pim = *pims[iF_loc];

            feval_basisT_PTF[iF_loc].reset(new BasisFunctionEvaluation<CellBasis>(basisT.get(), pim.points()));
            deval_basisT_PTF[iF_loc].reset(new BasisGradientEvaluation<CellBasis>(basisT.get(), pim.points()));
            feval_vec_basisT_PTF[iF_loc].reset(new BasisFunctionEvaluation<CellVecBasis>(basis_vec_T.get(), pim.points()));

            fims[iF_loc].reset(new FaceIntegrator(Th, iF, 2*(K+1) + add_integrator  ));	
            const FaceIntegrator & fim = *fims[iF_loc];
            feval_basisT_F[iF_loc].reset(new BasisFunctionEvaluation<CellBasis>(basisT.get(), fim.points()));
            deval_basisT_F[iF_loc].reset(new BasisGradientEvaluation<CellBasis>(basisT.get(), fim.points(), 1));
            feval_basisF[iF_loc].reset(new BasisFunctionEvaluation<FaceBasis>(basisF[iF_loc].get(), fim.points()));      
            feval_vec_basisT_F[iF_loc].reset(new BasisFunctionEvaluation<CellVecBasis>(basis_vec_T.get(), fim.points()));

        } // for iF_loc

        //------------------------------------------------------------------------------
        // Count local unknowns

        nb_tot_faces_dofs = m * FaceBasis::size; 								
        nb_tot_dofs = nb_cell_dofs + nb_tot_faces_dofs;		
        
        bTF = Eigen::VectorXd::Zero(nb_tot_dofs);      
        
        
        //------------------------------------------------------------------------------
        // Creation of the local -> global mapping for faces dofs 
        
        bTF = Eigen::VectorXd::Zero(nb_tot_dofs);  
        idx_T.resize( m * nb_local_face_dofs);
        std::size_t index_idx = 0;
        for(Integer iF_loc = 0; iF_loc < m ; iF_loc++) {
            std::size_t offset_F = T.faceId(iF_loc) * nb_local_face_dofs;
            for(std::size_t i = 0; i < nb_local_face_dofs; i++) {
                idx_T[index_idx++] = offset_F + i;
            } // for i
        } // for iF_loc

        //------------------------------------------------------------------------------
        // Gradient reconstruction
        
        Eigen::MatrixXd SG = Eigen::MatrixXd::Zero(NG_full, NG_full);	
        Eigen::MatrixXd SG_Lambda = Eigen::MatrixXd::Zero(NG_full, NG_full);	
        Eigen::MatrixXd RG = Eigen::MatrixXd::Zero(NG_full, nb_tot_dofs); // matrix of the RHS of the discrete IBP formula defining the full discrete gradient 


        MTT = Eigen::Matrix<Real, CellBasis::size, CellBasis::size>::Zero(); 	
        MFF.resize(m);				
        LU_MFF.resize(m);

        //std::vector<Eigen::Matrix<Real, FaceBasis::size, CellBasis::size> > MFT(m);	
        MFT.resize(m);	

        for(int iF_loc = 0 ; iF_loc < m; iF_loc++) {
            int iF = T.faceId(iF_loc);											
            const Face & F = Th->face(iF);											
            Eigen::Matrix<Real, DIM, 1> nTF;										
            { Point _nTF = F.normal(xT); nTF << _nTF(0), _nTF(1); }								
            const Real & hF = F.measure();			       
            
            Real Lambda_TF = 0.; 								

            const PyramidIntegrator & pim = *pims[iF_loc];									
            const FaceIntegrator & fim = *fims[iF_loc];

            //------------------------------------------------------------------------------
            // Initialize matrices

            MFT[iF_loc] = Eigen::Matrix<Real, FaceBasis::size, CellBasis::size>::Zero();					
            MFF[iF_loc] = Eigen::Matrix<Real, FaceBasis::size, FaceBasis::size>::Zero();		

            //------------------------------------------------------------------------------
            // Volumetric terms

            for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
            const Point & xQN = pim.point(iQN);										
            const Real & wQN = pim.weight(iQN);									          
            
            // full gradient
            for(std::size_t i = 0; i < NG_full; i++) {
                const typename CellVecBasis::ValueType & tau_i_iqn = (*feval_vec_basisT_PTF[iF_loc])(i, iQN);

                // LHS
                for(std::size_t j = 0; j< NG_full; j ++){
                const typename CellVecBasis::ValueType & tau_j_iqn = (*feval_vec_basisT_PTF[iF_loc])(j, iQN);
                SG(i,j) += wQN * tau_i_iqn.dot(tau_j_iqn);              
                const TensorType Lambda_iqn = Lambda(xQN);
                SG_Lambda(i,j) += wQN * tau_i_iqn.dot(Lambda_iqn * tau_j_iqn);
                } // for j
                

                // RHS (\GRAD vT, Tau )_{PTF}
                for(std::size_t j = 0; j < nb_cell_dofs; j++) {
                const typename CellBasis::GradientType & dphi_j_iqn = (*deval_basisT_PTF[iF_loc])(j, iQN);
                RG(i,j) += wQN * dphi_j_iqn.dot(tau_i_iqn);              
                } // for j
            } // for i
            
            
            // MTT (mass matrix)
            for(std::size_t i = 0; i < CellBasis::size; i++) {
                const Real & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
                for(std::size_t j = 0; j < CellBasis::size; j++) {
                const Real & phi_j_iqn = (*feval_basisT_PTF[iF_loc])(j, iQN);
                MTT(i,j) += wQN * phi_i_iqn * phi_j_iqn;
                } // for j
            } // for i

            // Forcing term
            for(std::size_t i = 0; i < nb_cell_dofs; i++) {
                const Real & phi_i_iqn = (*feval_basisT_PTF[iF_loc])(i, iQN);
                bTF(i) += wQN * phi_i_iqn * load(xQN);
            } // for i
            } // for iQN

            //------------------------------------------------------------------------------
            // Interface terms

            for(int iQN = 0; iQN < fim.numberOfPoints(); iQN++) {
            const Point & xQN = fim.point(iQN);
            const Real & wQN = fim.weight(iQN);

            Lambda_TF += wQN * std::pow((Lambda(xQN) * nTF).dot(nTF), 2); 

            // Offset for face unknowns
            const std::size_t offset_F = nb_cell_dofs + iF_loc * FaceBasis::size;

            // full gradient
            for(std::size_t i = 0; i < NG_full; i++) {
                const typename CellVecBasis::ValueType & tau_i_iqn = (*feval_vec_basisT_F[iF_loc])(i, iQN);
                const Real tau_i_n_iqn = tau_i_iqn.dot(nTF);

                // RHS (v_F, Tau \SCAL n_{TF})_F
                for(std::size_t j = 0; j < FaceBasis::size; j++) {
                const typename FaceBasis::ValueType & phi_j_iqn = (*feval_basisF[iF_loc])(j, iQN);
                RG(i,offset_F + j) += wQN * tau_i_n_iqn * phi_j_iqn;
                } // for j

                // RHS -(v_T, Tau \SCAL n_{TF})_F
                for(std::size_t j = 0; j < nb_cell_dofs; j++) {
                const typename CellBasis::ValueType & phi_j_iqn = (*feval_basisT_F[iF_loc])(j, iQN);
                RG(i,j) -= wQN * tau_i_n_iqn * phi_j_iqn;
                } // for j
            } // for i            
            
            // loading term associated to the Neumann BC
            if(isNeu(F)){
                for(std::size_t i = 0; i < FaceBasis::size; i++) {
                const Real & phi_i_iqn = (*feval_basisF[iF_loc])(i, iQN);
                bTF(offset_F + i)+= wQN * phi_i_iqn * g_n(xQN);  
                }//for i
            } // if F is Neumann

            // MFT (matrices "interraction between faces and cells")
            for(std::size_t i = 0; i < FaceBasis::size; i++) {
                const typename FaceBasis::ValueType & phi_i_iqn = (*feval_basisF[iF_loc])(i, iQN);
                
                for(std::size_t j = 0; j < CellBasis::size; j++) {
                const typename CellBasis::ValueType & phi_j_iqn = (*feval_basisT_F[iF_loc])(j, iQN);
                MFT[iF_loc](i,j) += wQN * phi_i_iqn * phi_j_iqn;
                } // for j
            } // for i

            // MFF ("mass" matrices associated to the faces)
            for(std::size_t i = 0; i < FaceBasis::size; i++) {
                const typename FaceBasis::ValueType & phi_i_iqn = (*feval_basisF[iF_loc])(i, iQN);
                //const auto & phi_i_iqn = (*feval_basisF[iF_loc])(i, iQN);
                for(std::size_t j = 0; j < FaceBasis::size; j++) {
                const typename FaceBasis::ValueType  & phi_j_iqn = (*feval_basisF[iF_loc])(j, iQN);
                //const auto & phi_j_iqn = (*feval_basisF[iF_loc])(j, iQN);
                MFF[iF_loc](i,j) += wQN * phi_i_iqn * phi_j_iqn;
                } // for j
            } // for i
            } // for iQN
            
            LU_MFF[iF_loc].compute(MFF[iF_loc]);
            Lambda_intensity.push_back(std::sqrt(Lambda_TF));
            
        } // for iF_loc

        //------------------------------------------------------------------------------
        // Consistent terms, gradient of the reconstruction operator

        LU_MTT.compute(MTT);
        G_full_T = SG.ldlt().solve(RG);
        ATF =  G_full_T.transpose() * (SG_Lambda * G_full_T) ; // full gradient 
        //------------------------------------------------------------------------------
        // Stabilization

        STF = Eigen::MatrixXd::Zero(nb_tot_dofs, nb_tot_dofs);
        
        // Matrices for extraction of the cell dofs and faces dofs
        Eigen::MatrixXd Extract_T = Eigen::MatrixXd::Zero(nb_cell_dofs, nb_tot_dofs); // Matrix for the extraction of the cell dofs
        for(int i=0; i< nb_cell_dofs; i++){
            Extract_T(i,i) += 1.;      
        } //for i
        
        //std::vector< Eigen::MatrixXd > Extract_Faces; // vector of extraction matrices for the coordinates of the faces
        //Extract_Faces.resize(m); 
        for(int iF_loc = 0; iF_loc < m; iF_loc++) {        
            int iF = T.faceId(iF_loc);
            const Face & F = Th->face(iF);
            const Real & hF = F.measure();
            
            Eigen::MatrixXd Extract_F = Eigen::MatrixXd::Zero(nb_local_face_dofs, nb_tot_dofs); //extraction matrices for the coordinates of the faces
            const int offset_F = nb_cell_dofs + iF_loc * FaceBasis::size;
            for(int i = 0; i < nb_local_face_dofs; i++){
            Extract_F(i, offset_F + i) += 1.; 
            } // for i       
            
            Eigen::MatrixXd J_TF; // matrix repesenting the jump \pi_F^k(u_T) - u_F 
            J_TF =  LU_MFF[iF_loc].solve( MFT[iF_loc] )* Extract_T - Extract_F;
                    
            STF += eta * (Lambda_intensity[iF_loc] / hF) * J_TF.transpose() * MFF[iF_loc] * J_TF; // RMK : scaling factor = 1/h_F
        } // for iF_loc

        ATF += STF;

        //------------------------------------------------------------------------------
        // Static condensation      
        
        LU_ATT.compute(ATF.topLeftCorner(nb_cell_dofs, nb_cell_dofs));

        AF = ATF.bottomRightCorner(nb_tot_faces_dofs, nb_tot_faces_dofs)
            - ATF.bottomLeftCorner(nb_tot_faces_dofs, nb_cell_dofs) * LU_ATT.solve(ATF.topRightCorner(nb_cell_dofs, nb_tot_faces_dofs));

        bF = bTF.tail(nb_tot_faces_dofs) - ATF.bottomLeftCorner(nb_tot_faces_dofs, nb_cell_dofs) * LU_ATT.solve(bTF.head(nb_cell_dofs));

    } // LocalContributions

    //------------------------------------------------------------------------------

    template<std::size_t K>
    Eigen::VectorXd LocalContrib<K>::interpolate(   const Mesh * Th,
                                                    const Integer & iT,
                                                    const ExactSolutionType & u) const
    {
        const Cell & T = Th->cell(iT);

        Eigen::VectorXd uTF = Eigen::VectorXd::Zero(nb_cell_dofs + m * FaceBasis::size);
        Eigen::VectorXd bT = Eigen::VectorXd::Zero(nb_cell_dofs);

        for(int iF_loc = 0; iF_loc < m; iF_loc++) {
            const PyramidIntegrator & pim = *pims[iF_loc];

            for(std::size_t i = 0; i < nb_cell_dofs; i++) {
            for(int iQN = 0; iQN < pim.numberOfPoints(); iQN++) {
                const Point & xQN = pim.point(iQN);
                const Real & wQN = pim.weight(iQN);

                const typename CellBasis::ValueType & phi_i_iqn = (*feval_basisT_PTF[iF_loc]) (i, iQN);

                bT(i) += wQN * phi_i_iqn * u(xQN);
            } // for iQN
            } // for i

            const FaceIntegrator & fim = *fims[iF_loc];

            Eigen::VectorXd bF = Eigen::VectorXd::Zero(FaceBasis::size);

            for(std::size_t i = 0; i < FaceBasis::size; i++) {
            for(int iQN = 0; iQN < fim.numberOfPoints(); iQN++) {
                const Point & xQN = fim.point(iQN);
                const Real & wQN = fim.weight(iQN);

                const typename FaceBasis::ValueType & phi_i_iqn = (*feval_basisF[iF_loc]) (i, iQN);

                bF(i) += wQN * phi_i_iqn * u(xQN);
            } // for iQN
            } // for i

            std::size_t offset_F = nb_cell_dofs + iF_loc * FaceBasis::size;
            uTF.segment<FaceBasis::size>(offset_F) = LU_MFF[iF_loc].solve(bF);
        } // for iF_loc

        uTF.head(nb_cell_dofs) = LU_MTT.solve(bT);

        return uTF;
    } // interpolate

    //------------------------------------------------------------------------------

    template<std::size_t K>
    template<class SolutionVectorType, class IndexVectorType>
    Eigen::VectorXd LocalContrib<K>::reconstruct(const SolutionVectorType & X) const
    {
        Eigen::VectorXd uTF = Eigen::VectorXd::Zero(nb_tot_dofs);
        for(int i = 0; i < idx_T.size(); i++) { 
            uTF(nb_cell_dofs + i) =X(idx_T(i)); 
        } // for i 
        Eigen::VectorXd u_Faces =  uTF.tail(nb_tot_faces_dofs);
        Eigen::VectorXd rhs_recons = bTF.head(nb_cell_dofs)- ATF.topRightCorner(nb_cell_dofs, nb_tot_faces_dofs) * u_Faces;
        uTF.head(nb_cell_dofs) = LU_ATT.solve(rhs_recons);
        return uTF;
    } // reconstruct

  } // namespace diffusion

} // namespace ho
*/