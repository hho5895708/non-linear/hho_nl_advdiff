// -*- C++ -*-
#ifndef DATA_H
#define DATA_H

#include <boost/math/constants/constants.hpp>
#include "Common/defs.h"
#include "Mesh/Mesh.h"
#include "Stat.h"
#include <complex>

using boost::math::constants::pi;


//type added 2023 (Nonlinear HHO / HDG+ for advection-diffusion equation)
/*----------------------------------------------------------------------------*/
typedef Eigen::Matrix<Real, DIM, DIM > TensorType;
typedef std::function<TensorType(const Point &)> DiffusivityType;
typedef std::function<Real(const Point &)> PotentialType;
typedef std::function<Eigen::Matrix<Real, 2, 1>(const Point &)> FieldType;
typedef std::function<Real(const Point &)> LoadType;
typedef std::function<Real(const Point &)> FctContType;
typedef std::function<Real(const Real &)> RealFctType;
typedef std::function<Real(const Point &)> PotentialType;
typedef std::function<Real(const Point &)> InitialDataType;

typedef std::function<Real(const Point &)> BoundaryDirichletType;
typedef std::function<Real(const Point &)> BoundaryNeumannType;
typedef std::function<bool(const Face &)> DefBoundaryConditionType;

typedef std::function<Real(const Point &)> ExactSolutionType;      
typedef std::function<Real(const Real & t, const Point &)> ExactSolutionTimeType;      
typedef std::function<Eigen::Matrix<Real, DIM, 1>(const Real & t, const Point &)> ExactGradientTimeType; 

typedef std::function<Eigen::Matrix<Real, DIM, 1>(const Point &)> ExactGradientType;

typedef std::complex<double> Complex;
typedef std::function<Complex(const Complex &)> C_to_C_Fct;

//

namespace ho
{
  namespace NL
  {
    struct DataEvol // data for evolutive problem        
    {

      
      typedef std::function<TensorType(const Point &)> DiffusivityType;      
      typedef std::function<Real(const Point &)> PotentialType;
      typedef std::function<Real(const Point &)> LoadType; 
   
      
      typedef std::function<Real(const Point &)> BoundaryDirichletType;
      typedef std::function<Real(const Point &)> BoundaryNeumannType;

      typedef std::function<bool(const Face &)> DefBoundaryConditionType;

      typedef std::function<Real(const Point &)> ExactSolutionType;
      typedef std::function<Real(const Real & t, const Point &)> ExactSolutionTimeType;
      typedef std::function<Eigen::Matrix<Real, DIM, 1>(const Point &)> ExactGradientType;   
      typedef std::function<Eigen::Matrix<Real, DIM, 1>(const Real & t, const Point &)> ExactGradientTimeType;  

      Statistics Stat;
      DiffusivityType Lambda;
      
      PotentialType Phi;
      LoadType f;      
      
      DefBoundaryConditionType isDirichlet;
      DefBoundaryConditionType isNeumann;

      BoundaryDirichletType u_b;
      BoundaryNeumannType g_n;

      InitialDataType u_0; 
      ExactSolutionTimeType u;
      ExactGradientTimeType G;
    };

    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    
    // test case for long-time behaviour + exact sol avaliable (CV)
    struct AdvDiff_exact : public DataEvol
    {  
      AdvDiff_exact(const Real & _nu = 1., const  Real & _C = 0.2, const Real & t_0 = 0.   )  
      {   
        isDirichlet = [](const Face & F) -> bool {
          //Point x_F = F.barycenter();
          //Real x = x_F(0);
          //Real y = x_F(1);
          //bool D_1 = ( x == 1.) ;
          // bool D_2 = ( x == 0.);
          //bool D_2 = (y==1.) && (0. < x && x < 1.);
          //bool Dir = D_1 || D_2;
          bool Dir = false;
          bool Bound = F.isBoundary();
          return Dir && Bound;
        };

        isNeumann = [this](const Face & F) -> bool {
          //Point x_F = F.barycenter();
          //Real x = x_F(0);
          //Real y = x_F(1);
          bool Bound = F.isBoundary();
          bool Dir = this->isDirichlet(F);
          return Bound && (! Dir) ;
        };


        Lambda = [_nu](const Point & x) -> TensorType {
          TensorType L;
          L <<
          _nu , 0. ,
          0. , 1. ;
          return   L;
        };

        Phi =[](const Point & x) -> Real {
          return -x(0);
        };

        u = [_nu,_C, t_0](const Real t,const Point & x) -> Real {
          Real alpha = _nu*(pi<Real>()*pi<Real>() + 0.25);
          Real exp_fact = exp(- alpha * (t+t_0)  + 0.5*x(0));
          Real exp_sum = pi<Real>() * exp((x(0) - 0.5));
          //return _C * exp_fact* (pi<Real>() * cos(pi<Real>() * x(0)) + 0.5* sin( pi<Real>() * x(0))  ) + _C * exp_sum ; 
          //modif pour test 
          return _C * exp_fact* (pi<Real>() * cos(pi<Real>() * x(0)) + 0.5* sin( pi<Real>() * x(0))  ) + _C * exp_sum ;
        };

        u_0 =[this](const Point & x) -> Real {
          return this->u(0.,x);
        };

        u_b =[this](const Point & x) -> Real {
          return this->u(0.,x);
        };	      
          
        g_n =[this](const Point & x) -> Real {
          return 0.;
        };

        G = [_C,_nu,t_0](const Real & t, const Point & x) -> Eigen::Matrix<Real, 2, 1> {          
          Real alpha = _nu*(pi<Real>()*pi<Real>() + 0.25);
          Real exp_fact = exp(- alpha * (t+t_0)  + 0.5*x(0));
          Real exp_sum = pi<Real>() * exp((x(0) - 0.5));
          Real d_x = 0.; 
          d_x += exp_fact * (pi<Real>() * cos( pi<Real>() * x(0)) + (0.25 - pi<Real>() *pi<Real>()) * sin(pi<Real>() * x(0))  );
          d_x += exp_sum; 
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          _C * d_x ,
          0.;
          return res;
        };

        f = [](const Point & x) -> Real {
        return  0.;
        };
        
        
      }; // AdvDiff_exact 
    };  // struct AdvDiff_exact   


    // test case for mixed Dirichlet-Neumann conditions
    struct AdvDiff_exact_mix : public DataEvol
    {  
      
      AdvDiff_exact_mix(const Real & _nu = 1., const  Real & _rho = 1., const Real & _v = -2.)  
      {   

        isDirichlet = [](const Face & F) -> bool {
          Point x_F = F.barycenter();
          Real x = x_F(0);
          //Real y = x_F(1);
          bool Dir = ( x == 1.) ;
          bool Bound = F.isBoundary();
          return Dir && Bound;
        };

        isNeumann = [this](const Face & F) -> bool {
          //Point x_F = F.barycenter();
          //Real x = x_F(0);
          //Real y = x_F(1);
          bool Bound = F.isBoundary();
          bool Dir = this->isDirichlet(F);
          return Bound && (! Dir) ;
        };


        Lambda = [_nu](const Point & x) -> TensorType {
          TensorType L;
          L <<
          1. , 0. ,
          0. , _nu ;
          return   L;
        };

        Phi =[_v](const Point & x) -> Real {
          return 0.25 * pi<Real>() * x(0)  + _v * x(1) * x(1) * x(1);
        };

        u = [_nu, _rho, this ](const Real t,const Point & x) -> Real {        
          // Some relevant values related to the solution 
          Real _pi =  pi<Real>();
          const Real r = 0.25 * _pi;
          const Real omega =  0.25 * _pi; 
          const Real alpha = (_pi*_pi) / 32.; 
        
          const Real phi = this->Phi(x); 

          const Real eq = _rho *  exp(-phi);
          const Real sol_evol = exp(- alpha * t - phi) * (cos( omega  *x(0)) - sin(omega  * x(0))); 

          return sol_evol + eq;
        };

        u_0 =[this](const Point & x) -> Real {
          return this->u(0.,x);
        };

        u_b =[this](const Point & x) -> Real {
          return this->u(0.,x);
        };	      
          
        g_n =[this](const Point & x) -> Real {
          return 0.;
        };

        G = [_nu, _rho, _v,  this ](const Real & t, const Point & x) -> Eigen::Matrix<Real, 2, 1> {          
          
          const Real u = this->u(t,x); 

          Real _pi =  pi<Real>();
          const Real omega =  0.25 * _pi; 
          const Real alpha = (_pi*_pi) / 32.;     
          const Real phi = this->Phi(x); 




          Real d_x = - 0.25 * _pi * u - omega * exp(-alpha * t - phi ) * ( cos(omega * x(0)) + sin(omega *x(0)));
          Real d_y = - 3. * _v * x(1) * x(1); 

          Eigen::Matrix<Real, 2, 1> res;
          res <<
          d_x ,
          d_y;
          return res;
        };

        f = [](const Point & x) -> Real {
        return  0.;
        };
        
        
      }; // AdvDiff_exact 
    };  // struct AdvDiff_mix   


    // test case for positivity 
    struct AdvDiff_pos : public DataEvol
    {  
      AdvDiff_pos(const Real & _nu = 0.8 )
      {   
        
        isDirichlet = [](const Face & F) -> bool {
          // Point x_F = F.barycenter();
          // Real x = x_F(0);
          // Real y = x_F(1);
          // bool D_1 = ( x == 1.) ;
          // bool D_2 = ( x == 0.);
          //bool D_2 = (y==1.) && (0. < x && x < 1.);
          //bool Dir = D_1 || D_2;
          bool Dir = false;
          bool Bound = F.isBoundary();
          return Dir && Bound;
        };

        isNeumann = [this](const Face & F) -> bool {
          //Point x_F = F.barycenter();
          //Real x = x_F(0);
          //Real y = x_F(1);
          bool Bound = F.isBoundary();
          bool Dir = this->isDirichlet(F);
          return Bound && (! Dir) ;
        };


        Lambda = [_nu](const Point & x) -> TensorType {
        TensorType L;
          L <<
          _nu , 0. ,
          0. , 1. ;
          return   L;
        };

      	Phi =[](const Point & x) -> Real {
	        return -1. * ( (x(0)-0.4) * (x(0)-0.4) + (x(1)-0.6) * (x(1)-0.6));
	      };

        u_0 =[](const Point & x) -> Real {         
          Real f;
          if((x(1)-0.5)*(x(1)-0.5) + (x(0)-0.5)*(x(0)-0.5) <0.2*0.2){ f = 0.001;}
          else{f =1.;}
          return f;
	      };

        u = [this](const Real t,const Point & x) -> Real {
          return this->u_0(x) ;
        };

	      u_b =[this](const Point & x) -> Real {
	        return this->u_0(x);
	      };	      
        
        g_n =[this](const Point & x) -> Real {
	        return 0.;
	      };

        G = [](const Real & t, const Point & x) -> Eigen::Matrix<Real, 2, 1> {          
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          0. ,
          0.;
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }; // AdvDiff_pos
    };  // struct AdvDiff_pos


    // test case for long-time, non-polynomial potential and magnetic field 
    struct AdvDiff_mag : public DataEvol
    {  
      AdvDiff_mag(const Real & _b = 10., const Real & _nu =1., const Real & _v = 1., const Real & _a = 0.5 )
      {   
        
        isDirichlet = [](const Face & F) -> bool {
          // Point x_F = F.barycenter();
          // Real x = x_F(0);
          // Real y = x_F(1);
          // bool D_1 = ( x == 1.) ;
          // bool D_2 = ( x == 0.);
          //bool D_2 = (y==1.) && (0. < x && x < 1.);
          //bool Dir = D_1 || D_2;
          bool Dir = false;
          bool Bound = F.isBoundary();
          return Dir && Bound;
        };

        isNeumann = [this](const Face & F) -> bool {
          //Point x_F = F.barycenter();
          //Real x = x_F(0);
          //Real y = x_F(1);
          bool Bound = F.isBoundary();
          bool Dir = this->isDirichlet(F);
          return Bound && (! Dir) ;
        };


        Lambda = [_nu, _b](const Point & x) -> TensorType {
        TensorType L;
          L <<
          1. , _b ,
          -_b , 1. ;
          return   (_nu /( 1 +_b * _b  ) ) * L;
        };

      	Phi =[_v](const Point & x) -> Real {
	        return 0.5 * log( (1./_v) + x(0)*x(0) + 5. * x(0) * x(1) + 3. * x(1) * x(1) );
	      };

        u_0 =[_a](const Point & x) -> Real {         
          Real _pi =  pi<Real>();
          Real o = cos(2.* _pi * x(0)) * sin(2. * _pi * x(1));
          return 1. + _a * o; 
	      };

        u = [this](const Real t,const Point & x) -> Real {
          return this->u_0(x) ;
        };

	      u_b =[this](const Point & x) -> Real {
	        return this->u_0(x);
	      };	      
        
        g_n =[this](const Point & x) -> Real {
	        return 0.;
	      };

        G = [](const Real & t, const Point & x) -> Eigen::Matrix<Real, 2, 1> {          
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          0. ,
          0.;
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }; // AdvDiff_mag
    };  // struct AdvDiff_mag

    // test case for long-time, non-polynomial potential
    struct AdvDiff_nonpoly : public DataEvol
    {  
      AdvDiff_nonpoly(const Real & _l = 1000., const Real & _nu =1., const Real & _v = 1., const Real & _a = 0.5 )
      {   
        
        isDirichlet = [](const Face & F) -> bool {
          // Point x_F = F.barycenter();
          // Real x = x_F(0);
          // Real y = x_F(1);
          // bool D_1 = ( x == 1.) ;
          // bool D_2 = ( x == 0.);
          //bool D_2 = (y==1.) && (0. < x && x < 1.);
          //bool Dir = D_1 || D_2;
          bool Dir = false;
          bool Bound = F.isBoundary();
          return Dir && Bound;
        };

        isNeumann = [this](const Face & F) -> bool {
          //Point x_F = F.barycenter();
          //Real x = x_F(0);
          //Real y = x_F(1);
          bool Bound = F.isBoundary();
          bool Dir = this->isDirichlet(F);
          return Bound && (! Dir) ;
        };


        Lambda = [_nu, _l](const Point & x) -> TensorType {
        TensorType L;
          L <<
            _l  , 0. ,
           0.   , 1. ;
          return   _nu * L;
        };

      	Phi =[_v](const Point & x) -> Real {
	        return 0.5 * log( (1./_v) + (x(0) - x(1)) * (x(0) - x(1))  + 3. * x(1) * x(1) );
	      };

        u_0 =[_a](const Point & x) -> Real {         
          Real _pi =  pi<Real>();
          Real o = cos(2.* _pi * x(0)) * sin(2. * _pi * x(1));
          return 1. + _a * o; 
	      };

        u = [this](const Real t,const Point & x) -> Real {
          return this->u_0(x) ;
        };

	      u_b =[this](const Point & x) -> Real {
	        return this->u_0(x);
	      };	      
        
        g_n =[this](const Point & x) -> Real {
	        return 0.;
	      };

        G = [](const Real & t, const Point & x) -> Eigen::Matrix<Real, 2, 1> {          
          Eigen::Matrix<Real, 2, 1> res;
          res <<
          0. ,
          0.;
          return res;
        };

        f = [](const Point & x) -> Real {
          return  0.;
        };
      }; // AdvDiff_nonpoly
    };  // struct AdvDiff_nonpoly


  } // namespace NL
} // namespace ho

#endif
