#ifndef STAT_H
#define STAT_H


#include "Common/defs.h"

typedef std::function<Real(const Real &)> RealFctType;
typedef std::function<Real(const Real &,const Real & )> MeanFctType;


class Statistics
{
    public:
    Statistics();
    Statistics(const std::string & stat_name , const Real & gamma);
    Real h(const Real & s) const ;
    Real dh(const Real & s) const ;
    Real H(const Real & s) const;
    Real g(const Real & s) const ;
    Real dg(const Real & s) const ;
    bool is_Ih_valued(const Real & s) const; 
    Real proj_Ih_eps(const Real & s, const Real & s_proj) const; 
    
    int nb_bounds;
    std::vector<Real> Bounds; 

    Real a(const Real & s) const ;
    Real da(const Real & s) const ;
    Real Beta(const Real & s) const ;
    Real dBeta(const Real & s) const ;    
    Real expot(const Real & s) const ;
    Real dexpot(const Real & s) const ;
    
    RealFctType h_fct() const;
    RealFctType g_fct() const;
    RealFctType expot_fct() const;

    private:
    RealFctType m_h;
    RealFctType m_dh ;
    RealFctType m_H;
    RealFctType m_g;
    RealFctType m_dg ;
    RealFctType m_expot ;
    std::function<bool(const Real &)> m_bounds;
};

#endif