#include "Stat.h"


#include <cmath>

#ifndef s_regul
#define s_regul 0.5*std::pow(10., -4)
#endif

//------------------------------------------------------------------------------
//Statistics, implementation
Statistics::Statistics()
{
    m_h = [](const Real & s) -> Real{return log(s);};
    m_dh = [](const Real & s) -> Real{return 1./s;};
    m_H = [](const Real & s) -> Real{return  s * log(s) - s +1. ;};
    m_g = [](const Real & s) -> Real{return  exp(s);}; 
    m_dg = [](const Real & s) -> Real{return  exp(s);};
    m_bounds = [](const Real & s) -> bool {return  (s>0);};
    nb_bounds = 1;
    Bounds.resize(1);
    Bounds[0]=0.; 

}


Statistics::Statistics(const std::string & stat_name, const Real & gamma)
{
    if(stat_name =="Boltzmann"){
        m_h = [](const Real & s) -> Real{return log(s);};
        m_dh = [](const Real & s) -> Real{return 1./s;};
        m_H = [](const Real & s) -> Real{return  s * log(s) - s +1. ;};
        m_g = [](const Real & s) -> Real{return  exp(s);}; 
        m_dg = [](const Real & s) -> Real{return  exp(s);};        
        
        m_bounds = [](const Real & s) -> bool {return  (s>0);}; 
        nb_bounds = 1;
        Bounds.resize(1);
        Bounds[0]=0.;         
    }
    else if(stat_name =="Blakemore"){
        RealFctType prim_log = [](const Real & s) -> Real{return  s * log(s) - s +1. ;};
        
        m_h = [gamma](const Real & s) -> Real{return log(s/(1. - gamma *s));};
        m_dh = [gamma](const Real & s) -> Real{return 1./( s * (1.- gamma*s));};
        m_H = [gamma,prim_log](const Real & s) -> Real{return  prim_log(s) + (1./gamma) * prim_log(1. - gamma* s) ;};
        m_g = [gamma](const Real & s) -> Real{return  1./(gamma + exp(-s));}; 
        m_dg = [gamma](const Real & s) -> Real{return  exp(s)/std::pow(1. + gamma * exp(s),2.);};        
        
        m_bounds = [gamma](const Real & s) -> bool {return  (s>0) && (s< 1./gamma);};        
        nb_bounds = 2;
        Bounds.resize(2);
        Bounds[0]=0.; 
        Bounds[1]= 1./gamma; 
    }
    else{
        std::cout <<"You want to use the " + stat_name +"statistics  \n"  <<  std::endl;
        std::cout <<"This statistics is not implemented (yet ?) \n"  <<  std::endl;
        std::cout <<"Just check the spelling ;)  \n"  <<  std::endl;
    }
    
}


Real Statistics::h(const Real & s) const 
{
    return m_h(s);
}

Real Statistics::dh(const Real & s) const 
{
    return m_dh(s);
}

Real Statistics::H(const Real & s) const
{
    return m_H(s); 
}

Real Statistics::g(const Real & s) const 
{
    return m_g(s);
}

Real Statistics::dg(const Real & s) const
{
    return m_dg(s); 
}

bool Statistics::is_Ih_valued(const Real & s) const
{
    return m_bounds(s);
}  

Real Statistics::proj_Ih_eps(const Real & s, const Real & s_proj) const
{   
    Real proj = 0; 
    const Real val_min = Bounds[0] + s_proj;

    if(nb_bounds == 1){ 
        proj = std::max( s , val_min );
    } // if one bounds 

    else if(nb_bounds == 2){
        const Real val_max = Bounds[1] - s_proj;
        proj = std::min(val_max, std::max( s,val_min ) );
    } //if two bounds 

    return proj; 

} //proj_Ih_eps  




Real Statistics::a(const Real & s) const 
{
    return exp(h(s));
}

Real Statistics::da(const Real & s) const
{
    return dh(s) * a(s); 
} 

Real Statistics::Beta(const Real & s) const 
{
    return s / a(s);
}

Real Statistics::dBeta(const Real & s) const 
{
    return  (1. - s * dh(s) ) / a(s);
}

Real Statistics::expot(const Real & s) const 
{
    return h(s) - log(s);
}

Real Statistics::dexpot(const Real & s) const 
{
    return  dh(s) - 1./s;
}

RealFctType Statistics::h_fct() const
{
    return m_h;
}

RealFctType Statistics::g_fct() const
{
    return m_g;
}

RealFctType Statistics::expot_fct() const
{   
    RealFctType expot_fc = [this](const Real & s) -> Real{return this->h(s) - log(s);};
    return expot_fc;
}


