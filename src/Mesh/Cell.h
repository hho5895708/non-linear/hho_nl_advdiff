// -*- C++ -*-
#ifndef CELL_H
#define CELL_H 1

#include "Face.h"

class Mesh;
class Cell
{
public:
  typedef std::vector<Integer> IdVectorType;
  typedef std::vector<Face> FaceVectorType;
  typedef std::vector<Cell> CellVectorType;

  typedef std::pair<FaceVectorType::const_iterator,
                    FaceVectorType::const_iterator> FaceVectorConstIterators;

  typedef std::pair<CellVectorType::const_iterator,
                    CellVectorType::const_iterator> CellVectorConstIterators;

  enum CellParameter {
    P_MEASURE,
    P_FACENUMBER,
    P_NODENUMBER,
    P_CENTER
  };

public:
  Cell(){};

  Cell(Integer a_number_of_faces, const Point & a_center = ZeroRealVector(DIM))
    : m_number_of_faces(a_number_of_faces),
      m_number_of_nodes(0),
      m_center(a_center),
      m_measure(0.)
  {
    m_face_ids.reserve(a_number_of_faces);
  }


  //! Set parameter
  template<typename T>
  void setParameter(CellParameter a_parameter, const T & a_value);
  //! Get parameter
  template<typename T>
  const T & getParameter(CellParameter a_parameter);

  //! Return the number of faces
  Integer numberOfFaces() const;
  //! Return the number of faces
  Integer  numberOfNodes() const;
  //! Return the center
  const Point & center()  const;
  //! Return the measure
  const Real & measure() const;
  //! Return face id
  Integer faceId(Integer i) const;
  //! Return face local id
  Integer faceLocalId(Integer i) const;
  //! Return node id
  Integer nodeId(Integer i) const;

  friend std::ostream & operator<<(std::ostream & a_ostr, const Cell & a_K);

  //! Build subfaces
  void buildSubMesh(const Mesh * a_Th);
  //! Return the number of subfaces
  Integer numberOfSubFaces() const
  {
    return m_subfaces.size();
  }
  //! Return the number of subcells
  Integer numberOfSubCells() const
  {
    return m_subcells.size();
  }
  //! Return subfaces
  inline FaceVectorConstIterators subFaces() const
  {
    return FaceVectorConstIterators(m_subfaces.begin(), m_subfaces.end());
  }
  //! Return subcells
  inline CellVectorConstIterators subCells() const
  {
    return CellVectorConstIterators(m_subcells.begin(), m_subcells.end());
  }
  //! Return \f$i\f$th subcell
  inline const Cell & subCell(Integer a_i) const
  {
    return m_subcells[a_i];
  }
private:
  Integer m_number_of_faces;
  IdVectorType m_face_ids;
  Integer m_number_of_nodes;
  IdVectorType m_node_ids;
  Point m_center;
  Real m_measure;

  FaceVectorType m_subfaces;
  CellVectorType m_subcells;
};

#endif
