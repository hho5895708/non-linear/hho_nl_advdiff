#ifndef ITRANSFORMATION_H
#define ITRANSFORMATION_H

#include "Common/defs.h"

class ITransformation
{
 public:
  virtual Point apply(const Point & a_P) = 0;
};

#endif
