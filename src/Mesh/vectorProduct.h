// -*- C++ -*-
#ifndef VECTORPRODUCT_H
#define VECTORPRODUCT_H

#include "Common/defs.h"

template<Integer Dimension>
boost::numeric::ublas::bounded_vector<Real, Dimension>
vector_product(const boost::numeric::ublas::bounded_vector<Real, Dimension> & a_x, 
               const boost::numeric::ublas::bounded_vector<Real, Dimension> & a_y);

#endif
