#include "Node.h"

int Node::nbmaille() const{
	return nummaille.size();
}
int Node::nbface() const{
	return numface.size();
}

const Point & Node::getPoint() const
{
		return p;
}

void Node::putPoint(Point &n){
	p = n;
}



void Node::putNumeroMaille(int num){
	int i,size,ok=1;

	size = nummaille.size();
	for(i=0;i<size;i++){
		if(num == nummaille[i]){
			ok = 0;
			break;
		}
	}
	if(ok)
		nummaille.push_back(num);
}


void Node::putNumeroFace(int num){
	int i,size,ok=1;

	size = numface.size();
	for(i=0;i<size;i++){
		if(num == numface[i]){
			ok = 0;
			break;
		}
	}
	if(ok)
	  numface.push_back(num);
}

void Node::ConnectiviteCommune(Node * n){

  int sizej,sizek,p,num;
  
  // connectivite des mailles
  sizej = nummaille.size();
  sizek = n->nummaille.size();
  
  for(p=0;p<sizek;p++){
    num = n->nummaille[p];
    putNumeroMaille(num);
  }
  
  for(p=0;p<sizej;p++){
    num = nummaille[p];
    n->putNumeroMaille(num);
  }




}


void Node::egale(const Node & np){
	int i,size;

	p = np.p;
	size = np.nummaille.size();

	for(i=0;i<size;i++)
		nummaille.push_back(np.nummaille[i]);
}
