#ifndef POINT_HPP
#define POINT_HPP

#include <iostream>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/vector_expression.hpp>
#include <boost/numeric/ublas/io.hpp>

namespace common {
  namespace ublas = boost::numeric::ublas;

  /*!
    \class Point
    \brief Geometric point
    \author Daniele A. Di Pietro
    \date 2006-07-14
  */

  template<class T>
  class Point : public ublas::bounded_vector<T, 3> {
  public:
    /** @name Constructors and destructors
     */
    //@{
    //! Construct from identifier only
    Point(int id = 0) : 
      ublas::bounded_vector<T, 3>(ublas::zero_vector<T>(3)),
      M_id(id) {};
    //! Construct from set of co-ordinates and identifier
    Point(T x0, T x1 = 0, T x2 = 0, int id = 0) : M_id(id) {
      this->operator()(0) = x0;
      this->operator()(1) = x1;
      this->operator()(2) = x2;
    }
    //! Construct from a boost vector expression
    template<typename E>
    Point(const ublas::vector_expression<E>& b) {
      assert(b().size() <= 3);
      copy(b().begin(), b().end(), this->begin());
    }
    //@}

    /** @name Members
     */
    //@{
    //! Return the identifier
    inline int id() const { return M_id; };
    //@}
  private:
    //! Identifier
    int M_id;
  };

  ////////////////////////////////////////////////////////////
  // Implementation

  template<typename T>
  inline Point<T> operator+(const Point<T>& P1, const Point<T>& P2) {
    Point<T> P(P1);
    for(int i = 0; i < 3; i++) P(i) += P2(i);
    return P;
  }
  
  template<typename T>
  inline Point<T> operator-(const Point<T>& P1, const Point<T>& P2) {
    Point<T> P(P1);
    for(int i = 0; i < 3; i++) P(i) -= P2(i);
    return P;
  }
 }

#endif
